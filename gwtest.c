#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <ipdc/packet.h>
#include <ipdc/messages.h>
#include <ipdc/types.h>
#include <ipdc/tags.h>
#include <ipdc/modules.h>
#include <ipdc/pri.h>

extern int errno;
struct ipdc_msg_handlers_t *msgh_head = NULL, *msgh_tail = NULL;
struct ipdc_system_info_t *si_head = NULL, *si_tail = NULL;
struct ipdc_conga_line_t *conga_head = NULL, *conga_tail = NULL;
struct mod_stat_t *mod_head = NULL, *mod_tail = NULL;
struct mod_lines_t *mod_line_head = NULL, *mod_line_tail = NULL;
struct mod_chans_t *mod_chan_head = NULL, *mod_chan_tail = NULL;
struct call_ref_t *call_ref_head = NULL, *call_ref_tail = NULL;
struct xconnect_t *xc_head = NULL, *xc_tail = NULL;
struct trunkgroup_t *tg_head = NULL, *tg_tail = NULL;
struct call_route_t *call_route_head = NULL, *call_route_tail = NULL;
struct db_xconnect_t *dbx_head = NULL, *dbx_tail = NULL;
struct db_trunkgroup_t *dbt_head = NULL, *dbt_tail = NULL;

pthread_mutex_t mtxnet;
pthread_rwlock_t rwlock;

struct ipdc_msgs_t ipdc_find_msg(uint16_t code)
{
	int i;

	for (i = 0; i < IPDC_MSGS_LEN; i++) {
		if (ipdc_msgs[i].code == code) {
			return ipdc_msgs[i];
		}
	}

	return ipdc_find_msg(IPDC_MSG_UNKNOWN);
}

char *ipdc_msgcode_name(uint16_t code)
{
	return (ipdc_find_msg(code)).name;
}

char *ipdc_msgcode_descr(uint16_t code)
{
	return (ipdc_find_msg(code)).descr;
}

struct ipdc_tags_t ipdc_find_tag(uint8_t tag)
{
	int i;

	for (i = 0; i < IPDC_TAGS_LEN; i++) {
		if (ipdc_tags[i].tag == tag) {
			return ipdc_tags[i];
		}
	}

	return ipdc_find_tag(IPDC_TAG_UNKNOWN);
}

struct ipdc_mod_types_t ipdc_find_modtype(uint8_t type)
{
	int i;

	for (i = 0; i < IPDC_MOD_TYPE_LEN; i++) {
		if (ipdc_mod_types[i].type == type) {
			return ipdc_mod_types[i];
		}
	}

	return ipdc_find_modtype(IPDC_MODULE_UNKNOWN);
}

struct ipdc_mod_status_t ipdc_find_modstatus(uint8_t type)
{
	int i;

	for (i = 0; i < IPDC_MOD_STATUS_LEN; i++) {
		if (ipdc_mod_status[i].type == type) {
			return ipdc_mod_status[i];
		}
	}

	return ipdc_find_modstatus(IPDC_MOD_STATUS_UNKNOWN);
}

struct ipdc_line_status_t ipdc_find_linestatus(uint8_t type)
{
	int i;

	for (i = 0; i < IPDC_LINE_STATUS_LEN; i++) {
		if (ipdc_line_status[i].type == type) {
			return ipdc_line_status[i];
		}
	}

	return ipdc_find_linestatus(IPDC_LINE_STATUS_UNKNOWN);
}

struct ipdc_chan_status_t ipdc_find_chanstatus(uint8_t type)
{
	int i;

	for (i = 0; i < IPDC_CHAN_STATUS_LEN; i++) {
		if (ipdc_chan_status[i].type == type) {
			return ipdc_chan_status[i];
		}
	}

	return ipdc_find_chanstatus(IPDC_CHAN_STATUS_UNKNOWN);
}

struct ipdc_q931_msg_t ipdc_find_q931_msg(uint8_t msg)
{
	int i;

	for (i = 0; i < IPDC_Q931_MSG_LEN; i++) {
		if (ipdc_q931_msgs[i].msg == msg) {
			return ipdc_q931_msgs[i];
		}
	}

	return ipdc_find_q931_msg(IPDC_Q931_MSG_UNKNOWN);
}

struct ipdc_q931_proto_t ipdc_find_q931_proto(uint8_t proto)
{
	int i;

	for (i = 0; i < IPDC_Q931_PROTO_LEN; i++) {
		if (ipdc_q931_protos[i].proto == proto) {
			return ipdc_q931_protos[i];
		}
	}

	return ipdc_find_q931_proto(IPDC_Q931_PROTO_UNKNOWN);
}

struct ipdc_q931_ie_t ipdc_find_q931_ie(uint8_t ie)
{
	int i;

	for (i = 0; i < IPDC_Q931_IE_LEN; i++) {
		if (ipdc_q931_ies[i].ie == ie) {
			return ipdc_q931_ies[i];
		}
	}

	return ipdc_find_q931_ie(IPDC_Q931_IE_UNKNOWN);
}

struct ipdc_q931_sig_proto_t ipdc_find_q931_sig_proto(uint16_t sig_proto)
{
	int i;

	for (i = 0; i < IPDC_Q931_SIG_PROTO_LEN; i++) {
		if (ipdc_q931_sig_protos[i].sig_proto == sig_proto) {
			return ipdc_q931_sig_protos[i];
		}
	}

	return ipdc_find_q931_sig_proto(IPDC_Q931_SIG_PROTO_UNKNOWN);
}

struct ipdc_q931_num_type_t ipdc_find_q931_numtype(uint8_t type)
{
	int i;

	for (i = 0; i < IPDC_Q931_NUM_TYPES_LEN; i++) {
		if (ipdc_q931_num_types[i].type == type) {
			return ipdc_q931_num_types[i];
		}
	}

	return ipdc_find_q931_numtype(IPDC_Q931_NUMBER_TYPE_RESERVED);
}

struct ipdc_q931_num_plan_t ipdc_find_q931_numplan(uint8_t plan)
{
	int i;

	for (i = 0; i < IPDC_Q931_NUM_PLANS_LEN; i++) {
		if (ipdc_q931_num_plans[i].plan == plan) {
			return ipdc_q931_num_plans[i];
		}
	}

	return ipdc_find_q931_numplan(IPDC_Q931_NUMBER_PLAN_RESERVED);
}

char *ipdc_find_q931_info_chan(uint8_t sel)
{
	int i;

	for (i = 0; i < IPDC_Q931_INFO_CHANNELS_LEN; i++) {
		if (ipdc_q931_info_channels[i].sel == sel) {
			return ipdc_q931_info_channels[i].descr;
		}
	}

	return ipdc_find_q931_info_chan(IPDC_Q931_INFO_CHAN_UNKNOWN);
}

char *ipdc_find_q931_dchan(uint8_t id)
{
	int i;

	for (i = 0; i < IPDC_Q931_DCHAN_LEN; i++) {
		if (ipdc_q931_dchan[i].id == id) {
			return ipdc_q931_dchan[i].descr;
		}
	}

	return ipdc_find_q931_dchan(IPDC_Q931_DCHAN_UNKNOWN);
}

char *ipdc_find_q931_prefexcl(uint8_t val)
{
	int i;

	for (i = 0; i < IPDC_Q931_PREFEXCL_LEN; i++) {
		if (ipdc_q931_prefexcl[i].val == val) {
			return ipdc_q931_prefexcl[i].descr;
		}
	}

	return ipdc_find_q931_prefexcl(IPDC_Q931_PREFEXCL_UNKNOWN);
}

char *ipdc_find_q931_int_type(uint8_t type)
{
	int i;

	for (i = 0; i < IPDC_Q931_INT_TYPES_LEN; i++) {
		if (ipdc_q931_int_types[i].type == type) {
			return ipdc_q931_int_types[i].descr;
		}
	}

	return ipdc_find_q931_int_type(IPDC_Q931_INT_TYPE_UNKNOWN);
}

char *ipdc_find_q931_int_id_present(uint8_t type)
{
	int i;

	for (i = 0; i < IPDC_Q931_INT_ID_PRESENT_LEN; i++) {
		if (ipdc_q931_int_id_present[i].type == type) {
			return ipdc_q931_int_id_present[i].descr;
		}
	}

	return ipdc_find_q931_int_id_present(IPDC_Q931_INT_ID_PRESENT_UNKNOWN);
}

char *ipdc_find_q931_chanmap_type(uint8_t type)
{
	int i;

	for (i = 0; i < IPDC_Q931_CHANMAP_TYPES_LEN; i++) {
		if (ipdc_q931_chanmap_types[i].type == type) {
			return ipdc_q931_chanmap_types[i].descr;
		}
	}

	return ipdc_find_q931_chanmap_type(IPDC_Q931_CHANMAP_TYPE_UNKNOWN);
}

char *ipdc_find_q931_chanmap_num(uint8_t type)
{
	int i;

	for (i = 0; i < IPDC_Q931_CHANMAP_TYPES_LEN; i++) {
		if (ipdc_q931_chanmap_num[i].type == type) {
			return ipdc_q931_chanmap_num[i].descr;
		}
	}

	return ipdc_find_q931_chanmap_num(IPDC_Q931_CHANMAP_TYPE_UNKNOWN);
}

char *ipdc_find_q931_coding_std(uint8_t type)
{
	int i;

	for (i = 0; i < IPDC_Q931_CODING_STD_LEN; i++) {
		if (ipdc_q931_coding_std[i].type == type) {
			return ipdc_q931_coding_std[i].descr;
		}
	}

	return ipdc_find_q931_coding_std(IPDC_Q931_CODING_STD_UNKNOWN);
}

char *ipdc_find_q931_restart_ind(uint8_t ind)
{
	int i;

	for (i = 0; i < IPDC_Q931_RESTART_IND_LEN; i++) {
		if (ipdc_q931_restart_ind[i].ind == ind) {
			return ipdc_q931_restart_ind[i].descr;
		}
	}

	return ipdc_find_q931_restart_ind(IPDC_Q931_RESTART_IND_UNKNOWN);
}

char *ipdc_find_q931_call_state(uint8_t state)
{
	int i;

	for (i = 0; i < IPDC_Q931_CALL_STATE_LEN; i++) {
		if (ipdc_q931_call_state[i].state == state) {
			return ipdc_q931_call_state[i].descr;
		}
	}

	return ipdc_find_q931_call_state(IPDC_Q931_CALL_STATE_UNKNOWN);
}

char *ipdc_find_q931_cause_class(uint8_t class)
{
	int i;

	for (i = 0; i < IPDC_Q931_CAUSE_CLASS_LEN; i++) {
		if (ipdc_q931_cause_class[i].class == class) {
			return ipdc_q931_cause_class[i].descr;
		}
	}

	return ipdc_find_q931_cause_class(IPDC_Q931_CAUSE_CLASS_UNKNOWN);
}

struct ipdc_q931_cause_code_t ipdc_find_q931_cause_code(uint8_t code)
{
	int i;

	for (i = 0; i < IPDC_Q931_CAUSE_CODE_LEN; i++) {
		if (ipdc_q931_cause_code[i].code == code) {
			return ipdc_q931_cause_code[i];
		}
	}

	return ipdc_find_q931_cause_code(IPDC_Q931_CAUSE_CODE_UNKNOWN);
}

char *ipdc_tag_name(uint8_t tag)
{
	return (ipdc_find_tag(tag)).name;
}

ipdc_tag_type_t ipdc_tag_type(uint8_t tag)
{
	return (ipdc_find_tag(tag)).type;
}

struct ipdc_system_info_t *sys_find(uint32_t ipaddr)
{
	struct ipdc_system_info_t *si = si_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (si != NULL) {
		if (si->ip == ipaddr) {
			goto unlock;
		}
		si = si->next;
	}
	si = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return si;
}

struct ipdc_system_info_t *sys_find_by_thread_id(pthread_t thread_id)
{
	struct ipdc_system_info_t *si = si_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (si != NULL) {
		if (si->thread_id == thread_id) {
			goto unlock;
		}
		si = si->next;
	}
	si = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return si;
}

struct ipdc_system_info_t *sys_add(uint32_t ipaddr, pthread_t tid)
{
	struct ipdc_system_info_t *si = NULL;

	if (sys_find(ipaddr) != NULL) {
		return NULL;
	}

	si = calloc(1, sizeof(struct ipdc_system_info_t));
	if (si == NULL) {
		perror("malloc");
		return NULL;
	}

	si->thread_id = tid;
	si->ip = ipaddr;
	si->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return NULL;
	}

	if (si_head == NULL) {
		si_head = si_tail = si;
	} else {
		si_tail->next = si;
		si_tail = si_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return si;
}

int sys_del(uint32_t ipaddr)
{
	struct ipdc_system_info_t *si = NULL, *s = si_head;

	si = sys_find(ipaddr);
	if (si == NULL) {
		return -1;
	}

	printf("WORKING ON %s\n", si->id);

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (si == si_head && si == si_tail) {
		free(si);
		si_head = si_tail = NULL;
	} else if (si == si_head) {
		si_head = si_head->next;
		free(si);
	} else if (si == si_tail) {
		while (s != NULL) {
			if (s->next == si) {
				si_tail = s;
				free(si);
				break;
			}
			s = s->next;
		}
	} else {
		while (s != NULL) {
			if (s->next == si) {
				s->next = si->next;
				free(si);
				break;
			}
			s = s->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

struct db_xconnect_t *db_find_xc(uint32_t ipaddr, uint16_t module, uint16_t line, uint16_t chan)
{
	struct db_xconnect_t *dbx = dbx_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (dbx != NULL) {
		if (dbx->s_ip == ipaddr && dbx->s_module == module && dbx->s_line == line && dbx->s_chan == chan) {
			goto unlock;
		}

		if (dbx->d_ip == ipaddr && dbx->d_module == module && dbx->d_line == line && dbx->d_chan == chan) {
			goto unlock;
		}

		dbx = dbx->next;
	}
	dbx = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return dbx;
}

struct db_xconnect_t *db_add_xc(uint32_t s_ip, uint16_t s_mod, uint16_t s_line, uint16_t s_chan, uint32_t d_ip, uint16_t d_mod, uint16_t d_line, uint16_t d_chan)
{
	struct db_xconnect_t *dbx = NULL;;

	dbx = calloc(1, sizeof(struct db_xconnect_t));
	if (dbx == NULL) {
		perror("calloc");
		return NULL;
	}

	dbx->s_ip = s_ip;
	dbx->s_module = s_mod;
	dbx->s_line = s_line;
	dbx->s_chan = s_chan;
	dbx->d_ip = d_ip;
	dbx->d_module = d_mod;
	dbx->d_line = d_line;
	dbx->d_chan = d_chan;
	dbx->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return NULL;
	}

	if (dbx_head == NULL) {
		dbx_head = dbx_tail = dbx;
	} else {
		dbx_tail->next = dbx;
		dbx_tail = dbx_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return dbx;
}

int db_delete_xc(struct db_xconnect_t *dbx)
{
	struct db_xconnect_t *d = dbx_head;

	if (dbx == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (dbx_head == dbx && dbx_tail == dbx) {
		free(dbx);
		dbx_head = dbx_tail = NULL;
	} else if (dbx_head == dbx) {
		dbx_head = dbx_head->next;
		free(dbx);
	} else if (dbx_tail == dbx) {
		while (d != NULL) {
			if (d->next == dbx) {
				dbx_tail = d;
				free(dbx);
				break;
			}
			d = d->next;
		}
	} else {
		while (d != NULL) {
			if (d->next == dbx) {
				d->next = dbx->next;
				free(dbx);
				break;
			}
			d = d->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

struct db_trunkgroup_t *db_find_tg(uint32_t ipaddr, uint16_t module, uint16_t line, uint16_t chan)
{
	struct db_trunkgroup_t *dbt = dbt_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (dbt != NULL) {
		if (dbt->ip == ipaddr && dbt->module == module && dbt->line == line && dbt->chan == chan) {
			goto unlock;
		}

		dbt = dbt->next;
	}
	dbt = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return dbt;
}

struct db_trunkgroup_t *db_add_tg(uint32_t ip, uint16_t mod, uint16_t line, uint16_t chan, uint16_t tg)
{
	struct db_trunkgroup_t *dbt = NULL;;

	dbt = calloc(1, sizeof(struct db_trunkgroup_t));
	if (dbt == NULL) {
		perror("calloc");
		return NULL;
	}

	dbt->ip = ip;
	dbt->module = mod;
	dbt->line = line;
	dbt->chan = chan;
	dbt->tg = tg;
	dbt->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return NULL;
	}

	if (dbt_head == NULL) {
		dbt_head = dbt_tail = dbt;
	} else {
		dbt_tail->next = dbt;
		dbt_tail = dbt_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return dbt;
}

int db_delete_tg(struct db_trunkgroup_t *dbt)
{
	struct db_trunkgroup_t *t = dbt_head;

	if (dbt == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (dbt_head == dbt && dbt_tail == dbt) {
		free(dbt);
		dbt_head = dbt_tail = NULL;
	} else if (dbt_head == dbt) {
		dbt_head = dbt_head->next;
		free(dbt);
	} else if (dbt_tail == dbt) {
		while (t != NULL) {
			if (t->next == dbt) {
				dbt_tail = t;
				free(dbt);
				break;
			}
			t = t->next;
		}
	} else {
		while (t != NULL) {
			if (t->next == dbt) {
				t->next = dbt->next;
				free(dbt);
				break;
			}
			t = t->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

struct call_route_t *call_route_find(uint32_t ip, char *cpn)
{
	struct call_route_t *route = call_route_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	while (route != NULL) {
		if (route->ip == ip && strncmp(route->cpn, cpn, route->cpn_len) == 0) {
			goto unlock;
		}

		route = route->next;
	}
	route = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return route;
}

struct call_route_t *call_route_add(uint32_t ip, char *cpn, uint16_t tgid)
{
	struct call_route_t *r = NULL;

	if (call_route_find(ip, cpn) != NULL) {
		return NULL;
	}

	if (cpn == NULL || tgid == 0) {
		return NULL;
	}

	if (strlen(cpn) > sizeof(r->cpn)) {
		return NULL;
	}

	r = calloc(1, sizeof(struct call_route_t));
	if (r == NULL) {
		perror("calloc");
		return NULL;
	}

	r->ip = ip;
	r->tg = tgid;
	r->cpn_len = strlen(cpn);
	memcpy(r->cpn, cpn, r->cpn_len);
	r->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return NULL;
	}

	if (call_route_head == NULL) {
		call_route_head = call_route_tail = r;
	} else {
		call_route_tail->next = r;
		call_route_tail = call_route_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return r;
}

int call_route_delete(struct call_route_t *r)
{
	struct call_route_t *cr = call_route_head;

	if (r == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (call_route_head == r && call_route_tail == r) {
		free(r);
		call_route_head = call_route_tail = NULL;
	} else if (call_route_head == r) {
		call_route_head = call_route_head->next;
		free(r);
	} else if (call_route_tail == r) {
		while (cr != NULL) {
			if (cr->next == r) {
				call_route_tail = cr;
				free(r);
				break;
			}
			cr = cr->next;
		}
	} else {
		while (cr != NULL) {
			if (cr->next == r) {
				cr->next = r->next;
				free(r);
				break;
			}
			cr = cr->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

struct trunkgroup_t *tg_find(struct mod_chans_t *c)
{
	struct trunkgroup_t *tg = tg_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (tg != NULL) {
		if (tg->si == c->si && tg->t == c) {
			goto unlock;
		}
		tg = tg->next;
	}
	tg = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}
	
	return tg;
}

struct trunkgroup_t *tg_find_chan_with_status(struct ipdc_system_info_t *si, uint16_t tgid, uint8_t status)
{
	struct trunkgroup_t *tg = tg_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (tg != NULL) {
		if (tg->si == si && tg->tg == tgid && tg->t->status == status) {
			goto unlock;
		}
		if (tg->si == si && tg->tg == tgid) {
			printf("%s:1/%d/%d/%d has status %s\n", inet_ntoa(*((struct in_addr *)&tg->si->ip)), tg->t->l->m->module, tg->t->l->line, tg->t->chan, (ipdc_find_chanstatus(tg->t->status).descr));
		}
		tg = tg->next;
	}
	tg = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}
	
	return tg;
}

struct trunkgroup_t *tg_add(struct ipdc_system_info_t *si, struct mod_chans_t *c, uint16_t tg)
{
	struct trunkgroup_t *t = NULL;;

	t = calloc(1, sizeof(struct trunkgroup_t));
	if (t == NULL) {
		perror("calloc");
		return NULL;
	}

	t->si = si;
	t->t = c;
	t->tg = tg;
	t->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return NULL;
	}

	if (tg_head == NULL) {
		tg_head = tg_tail = t;
	} else {
		tg_tail->next = t;
		tg_tail = tg_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return t;
}

int tg_delete(struct trunkgroup_t *t)
{
	struct trunkgroup_t *tg = tg_head;

	if (t == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (tg_head == t && tg_tail == t) {
		free(t);
		tg_head = tg_tail = NULL;
	} else if (tg_head == t) {
		tg_head = tg_head->next;
		free(t);
	} else if (tg_tail == t) {
		while (tg != NULL) {
			if (tg->next == t) {
				tg_tail = tg;
				free(t);
				break;
			}
			tg = tg->next;
		}
	} else {
		while (tg != NULL) {
			if (tg->next == t) {
				tg->next = t->next;
				free(t);
				break;
			}
			tg = tg->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

struct xconnect_t *xc_find(struct mod_chans_t *c)
{
	struct xconnect_t *xc = xc_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (xc != NULL) {
		if (xc->src == c || xc->dst == c) {
			goto unlock;
		}
		xc = xc->next;
	}
	xc = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return xc;
}

struct xconnect_t *xc_find_src(struct mod_chans_t *c)
{
	struct xconnect_t *xc = xc_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (xc != NULL) {
		if (xc->src == c) {
			goto unlock;
		}
		xc = xc->next;
	}
	xc = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return xc;
}

struct xconnect_t *xc_find_dst(struct mod_chans_t *c)
{
	struct xconnect_t *xc = xc_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (xc != NULL) {
		if (xc->dst == c) {
			goto unlock;
		}
		xc = xc->next;
	}
	xc = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return xc;
}

struct xconnect_t *xc_find_call(struct mod_chans_t *c)
{
	struct xconnect_t *xc = xc_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (xc != NULL) {
		if (xc->call == c) {
			goto unlock;
		}
		xc = xc->next;
	}
	xc = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return xc;
}

struct xconnect_t *xc_add(struct mod_chans_t *sc, struct mod_chans_t *dc)
{
	struct xconnect_t *x = NULL, *xc = NULL;

	x = xc_find_src(sc);
	if (x != NULL && x->dst == dc) {
		/* Crossconnect already exists, break out */
		return NULL;
	}

	xc = calloc(1, sizeof(struct xconnect_t));
	if (xc == NULL) {
		perror("malloc");
		return NULL;
	}

	xc->src = sc;
	xc->dst = dc;
	xc->call = NULL;
	xc->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return NULL;
	}

	if (xc_head == NULL) {
		xc_head = xc_tail = xc;
	} else {
		xc_tail->next = xc;
		xc_tail = xc_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return xc;
}

int xc_delete(struct xconnect_t *xc)
{
	struct xconnect_t *x = xc_head;

	if (xc == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (xc_head == xc && xc_tail == xc) {
		free(xc);
		xc_head = xc_tail = NULL;
	} else if (xc_head == xc) {
		xc_head = xc_head->next;
		free(xc);
	} else if (xc_tail == xc) {
		while (x != NULL) {
			if (x->next == xc) {
				xc_tail = x;
				free(xc);
				break;
			}
			x = x->next;
		}
	} else {
		while (x != NULL) {
			if (x->next == xc) {
				x->next = xc->next;
				free(xc);
				break;
			}
			x = x->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

int xc_remove(struct mod_chans_t *c)
{
	struct xconnect_t *xc = NULL;

	if ((xc = xc_find(c)) == NULL) {
		return -1;
	}

	if (xc_delete(xc) != 0) {
		return -1;
	}

	return 0;
}

int conga_add(struct ipdc_system_info_t *si, uint8_t msgcode, uint32_t trans_id)
{
	struct ipdc_conga_line_t *c = NULL;

	if (si == NULL) {
		return -1;
	}

	if ((ipdc_find_msg(msgcode).code) == IPDC_MSG_UNKNOWN) {
		return -1;
	}

	c = malloc(sizeof(struct ipdc_conga_line_t));
	if (c == NULL) {
		perror("malloc");
		return -1;
	}

	c->si = si;
	c->code = msgcode;
	c->trans_id = trans_id;
	c->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (conga_head == NULL) {
		conga_head = conga_tail = c;
	} else {
		conga_tail->next = c;
		conga_tail = conga_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

struct ipdc_conga_line_t *conga_find(struct ipdc_system_info_t *si, uint32_t trans_id)
{
	struct ipdc_conga_line_t *c = conga_head;

	if (si == NULL) {
		return NULL;
	}

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (c != NULL) {
		if (c->si == si && c->trans_id == trans_id) {
			goto unlock;
		}
		
		c = c->next;
	}
	c = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return c;
}

int conga_count(struct ipdc_system_info_t *si)
{
	uint16_t count = 0;
	struct ipdc_conga_line_t *c = conga_head;

	if (si == NULL) {
		return -1;
	}

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return -1;
	}

	while (c != NULL) {
		if (c->si == si) {
			count++;
		}

		c = c->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return count;
}

int conga_delete(struct ipdc_conga_line_t *conga)
{
	struct ipdc_conga_line_t *c = conga_head;

	if (conga == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (conga_head == conga && conga_tail == conga) {
		free(conga);
		conga_head = conga_tail = NULL;
	} else if (conga_head == conga) {
		conga_head = conga_head->next;
		free(conga);
	} else if (conga_tail == conga) {
		while (c != NULL) {
			if (c->next == conga) {
				conga_tail = c;
				free(conga);
				break;
			}
			c = c->next;
		}
	} else {
		while (c != NULL) {
			if (c->next == conga) {
				c->next = conga->next;
				free(conga);
				break;
			}
			c = c->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

int ipdc_write_pkt(struct ipdc_system_info_t *si, char *msg, int pktlen)
{
	int nwrite, nbytes;
	struct ipdc_pkthdr_t *h;
	uint8_t code;

	if (si == NULL) {
		return -1;
	}

	if (msg == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	nbytes = 0;

	/* write packet length */
	nwrite = write(si->sock, msg, 2);
	if (nwrite == -1) {
		perror("write");
		pthread_rwlock_unlock(&rwlock);
		return -1;
	}
	nbytes += nwrite;

	/* write zeros for unused sequence number fields */
	nwrite = write(si->sock, "\0\0", 2);
	if (nwrite == -1) {
		perror("write");
		pthread_rwlock_unlock(&rwlock);
		return -1;
	}
	nbytes += nwrite;

	/* write remainder of packet */
	nwrite = write(si->sock, msg + 2, pktlen - 2);
	if (nwrite == -1) {
		perror("write");
		pthread_rwlock_unlock(&rwlock);
		return -1;
	}
	nbytes += nwrite;

	h = (struct ipdc_pkthdr_t *)msg;
	if (h->trans_id > si->last_tid_sent) {
		si->last_tid_sent = h->trans_id;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	code = ntohs(h->msg_code);
	if (ipdc_find_msg(code).ack_needed) {
		if (conga_add(si, code, h->trans_id) != 0) {
			fprintf(stderr, "couldn't add message to the conga line!\n");
			return -1;
		}
	} else {
		printf("ACK NOT NEEDED FOR %s\n", ipdc_find_msg(code).name);
	}

	printf("CURRENTLY %d ELEMENTS IN CONGA LINE FOR %s\n", conga_count(si), inet_ntoa(*((struct in_addr *)&si->ip)));

	return nbytes;
}

struct ipdc_pktdata_t *ipdc_pop_tag(char *buf, int buflen, int offset)
{
	struct ipdc_pktdata_t *d;
	unsigned int dlen;

	if (offset > buflen - IPDC_PKTDATA_T_LEN) {
		return NULL;
	}

	if (offset + 1 == buflen) {
		return NULL;
	}

	memcpy(&dlen, buf + offset + 1, 1);
	if (dlen == 0) {
		return NULL;
	}

	d = calloc(1, dlen + IPDC_PKTDATA_T_LEN);
	if (d == NULL) {
		perror("calloc");
		return NULL;
	}
	memcpy(d, buf + offset, dlen + IPDC_PKTDATA_T_LEN);

	return d;
}

void ipdc_print_tag(struct ipdc_pktdata_t *d)
{
	int i;
	uint32_t datauint = 0;
	char *datastr = NULL;

	printf("  tag: 0x%02x --> %s\n", d->tag, ipdc_tag_name(d->tag));
	printf("  size: %d byte%s\n", d->size, d->size == 1 ? "" : "s");

	switch(ipdc_tag_type(d->tag)) {
		case IPDC_TAG_TYPE_UINT:
			printf("  type: unsigned int\n");
			datauint = 0;

			if (d->size > sizeof(uint32_t)) {
				printf("  error: integer overflow\n");
				break;
			}

			if (d->size > 0) {
				memcpy(&datauint, d->data, d->size);
				if (d->size == 1) {
					datauint = ntohs(datauint) >> 8;
				} else {
					datauint = ntohl(datauint) >> (d->size * 8);
				}
			}

			printf("  value: %d\n", datauint);
			break;
		case IPDC_TAG_TYPE_ASCII:
			printf("  type: string\n");

			if (d->size == 0) {
				printf("  <empty string>\n");
				break;
			}

			datastr = calloc(1, d->size + 1);
			if (datastr == NULL) {
				perror("calloc");
				break;
			}
			memcpy(datastr, d->data, d->size);
			printf("  value: %s\n", datastr);

			free(datastr);
			break;
		case IPDC_TAG_TYPE_BYTE:
		case IPDC_TAG_TYPE_OCTET:
			printf("  type: octet/byte\n");
			printf("    ");

			for (i = 0; i < d->size; i++) {
				printf("%02x ", d->data[i]);
				if (i == 24)
					printf("\n    ");
			}

			printf("\n");
			break;
		default:
			printf("  type: unknown\n");
	}

	printf("\n");
}

void ipdc_print_all_tags(char *buf, int buflen, int offset)
{
	struct ipdc_pktdata_t *d;
	int len = 0;

	while((d = ipdc_pop_tag(buf + offset, buflen, len)) != NULL) {
		len += d->size + IPDC_PKTDATA_T_LEN;
		ipdc_print_tag(d);
		free(d);
	}
}

struct ipdc_pkthdr_t *ipdc_create_pkthdr(uint32_t trans_id, uint16_t msg_code)
{
	struct ipdc_pkthdr_t *h;

	if ((ipdc_find_msg(msg_code)).code == IPDC_MSG_UNKNOWN) {
		return NULL;
	}

	h = calloc(1, IPDC_PKTHDR_T_LEN);
	if (h == NULL) {
		perror("calloc");
		return NULL;
	}

	h->pkt_len = IPDC_MAX_PACKET_SIZE;
	h->protocol_id = IPDC_PROTOCOL_ID;
	h->trans_id_len = IPDC_TRANS_ID_SIZE;

	if (trans_id == 0) {
	} else {
		h->trans_id = trans_id;
	}
	h->msg_code = ntohs(msg_code);

	return h;
}

struct ipdc_pktdata_t *ipdc_create_tag(uint8_t tag, void *data, uint16_t datalen)
{
	struct ipdc_pktdata_t *d;
	struct ipdc_tags_t t = ipdc_find_tag(tag);

	if (t.tag == IPDC_TAG_UNKNOWN) {
		fprintf(stderr, "ipdc_create_tag: unknown tag passed\n");
		return NULL;
	}

	if (datalen == 0) {
		fprintf(stderr, "ipdc_create_tag: zero length of data passed\n");
		return NULL;
	}

	if (datalen < t.min_len) {
		fprintf(stderr, "ipdc_create_tag: %d bytes of data passed is less than minimum allowed amount of %d bytes\n", datalen, t.min_len);
		return NULL;
	}

	if (datalen > t.max_len) {
		fprintf(stderr, "ipdc_create_tag: %d bytes of data passed is more than maximum allowed amount of %d bytes\n", datalen, t.max_len);
		return NULL;
	}

	d = calloc(1, IPDC_PKTDATA_T_LEN + datalen);
	if (d == NULL) {
		perror("calloc");
		return NULL;
	}

	d->tag = tag;
	d->size = datalen;
	memcpy(d->data, data, datalen);

	return d;
}

char *ipdc_create_pkt(struct ipdc_pkthdr_t *h, struct ipdc_pktdata_t **d, unsigned int taglen, unsigned int *pktlen)
{
	int i;
	uint32_t cp, len;
	char *msg;

	cp = len = 0;

	len = IPDC_PKTHDR_T_LEN;
	for(i = 0; i < taglen; i++) {
		len += IPDC_PKTDATA_T_LEN + d[i]->size;
	}

	len++;
	*pktlen = len;
	h->pkt_len = htons(len);

	msg = calloc(1, len);
	if (msg == NULL) {
		perror("calloc");
		return NULL;
	}

	memcpy(msg, h, IPDC_PKTHDR_T_LEN);
	cp += IPDC_PKTHDR_T_LEN;
	free(h);

	for (i = 0; i < taglen; i++) {
		memcpy(msg + cp, d[i], IPDC_PKTDATA_T_LEN + d[i]->size);
		cp += IPDC_PKTDATA_T_LEN + d[i]->size;
	}

	msg[cp] = '\0';
	return msg;
}

uint32_t ipdc_inc_trans_id(struct ipdc_system_info_t *si)
{
	uint32_t new_tid, sent_tid, rcvd_tid;

	sent_tid = ntohl(si->last_tid_sent) & ~(1 << 31);
	rcvd_tid = ntohl(si->last_tid_rcvd) & ~(1 << 31);
	new_tid = (sent_tid > rcvd_tid ? sent_tid : rcvd_tid) + 1;

	printf("sent = %d, rcvd = %d\n", sent_tid, rcvd_tid);

	return htonl(new_tid);
}

int ipdc_register_msg_handler(uint8_t code, uint8_t stack_pos, int (*func)(char *, uint32_t, struct ipdc_system_info_t *))
{
	struct ipdc_msg_handlers_t *m, *msgh = msgh_head;
	int retval = 0;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	while (msgh != NULL) {
		if (msgh->code == code && msgh->stack_pos == stack_pos) {
			retval = -1;
			goto reader_unlock;
		}
		msgh = msgh->next;
	}

reader_unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	if (retval != 0) {
		return retval;
	}

	m = malloc(sizeof(struct ipdc_msg_handlers_t));
	if (m == NULL) {
		perror("malloc");
		return -1;
	}

	m->code = code;
	m->stack_pos = stack_pos;
	m->func = func;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (msgh_head == NULL) {
		msgh_head = msgh_tail = m;
	} else {
		msgh_tail->next = m;
		msgh_tail = msgh_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

struct ipdc_msg_handlers_t *ipdc_find_msg_handler(uint16_t code)
{
	struct ipdc_msg_handlers_t *msgh = msgh_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while(msgh != NULL) {
		if (msgh->code == code) {
			goto unlock;
		}
		msgh = msgh->next;
	}
	msgh = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return msgh;
}

int ipdc_handle_msg(int sock, void *buf, uint32_t buflen, uint32_t ipaddr, uint32_t trans_id, pthread_t tid)
{
	struct ipdc_msg_handlers_t *msgh = msgh_head;
	struct ipdc_system_info_t *si = NULL;
	struct ipdc_pkthdr_t *h;
	uint8_t code;
	int ret = 0;

	if (buf == NULL) {
		printf("NULL BUFFER POINTER RECEIVED!\n");
		return ret;
	}

	if (msgh == NULL) {
		printf("NO MESSAGE HANDLERS REGISTERED!\n");
		return ret;
	}

	h = (struct ipdc_pkthdr_t *)buf;
	code = ntohs(h->msg_code);

	si = sys_find(ipaddr);
	if (si == NULL) {
		si = sys_add(ipaddr, tid);
		if (si == NULL) {
			printf("COULD NOT CREATE SYSINFO STRUCTURE FOR [%s]\n", inet_ntoa(*((struct in_addr *)&ipaddr)));
			return -127;
		}
		si->sock = sock;
	}
	si->last_tid_rcvd = trans_id;

	msgh = ipdc_find_msg_handler(code);
	if (msgh == NULL) {
		msgh = ipdc_find_msg_handler(IPDC_MSG_UNKNOWN);
		if (msgh == NULL) {
			printf("NO MESSAGE HANDLERS FOUND FOR [%s]\n", ipdc_msgcode_name(code));
		} else {
			printf("NO MESSAGE HANDLERS FOUND FOR [%s], USING DEFAULT\n", ipdc_msgcode_name(code));
			ret = msgh->func(buf, buflen, si);
		}
	} else {
		ret = msgh->func(buf, buflen, si);
	}

	return ret;
}

struct mod_stat_t *mod_find_module(struct ipdc_system_info_t *si, uint16_t module)
{
	struct mod_stat_t *mod = mod_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (mod != NULL) {
		if (mod->si == si && mod->module == module) {
			goto unlock;
		}
		mod = mod->next;
	}
	mod = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	return mod;
}

struct mod_stat_t *mod_find_with_type(struct ipdc_system_info_t *si, uint8_t type)
{
	struct mod_stat_t *mod = mod_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (mod != NULL) {
		if (mod->si == si && mod->type == type) {
			goto unlock;
		}
		mod = mod->next;
	}
	mod = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	return mod;
}

int mod_add_module(struct ipdc_system_info_t *si, uint16_t module, uint8_t type, uint8_t status, uint8_t lines)
{
	struct mod_stat_t *m = NULL;

	if (mod_find_module(si, module) != NULL) {
		return -1;
	}

  m = malloc(sizeof(struct mod_stat_t));
	if (m == NULL) {
		perror("malloc");
		return -1;
	}

	m->si = si;
	m->module = module;
	m->type = type;
	m->status = status;
	m->lines = lines;
	m->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (mod_head == NULL) {
		mod_head = mod_tail = m;
	} else {
		mod_tail->next = m;
		mod_tail = mod_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

int mod_delete_module(struct mod_stat_t *m)
{
	struct mod_stat_t *mod = mod_head;

	if (m == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (mod_head == m && mod_tail == m) {
		free(m);
		mod_head = mod_tail = NULL;
	} else if (mod_head == m) {
		mod_head = mod_head->next;
		free(m);
	} else if (mod_tail == m) {
		while (mod != NULL) {
			if (mod->next == m) {
				mod_tail = mod;
				free(m);
				break;
			}
			mod = mod->next;
		}
	} else {
		while (mod != NULL) {
			if (mod->next == m) {
				mod->next = m->next;
				free(m);
				break;
			}
			mod = mod->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

struct mod_lines_t *mod_find_line(struct ipdc_system_info_t *si, uint16_t module, uint16_t line)
{
	struct mod_stat_t *mod = mod_find_module(si, module);
	struct mod_lines_t *mod_line = mod_line_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	if (mod != NULL) {
		while (mod_line != NULL) {
			if (mod_line->m == mod && mod_line->line == line) {
				goto unlock;
			}
			mod_line = mod_line->next;
		}
	}
	mod_line = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return mod_line;
}

int mod_add_line(struct ipdc_system_info_t *si, uint16_t module, uint16_t line, uint8_t status, uint8_t chans)
{
	struct mod_stat_t *m = NULL;
	struct mod_lines_t *l = NULL;

	m = mod_find_module(si, module);
	if (m == NULL) {
		return -1;
	}

	if (mod_find_line(si, module, line) != NULL) {
		return -1;
	}

  l = malloc(sizeof(struct mod_lines_t));
	if (l == NULL) {
		perror("malloc");
		return -1;
	}

	l->si = si;
	l->m = m;
	l->line = line;
	l->status = status;
	l->chans = chans;
	l->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (mod_line_head == NULL) {
		mod_line_head = mod_line_tail = l;
	} else {
		mod_line_tail->next = l;
		mod_line_tail = mod_line_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

int mod_delete_line(struct mod_lines_t *l)
{
	struct mod_lines_t *line = mod_line_head;

	if (l == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (mod_line_head == l && mod_line_tail == l) {
		free(l);
		mod_line_head = mod_line_tail = NULL;
	} else if (mod_line_head == l) {
		mod_line_head = mod_line_head->next;
		free(l);
	} else if (mod_line_tail == l) {
		while (line != NULL) {
			if (line->next == l) {
				mod_line_tail = line;
				free(l);
				break;
			}
			line = line->next;
		}
	} else {
		while (line != NULL) {
			if (line->next == l) {
				line->next = l->next;
				free(l);
				break;
			}
			line = line->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

struct mod_chans_t *mod_find_chan(struct ipdc_system_info_t *si, uint16_t module, uint16_t line, uint16_t chan)
{
	struct mod_lines_t *mod_line = mod_find_line(si, module, line);
	struct mod_chans_t *mod_chan = mod_chan_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	if (mod_line != NULL) {
		while (mod_chan != NULL) {
			if (mod_chan->si == si && mod_chan->l == mod_line && mod_chan->chan == chan) {
				goto unlock;
			}
			mod_chan = mod_chan->next;
		}
	}
	mod_chan = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return mod_chan;
}

struct mod_chans_t *mod_find_chan_with_status(struct ipdc_system_info_t *si, uint16_t module, uint16_t line, uint8_t status)
{
	struct mod_chans_t *mod_chan = mod_chan_head;
 
	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (mod_chan != NULL) {
		if (module == 0 && line == 0) {
			if (mod_chan->si == si && mod_chan->status == status) {
				goto unlock;
			}
		} else if (module > 0 && line == 0) {
			if (mod_chan->si == si && mod_chan->l->m->module == module && mod_chan->status == status) {
				goto unlock;
			}
		} else {
			if (mod_chan->si == si && mod_chan->l->m->module == module && mod_chan->l->line == line && mod_chan->status == status) {
				goto unlock;
			}
		}		
		mod_chan = mod_chan->next;
	}
	mod_chan = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return mod_chan;
}

struct mod_chans_t *mod_add_chan(struct ipdc_system_info_t *si, uint16_t module, uint16_t line, uint16_t chan, uint8_t status)
{
	struct mod_lines_t *l = NULL;
	struct mod_chans_t *c = NULL;

	l = mod_find_line(si, module, line);
	if (l == NULL) {
		return NULL;
	}

  c = malloc(sizeof(struct mod_chans_t));
	if (c == NULL) {
		perror("malloc");
		return NULL;
	}

	c->si = si;
	c->l = l;
	c->chan = chan;
	c->status = status;
	c->tid = 0;
	c->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return NULL;
	}

	if (mod_chan_head == NULL) {
		mod_chan_head = mod_chan_tail = c;
	} else {
		mod_chan_tail->next = c;
		mod_chan_tail = mod_chan_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return c;
}

int mod_delete_chan(struct mod_chans_t *c)
{
	struct mod_chans_t *chan = mod_chan_head;

	if (c == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (mod_chan_head == c && mod_chan_tail == c) {
		free(c);
		mod_chan_head = mod_chan_tail = NULL;
	} else if (mod_chan_head == c) {
		mod_chan_head = mod_chan_head->next;
		free(c);
	} else if (mod_chan_tail == c) {
		while (chan != NULL) {
			if (chan->next == c) {
				mod_chan_tail = chan;
				free(c);
				break;
			}
			chan = chan->next;
		}
	} else {
		while (chan != NULL) {
			if (chan->next == c) {
				chan->next = c->next;
				free(c);
				break;
			}
			chan = chan->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

struct call_ref_t *cr_find_callref_by_ref(uint16_t callref, struct mod_chans_t *chan, uint8_t chantype)
{
	struct call_ref_t *cr = call_ref_head;

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	if (chantype == CALL_REF_SIGNALING_CHAN) {
		while (cr != NULL) {
			if (cr->callref == callref && cr->sig == chan) {
				goto unlock;
			}
			cr = cr->next;
		}
	} else if (chantype == CALL_REF_CALL_CHAN) {
		while (cr != NULL) {
			if (cr->callref == callref && cr->call == chan) {
				goto unlock;
			}
			cr = cr->next;
		}
	}
	cr = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return cr;
}

struct call_ref_t *cr_find_callref_by_call(struct mod_chans_t *chan)
{
	struct call_ref_t *call_ref = call_ref_head;

	if (chan == NULL) {
		return NULL;
	}

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (call_ref != NULL) {
		if (call_ref->call == chan) {
			goto unlock;
		}

		call_ref = call_ref->next;
	}
	call_ref = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return call_ref;
}

struct call_ref_t *cr_find_callref_by_sig(struct mod_chans_t *chan)
{
	struct call_ref_t *call_ref = call_ref_head;

	if (chan == NULL) {
		return NULL;
	}

	if (pthread_rwlock_rdlock(&rwlock) != 0) {
		perror("pthread_rwlock_rdlock");
		return NULL;
	}

	while (call_ref != NULL) {
		if (call_ref->sig == chan) {
			goto unlock;
		}

		call_ref = call_ref->next;
	}
	call_ref = NULL;

unlock:
	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return call_ref;
}

struct call_ref_t *cr_add_call_ref(struct mod_chans_t *sig, struct mod_chans_t *call, uint16_t callref, uint8_t state)
{
	struct call_ref_t *cr = NULL;
	struct mod_chans_t *chan = NULL;

	if (sig == NULL) {
		return NULL;
	}

	if (cr_find_callref_by_ref(callref, sig, CALL_REF_SIGNALING_CHAN) != NULL) {
		printf("CALL REFERENCE ALREADY EXISTS ON SIGNALING CHANNEL 1/%d/%d/%d\n", sig->l->m->module, sig->l->line, sig->chan);
		return NULL;
	}

	if (call != NULL) {
		if (cr_find_callref_by_ref(callref, call, CALL_REF_CALL_CHAN) != NULL) {
			printf("CALL REFERENCE ALREADY EXISTS ON TRUNK CHANNEL 1/%d/%d/%d\n", call->l->m->module, call->l->line, call->chan);
			return NULL;
		}
	}

	chan = mod_find_chan(sig->si, sig->l->m->module, sig->l->line, sig->chan);
	if (chan == NULL) {
		return NULL;
	}

	if (call != NULL) {
		if (sig->si != call->si) {
			return NULL;
		}

		chan = mod_find_chan(call->si, call->l->m->module, call->l->line, call->chan);
		if (chan == NULL) {
			return NULL;
		}
	}

	cr = malloc(sizeof(struct call_ref_t));
	if (cr == NULL) {
		perror("malloc");
		return NULL;
	}

	cr->sig = sig;
	cr->call = call;
	cr->callref = callref;
	cr->state = state;
	cr->next = NULL;

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return NULL;
	}

	if (call_ref_head == NULL) {
		call_ref_head = call_ref_tail = cr;
	} else {
		call_ref_tail->next = cr;
		call_ref_tail = call_ref_tail->next;
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return NULL;
	}

	return cr;
}

int cr_remove_call_ref(struct call_ref_t *cr)
{
	struct call_ref_t *c = call_ref_head;

	if (cr == NULL) {
		return -1;
	}

	if (pthread_rwlock_wrlock(&rwlock) != 0) {
		perror("pthread_rwlock_wrlock");
		return -1;
	}

	if (cr == call_ref_head && cr == call_ref_tail) {
		free(cr);
		call_ref_head = call_ref_tail = NULL;
	} else if (cr == call_ref_head) {
		call_ref_head = call_ref_head->next;
		free(cr);
	} else if (cr == call_ref_tail) {
		while (c != NULL) {
			if (c->next == cr) {
				call_ref_tail = c;
				free(cr);
				break;
			}
			c = c->next;
		}
	} else {
		while (c != NULL) {
			if (c->next == cr) {
				c->next = cr->next;
				free(cr);
				break;
			}
			c = c->next;
		}
	}

	if (pthread_rwlock_unlock(&rwlock) != 0) {
		perror("pthread_rwlock_unlock");
		return -1;
	}

	return 0;
}

int ipdc_send_rsi(struct ipdc_system_info_t *si)
{
	uint32_t pktlen = 0;
	uint32_t trans_id;
	struct ipdc_pkthdr_t *h;
	char *msg;
	int nwrite;

	trans_id = ipdc_inc_trans_id(si);

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_RSI);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, NULL, 0, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(si, msg, pktlen)) < 0) {
		return -1;
	}

	free(msg);

	return nwrite;
}

int ipdc_send_rms(struct ipdc_system_info_t *si, uint32_t trans_id, uint16_t module)
{
	uint32_t pktlen = 0;
	uint16_t m = htons(module);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[1];
	char *msg;
	int nwrite;

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_RMS);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	d[0] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &m, sizeof(m));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 1, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(si, msg, pktlen)) < 0) {
		return -1;
	}

	printf("Sent a RMS of %d bytes for 1/%d\n", nwrite, module);
	free(msg);

	return nwrite;
}

int ipdc_send_rls(struct ipdc_system_info_t *si, uint32_t trans_id, uint16_t module, uint16_t line)
{
	uint32_t pktlen = 0;
	uint16_t m = htons(module);
	uint16_t l = htons(line);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[2];
	char *msg;
	int nwrite;

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_RLS);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	d[0] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &m, sizeof(m));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	d[1] = ipdc_create_tag(IPDC_TAG_LINE_NUMBER, &l, sizeof(l));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (line number)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 2, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(si, msg, pktlen)) < 0) {
		return -1;
	}

	// printf("Sent a RLS of %d bytes for 1/%d/%d\n", nwrite, module, line);
	free(msg);

	return nwrite;
}

int ipdc_send_rcs(struct ipdc_system_info_t *si, uint32_t trans_id, uint16_t module, uint16_t line, uint16_t chan)
{
	uint32_t pktlen = 0;
	uint16_t m = htons(module);
	uint16_t l = htons(line);
	uint16_t c = htons(chan);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[3];
	char *msg;
	int nwrite;

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_RCS);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	d[0] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &m, sizeof(m));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	d[1] = ipdc_create_tag(IPDC_TAG_LINE_NUMBER, &l, sizeof(l));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (line number)\n");
		return -1;
	}

	d[2] = ipdc_create_tag(IPDC_TAG_CHANNEL_NUMBER, &c, sizeof(c));
	if (d[2] == NULL) {
		fprintf(stderr, "could not create tag (channel number)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 3, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(si, msg, pktlen)) < 0) {
		return -1;
	}

	printf("Sent a RCS of %d bytes for 1/%d/%d/%d\n", nwrite, module, line, chan);
	free(msg);

	return nwrite;
}

int ipdc_send_rars(struct ipdc_system_info_t *si, uint32_t ip)
{
	uint32_t pktlen = 0;
	uint32_t trans_id;
	uint32_t i = htons(ip);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[1];
	char *msg;
	int nwrite;

	if (ip == 0) {
		return -1;
	}

	trans_id = ipdc_inc_trans_id(si);

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_RARS);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	d[0] = ipdc_create_tag(IPDC_TAG_DEST_SEND_IPADDR, &i, sizeof(i));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (destination listen ip address)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 1, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(si, msg, pktlen)) < 0) {
		return -1;
	}

	free(msg);

	return nwrite;
}

int ipdc_send_rrs(struct ipdc_system_info_t *si, uint32_t ip, uint16_t port)
{
	uint32_t pktlen = 0;
	uint32_t trans_id;
	uint32_t i = htons(ip);
	uint16_t p = htons(port);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[2];
	char *msg;
	int nwrite;

	if (ip == 0 || port == 0) {
		return -1;
	}

	trans_id = ipdc_inc_trans_id(si);

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_RRS);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	d[0] = ipdc_create_tag(IPDC_TAG_DEST_LISTEN_IPADDR, &i, sizeof(i));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (destination listen ip address)\n");
		return -1;
	}

	d[1] = ipdc_create_tag(IPDC_TAG_DEST_LISTEN_PORT, &p, sizeof(p));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (destination listen port)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 2, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(si, msg, pktlen)) < 0) {
		return -1;
	}

	free(msg);

	return nwrite;
}

int ipdc_send_sms(struct ipdc_system_info_t *si, uint32_t trans_id, uint16_t module, uint8_t status)
{
	uint32_t pktlen = 0;
	uint16_t m = htons(module);
	uint8_t s = htons(status);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[2];
	char *msg;
	int nwrite;

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_SMS);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	d[0] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &m, sizeof(m));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	d[1] = ipdc_create_tag(IPDC_TAG_REQ_MOD_STATE, &s, sizeof(s));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (requested module state)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 2, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(si, msg, pktlen)) < 0) {
		return -1;
	}

	printf("Sent a SMS of %d bytes for 1/%d\n", nwrite, module);
	free(msg);

	return nwrite;
}

int ipdc_send_sls(struct ipdc_system_info_t *si, uint32_t trans_id, uint16_t module, uint16_t line, uint8_t status)
{
	uint32_t pktlen = 0;
	uint16_t m = htons(module);
	uint16_t l = htons(line);
	uint8_t s = htons(status);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[3];
	char *msg;
	int nwrite;

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_SLS);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	d[0] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &m, sizeof(m));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	d[1] = ipdc_create_tag(IPDC_TAG_LINE_NUMBER, &l, sizeof(l));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (line number)\n");
		return -1;
	}

	d[2] = ipdc_create_tag(IPDC_TAG_REQ_LINE_STATE, &s, sizeof(s));
	if (d[2] == NULL) {
		fprintf(stderr, "could not create tag (requested line state)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 3, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(si, msg, pktlen)) < 0) {
		return -1;
	}

	printf("Sent a SLS of %d bytes for 1/%d/%d\n", nwrite, module, line);
	free(msg);

	return nwrite;
}

int ipdc_send_scs(struct mod_chans_t *start, struct mod_chans_t *end, uint8_t status, uint8_t valid_only)
{
	uint32_t pktlen = 0;
	uint16_t m, l, cs, ce;
	uint8_t s = htons(status);
	uint8_t v = htons(valid_only);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[6];
	char *msg;
	int nwrite;
	uint32_t trans_id;

	if (start == NULL) {
		fprintf(stderr, "starting channel cannot be null\n");
		return -1;
	}

	trans_id = ipdc_inc_trans_id(start->si);

	if (end != NULL && (start->si != end->si)) {
		fprintf(stderr, "channels from different chassis passed!\n");
		return -1;
	}

	if (end != NULL && (start->l->m->module != end->l->m->module || start->l->line != end->l->line || start->chan < end->chan)) {
		fprintf(stderr, "invalid channels passed\n");
		return -1;
	}

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_SCS);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	m = htons(start->l->m->module);
	d[0] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &m, sizeof(m));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	l = htons(start->l->line);
	d[1] = ipdc_create_tag(IPDC_TAG_LINE_NUMBER, &l, sizeof(l));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (line number)\n");
		return -1;
	}

	cs = htons(start->chan);
	d[2] = ipdc_create_tag(IPDC_TAG_CHANNEL_NUMBER, &cs, sizeof(cs));
	if (d[2] == NULL) {
		fprintf(stderr, "could not create tag (start channel number)\n");
		return -1;
	}

	if (end == NULL) {
		ce = htons(start->chan);
	} else {
		ce = htons(end->chan);
	}
	d[3] = ipdc_create_tag(IPDC_TAG_CHANGRP_LAST, &ce, sizeof(ce));
	if (d[3] == NULL) {
		fprintf(stderr, "could not create tag (end channel number)\n");
		return -1;
	}

	d[4] = ipdc_create_tag(IPDC_TAG_REQ_CHANSTAT_ACTION, &s, sizeof(s));
	if (d[4] == NULL) {
		fprintf(stderr, "could not create tag (requested channel status)\n");
		return -1;
	}

	d[5] = ipdc_create_tag(IPDC_TAG_SET_CHANSTAT_OPTION, &v, sizeof(v));
	if (d[5] == NULL) {
		fprintf(stderr, "could not create tag (set channel status option)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 6, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(start->si, msg, pktlen)) < 0) {
		return -1;
	}

	// printf("Sent a SCS of %d bytes for %s:1/%d/%d/%d-%d\n", nwrite, inet_ntoa(*((struct in_addr *)&start->si->ip)), start->l->m->module, start->l->line, start->chan, end == NULL ? start->chan : end->chan);
	free(msg);

	return nwrite;
}

int ipdc_send_rcsi(struct mod_chans_t *c, uint8_t bearer_cap, char *ani, char *dnis, uint8_t auth)
{
	uint32_t pktlen = 0;
	uint16_t m, l, chan;
	uint8_t b = htons(bearer_cap);
	uint8_t a = htons(auth);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[7];
	char *msg;
	int nwrite;
	uint32_t trans_id;

	if (c == NULL) {
		return -1;
	}

	// trans_id = c->si->last_tid_rcvd;
	trans_id = ipdc_inc_trans_id(c->si);

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_RCSI);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	m = htons(c->l->m->module);
	d[0] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &m, sizeof(m));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	l = htons(c->l->line);
	d[1] = ipdc_create_tag(IPDC_TAG_LINE_NUMBER, &l, sizeof(l));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (line number)\n");
		return -1;
	}

	chan = htons(c->chan);
	d[2] = ipdc_create_tag(IPDC_TAG_CHANNEL_NUMBER, &chan, sizeof(chan));
	if (d[2] == NULL) {
		fprintf(stderr, "could not create tag (channel number)\n");
		return -1;
	}

	d[3] = ipdc_create_tag(IPDC_TAG_BEARER_CAP, &b, sizeof(b));
	if (d[3] == NULL) {
		fprintf(stderr, "could not create tag (bearer capability)\n");
		return -1;
	}

	d[4] = ipdc_create_tag(IPDC_TAG_CALLING_NUMBER, ani, ani == NULL ? 0 : strlen(ani));
	if (d[4] == NULL) {
		fprintf(stderr, "could not create tag (calling number)\n");
		return -1;
	}

	d[5] = ipdc_create_tag(IPDC_TAG_CALLED_NUMBER, dnis, strlen(dnis));
	if (d[5] == NULL) {
		fprintf(stderr, "could not create tag (called number)\n");
		return -1;
	}

	d[6] = ipdc_create_tag(IPDC_TAG_RADIUS_ASCEND_AUTHTYPE, &a, sizeof(a));
	if (d[6] == NULL) {
		fprintf(stderr, "could not create tag (radius ascend authtype)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 7, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(c->si, msg, pktlen)) < 0) {
		return -1;
	}

	c->tid = h->trans_id;
	printf("Sent a RCSI of %d bytes for %s:1/%d/%d/%d\n", nwrite, inet_ntoa(*((struct in_addr *)&c->si->ip)), c->l->m->module, c->l->line, c->chan);
	free(msg);

	return nwrite;
}

int ipdc_send_rcst(struct mod_chans_t *src, struct mod_chans_t *dst, uint8_t bearer_cap, char *ani, char *dnis)
{
	uint32_t pktlen = 0;
	uint16_t sm, sl, schan;
	uint16_t dm, dl, dchan;
	uint8_t b = htons(bearer_cap);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[9];
	char *msg;
	int nwrite;
	uint32_t trans_id;

	if (src == NULL || dst == NULL || src->si != dst->si) {
		return -1;
	}

	trans_id = ipdc_inc_trans_id(src->si);

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_RCST);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	sm = htons(src->l->m->module);
	d[0] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &sm, sizeof(sm));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	sl = htons(src->l->line);
	d[1] = ipdc_create_tag(IPDC_TAG_LINE_NUMBER, &sl, sizeof(sl));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (line number)\n");
		return -1;
	}

	schan = htons(src->chan);
	d[2] = ipdc_create_tag(IPDC_TAG_CHANNEL_NUMBER, &schan, sizeof(schan));
	if (d[2] == NULL) {
		fprintf(stderr, "could not create tag (channel number)\n");
		return -1;
	}

	d[3] = ipdc_create_tag(IPDC_TAG_BEARER_CAP, &b, sizeof(b));
	if (d[3] == NULL) {
		fprintf(stderr, "could not create tag (bearer capability)\n");
		return -1;
	}

	dm = htons(dst->l->m->module);
	d[4] = ipdc_create_tag(IPDC_TAG_TDM_DEST_MOD, &dm, sizeof(dm));
	if (d[4] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	dl = htons(dst->l->line);
	d[5] = ipdc_create_tag(IPDC_TAG_TDM_DEST_LINE, &dl, sizeof(dl));
	if (d[5] == NULL) {
		fprintf(stderr, "could not create tag (line number)\n");
		return -1;
	}

	dchan = htons(dst->chan);
	d[6] = ipdc_create_tag(IPDC_TAG_TDM_DEST_CHAN, &dchan, sizeof(dchan));
	if (d[6] == NULL) {
		fprintf(stderr, "could not create tag (channel number)\n");
		return -1;
	}

	d[7] = ipdc_create_tag(IPDC_TAG_CALLING_NUMBER, ani, ani == NULL ? 0 : strlen(ani));
	if (d[7] == NULL) {
		fprintf(stderr, "could not create tag (calling number)\n");
		return -1;
	}

	d[8] = ipdc_create_tag(IPDC_TAG_CALLED_NUMBER, dnis, strlen(dnis));
	if (d[8] == NULL) {
		fprintf(stderr, "could not create tag (called number)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 9, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(src->si, msg, pktlen)) < 0) {
		return -1;
	}

	src->tid = trans_id;
	printf("Sent a RCST of %d bytes to %s for 1/%d/%d/%d --> 1/%d/%d/%d\n", nwrite, inet_ntoa(*((struct in_addr *)&src->si->ip)), src->l->m->module, src->l->line, src->chan, dst->l->m->module, dst->l->line, dst->chan);
	free(msg);

	return nwrite;
}

int ipdc_send_acr(struct ipdc_system_info_t *si, uint32_t trans_id, uint16_t module, uint16_t line, uint16_t chan, uint8_t cause)
{
	uint32_t pktlen = 0;
	uint16_t m = htons(module);
	uint16_t l = htons(line);
	uint16_t c = htons(chan);
	uint8_t cc = htons(cause);
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[4];
	char *msg;
	int nwrite;

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_ACR);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	d[0] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &m, sizeof(m));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	d[1] = ipdc_create_tag(IPDC_TAG_LINE_NUMBER, &l, sizeof(l));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (line number)\n");
		return -1;
	}

	d[2] = ipdc_create_tag(IPDC_TAG_CHANNEL_NUMBER, &c, sizeof(c));
	if (d[2] == NULL) {
		fprintf(stderr, "could not create tag (channel number)\n");
		return -1;
	}

	d[3] = ipdc_create_tag(IPDC_TAG_CAUSE_CODE, &cc, sizeof(cc));
	if (d[3] == NULL) {
		fprintf(stderr, "could not create tag (cause code)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 4, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(si, msg, pktlen)) < 0) {
		return -1;
	}

	printf("Sent a ACR of %d bytes for 1/%d/%d/%d\n", nwrite, module, line, chan);
	free(msg);

	return nwrite;
}

int ipdc_send_rcr(struct mod_chans_t *c, uint8_t cause)
{
	uint32_t pktlen = 0;
	uint16_t m, l, chan;
	// uint8_t cc = htons(cause);
	uint8_t cc = cause;
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[4];
	char *msg;
	int nwrite;
	uint32_t trans_id;

	if (c == NULL) {
		fprintf(stderr, "was passed null channel structure!\n");
		return -1;
	}

	if (c->tid > 0) {
		trans_id = htonl(ntohl(c->tid) & ~(1 << 31));
		printf("REUSING TRANSACTION ID %d\n", ntohl(trans_id));
	} else {
		trans_id = ipdc_inc_trans_id(c->si);
	}


	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_RCR);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		return -1;
	}

	m = htons(c->l->m->module);
	d[0] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &m, sizeof(m));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (module number)\n");
		return -1;
	}

	l = htons(c->l->line);
	d[1] = ipdc_create_tag(IPDC_TAG_LINE_NUMBER, &l, sizeof(l));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (line number)\n");
		return -1;
	}

	chan = htons(c->chan);
	d[2] = ipdc_create_tag(IPDC_TAG_CHANNEL_NUMBER, &chan, sizeof(chan));
	if (d[2] == NULL) {
		fprintf(stderr, "could not create tag (channel number)\n");
		return -1;
	}

	d[3] = ipdc_create_tag(IPDC_TAG_CAUSE_CODE, &cc, sizeof(cc));
	if (d[3] == NULL) {
		fprintf(stderr, "could not create tag (cause code)\n");
		return -1;
	}

	msg = ipdc_create_pkt(h, d, 4, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create data packet\n");
		return -1;
	}

	if ((nwrite = ipdc_write_pkt(c->si, msg, pktlen)) < 0) {
		return -1;
	}

	printf("Sent a RCR of %d bytes for %s:1/%d/%d/%d\n", nwrite, inet_ntoa(*((struct in_addr *)&c->si->ip)), c->l->m->module, c->l->line, c->chan);
	free(msg);

	c->tid = 0;

	return nwrite;
}

struct ipdc_q931_elem_t *ipdc_q931_create_ie(uint8_t ie, void *data, uint32_t datalen)
{
	struct ipdc_q931_elem_t *elem = NULL;

	elem = calloc(1, IPDC_Q931_ELEM_T_LEN + datalen);
	if (elem == NULL) {
		perror("calloc");
		return NULL;
	}

	elem->ie = ie;
	elem->len = datalen;
	memcpy(elem->data, data, datalen);

	return elem;
}

void *ipdc_q931_decode_ie(struct ipdc_q931_elem_t *ie)
{
	uint32_t n, o;
	/* uint32_t n32, *n32p = (uint32_t *)&n32;
	uint16_t n16, *n16p = (uint16_t *)&n16; */
	uint8_t n8, *n8p = (uint8_t *)&n8;

	switch(ie->ie) {
		case IPDC_Q931_IE_CHANNEL_ID:
			memcpy(&n, ie->data, 1);

			printf("  info channel selection: %s\n", ipdc_find_q931_info_chan(n & IPDC_Q931_INFO_CHAN_MASK));
			printf("  d channel selection: %s\n", ipdc_find_q931_dchan(n & IPDC_Q931_DCHAN_MASK));
			printf("  preferred or exclusive: %s\n", ipdc_find_q931_prefexcl(n & IPDC_Q931_PREFEXCL_MASK));
			printf("  interface type: %s\n", ipdc_find_q931_int_type(n & IPDC_Q931_INT_TYPE_MASK));
			printf("  interface id present: %s\n", ipdc_find_q931_int_id_present(n & IPDC_Q931_INT_ID_PRESENT_MASK));

			if ((n & IPDC_Q931_INT_ID_PRESENT_MASK) == IPDC_Q931_INT_ID_PRESENT_EXPLICIT) {
				memcpy(&n8, ie->data + 1, 1);
				n8 -= 128;
				memcpy(&o, ie->data + 2, 1);
			} else {
				memcpy(&n8, ie->data + 2, 1);
				n8 -= 128;
				memcpy(&o, ie->data + 1, 1);
			}

			printf("  channel/map type: %s\n", ipdc_find_q931_chanmap_type(o & IPDC_Q931_CHANMAP_TYPE_MASK));
			printf("  channel or map: %s\n", ipdc_find_q931_chanmap_num(o & IPDC_Q931_CHANMAP_NUM_MASK));
			printf("  coding standard: %s\n", ipdc_find_q931_coding_std(o & IPDC_Q931_CODING_STD_MASK));
			printf("  ds0 number: %d\n", n8);

			return n8p;
			break;
		default:
			return NULL;
	}

	/* NOTREACHED */
}

struct ipdc_q931_elem_t *ipdc_q931_chg_channel_id(struct ipdc_q931_elem_t *ie, uint8_t chan)
{
	uint8_t newchan;
	uint32_t n;

	if (chan == 0) {
		return NULL;
	}
	newchan = chan + 128;

	if (ie == NULL) {
		return NULL;
	}

	if (ie->ie != IPDC_Q931_IE_CHANNEL_ID) {
		return NULL;
	}

	memcpy(&n, ie->data, 1);
	if ((n & IPDC_Q931_INT_ID_PRESENT_MASK) == IPDC_Q931_INT_ID_PRESENT_EXPLICIT) {
		memcpy(ie->data + 1, &newchan, 1);
	} else {
		memcpy(ie->data + 2, &newchan, 1);
	}

	return ie;
}

int ipdc_send_q931_msg(uint16_t callref, uint8_t msgcode, struct mod_chans_t *c, struct ipdc_q931_elem_t **ie, uint16_t ie_len)
{
	struct ipdc_pkthdr_t *h;
	struct ipdc_pktdata_t *d[6];
	uint8_t src_port_type = 0;
	uint16_t mod_num;
	uint16_t line_num;
	uint16_t chan_num;
	uint32_t pktlen, len = 0;
	uint32_t trans_id;
	uint16_t sig_proto = IPDC_Q931_SIG_PROTO_Q931;
	struct ipdc_q931_t *q = NULL;
	char *msg = NULL;
	int nwrite;
	int retval = 0;
	int i;

	if (c == NULL) {
		return -1;
	}

	if (ipdc_find_q931_msg(msgcode).msg == IPDC_Q931_MSG_UNKNOWN) {
		return -1;
	}

	trans_id = ipdc_inc_trans_id(c->si);

	h = ipdc_create_pkthdr(trans_id, IPDC_MSG_TUNL);
	if (h == NULL) {
		fprintf(stderr, "could not create packet header\n");
		goto free_all;
	}

	src_port_type = htons(src_port_type);
	d[0] = ipdc_create_tag(IPDC_TAG_SRC_PORT_TYPE, &src_port_type, sizeof(src_port_type));
	if (d[0] == NULL) {
		fprintf(stderr, "could not create tag (source port type)\n");
		retval = -1;
		goto free_all;
	}

	mod_num = htons(c->l->m->module);
	d[1] = ipdc_create_tag(IPDC_TAG_MODULE_NUMBER, &mod_num, sizeof(mod_num));
	if (d[1] == NULL) {
		fprintf(stderr, "could not create tag (source module number)\n");
		retval = -1;
		goto free_all;
	}

	line_num = htons(c->l->line);
	d[2] = ipdc_create_tag(IPDC_TAG_LINE_NUMBER, &line_num, sizeof(line_num));
	if (d[2] == NULL) {
		fprintf(stderr, "could not create tag (source line number)\n");
		retval = -1;
		goto free_all;
	}

	chan_num = htons(c->chan);
	d[3] = ipdc_create_tag(IPDC_TAG_CHANNEL_NUMBER, &chan_num, sizeof(chan_num));
	if (d[3] == NULL) {
		fprintf(stderr, "could not create tag (source channel number)\n");
		retval = -1;
		goto free_all;
	}

	sig_proto = htons(sig_proto);
	d[4] = ipdc_create_tag(IPDC_TAG_SIGNALING_TYPE, &sig_proto, sizeof(sig_proto));
	if (d[4] == NULL) {
		fprintf(stderr, "could not create tag (signaling type)\n");
		retval = -1;
		goto free_all;
	}

	q = calloc(1, IPDC_Q931_HDR_LEN);
	if (q == NULL) {
		perror("calloc");
		retval = -1;
		goto free_all;
	}

	q->pd = IPDC_Q931_PROTO_Q931;
	q->crlen = 2;
	q->callref = callref;
	q->msgtype = msgcode;

	if (ie_len > 0) {
		if (ie == NULL) {
			retval = -1;
			goto free_all;
		} else if (ie_len == 1) {
			len = ie[0]->len + IPDC_Q931_ELEM_T_LEN;
			memcpy(q->ie, ie[0], len);
		} else {
			for (i = 0; i < ie_len; i++) {
				memcpy(q->ie + len, ie[i], ie[i]->len + IPDC_Q931_ELEM_T_LEN);
				len += ie[i]->len + IPDC_Q931_ELEM_T_LEN;
			}
		}

		d[5] = ipdc_create_tag(IPDC_TAG_PDU, q, IPDC_Q931_HDR_LEN + len);
	} else {
		d[5] = ipdc_create_tag(IPDC_TAG_PDU, q, IPDC_Q931_HDR_LEN);
	}

	if (d[5] == NULL) {
		fprintf(stderr, "could not create tag (pdu)\n");
		retval = -1;
		goto free_all;
	}

	h->trans_id = trans_id;
	msg = ipdc_create_pkt(h, d, 6, &pktlen);
	if (msg == NULL) {
		fprintf(stderr, "could not create final paclet\n");
		retval = -1;
		goto free_all;
	}

	nwrite = ipdc_write_pkt(c->si, msg, pktlen);
	if (nwrite < 0) {
		retval = -1;
		goto free_all;
	}

	printf("Sent a TUNL(%s) message of %d bytes\n", (ipdc_find_q931_msg(msgcode).descr), nwrite);

free_all:
	h = NULL;
	q = NULL;
	msg = NULL;
	d[0] = d[1] = d[2] = d[3] = d[4] = d[5] = d[6] = NULL;

	return retval;
}


uint16_t ipdc_q931_flip_callref(uint16_t callref)
{
	if ((ntohs(callref) & IPDC_Q931_CALLREF_MASK) == IPDC_Q931_CALLREF_ORIG) {
		return callref | IPDC_Q931_CALLREF_TERM;
	}

	return callref | IPDC_Q931_CALLREF_ORIG;
}

int ipdc_q931_send_connect(struct call_ref_t *cr, struct ipdc_q931_elem_t **ie, uint8_t ie_len)
{
	if (cr == NULL || cr->sig == NULL || cr->sig->si == NULL || cr->call == NULL || cr->call->si == NULL) {
		return -1;
	}

	return ipdc_send_q931_msg(cr->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_MSG_CONNECT, cr->sig, ie, ie_len);
}

int ipdc_q931_send_release_complete(struct mod_chans_t *c, uint16_t callref, uint8_t cause)
{
	struct ipdc_q931_elem_t *e;
	unsigned char cc[2];
	int nwrite;

	cc[0] = 0;
	cc[1] = cause & IPDC_Q931_CAUSE_CODE_MASK;

	e = ipdc_q931_create_ie(IPDC_Q931_IE_CAUSE, &cc, sizeof(cc));
	if (e == NULL) {
		fprintf(stderr, "could not create information element (cause code)\n");
		return -1;
	}

	nwrite = ipdc_send_q931_msg(callref, IPDC_Q931_MSG_RELEASE_COMP, c, &e, 1);

	e = NULL;
	return nwrite;
}

int ipdc_q931_send_status(struct mod_chans_t *c, uint16_t callref, uint8_t cause, uint8_t state)
{
	struct ipdc_q931_elem_t *e[2];
	unsigned char cc[2];
	uint8_t cs = state;
	int nwrite;

	cc[0] = 0;
	cc[1] = cause & IPDC_Q931_CAUSE_CODE_MASK;

	e[0] = ipdc_q931_create_ie(IPDC_Q931_IE_CAUSE, &cc, sizeof(cc));
	if (e[0] == NULL) {
		fprintf(stderr, "could not create information element (cause code)\n");
		return -1;
	}

	e[1] = ipdc_q931_create_ie(IPDC_Q931_IE_CALL_STATE, &cs, sizeof(cs));
	if (e[1] == NULL) {
		fprintf(stderr, "could not create information element (call state)\n");
		return -1;
	}

	nwrite = ipdc_send_q931_msg(callref, IPDC_Q931_MSG_STATUS, c, e, 2);

	e[0] = e[1] = NULL;
	return nwrite;
}

int msg_handler_SYSINFO(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m, *h;
	struct ipdc_pktdata_t *d[2], *tag = NULL;
	uint32_t cp, pktlen, n;
	uint16_t code;
	char *msg;
	int nwrite;
	int len = IPDC_PKTHDR_T_LEN;

	cp = pktlen = 0;

	m = (struct ipdc_pkthdr_t *)buf;
	code = ntohs(m->msg_code);
	printf("Got a %s message of %d bytes\n", ipdc_msgcode_name(code), ntohs(m->pkt_len));

	if (code == IPDC_MSG_NSUP) {
		if (si->id[0] != '\0') {
			printf("ERROR: NSUP MESSAGE ALREADY RECEIVED (%s)\n", si->id);
			return -1;
		}

		/* reset # of nms messages received */
		si->nms_rcvd = 0;
	} else if (code == IPDC_MSG_NSI) {
		if (si->id[0] == '\0') {
			printf("ERROR: NSI MESSAGE WITH NO PRE-EXISTING NSUP\n");
			return -1;
		}
	}

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_PROTO_VERSION:
				memcpy(&n, tag->data, sizeof(n));
				si->version = ntohs(n) >> 8;
				break;
			case IPDC_TAG_SYSTEM_ID:
				if (tag->size > sizeof(si->id)) {
					printf("ERROR: SYSTEM ID IS TOO LARGE\n");
					return -1;
				}
				memcpy(si->id, tag->data, sizeof(si->id));
				si->id[tag->size] = '\0';
				break;
			case IPDC_TAG_SYSTEM_TYPE:
				if (tag->size > sizeof(si->type)) {
					printf("ERROR: SYSTEM TYPE IS TOO LARGE\n");
					return -1;
				}
				memcpy(si->type, tag->data, sizeof(si->type));
				si->type[tag->size] = '\0';
				break;
			case IPDC_TAG_MAX_MODULES:
				memcpy(&n, tag->data, sizeof(n));
				si->modules = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_BAY_NUMBER:
				if (tag->size > sizeof(si->bay)) {
					printf("ERROR: SYSTEM BAY IS TOO LARGE\n");
					return -1;
				}
				memcpy(si->bay, tag->data, sizeof(si->bay));
				si->bay[tag->size] = '\0';
				break;
			case IPDC_TAG_COUNTRY_CODE:
				memcpy(&n, tag->data, sizeof(n));
				si->country = ntohs(n) >> 8;
				break;
			case IPDC_TAG_OPERATIONAL_UNIV_PORTS:
				memcpy(&n, tag->data, sizeof(n));
				si->u_ports = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_OPERATIONAL_HDLC_PORTS:
				memcpy(&n, tag->data, sizeof(n));
				si->h_ports = ntohl(n) >> (tag->size * 8);
				break;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x\n", tag->tag);
		}

		free(tag);
	}

	if (code == IPDC_MSG_NSUP) {
		h = ipdc_create_pkthdr(m->trans_id, IPDC_MSG_ASUP);
		if (h == NULL) {
			fprintf(stderr, "could not create packet header\n");
			return -1;
		}

		d[0] = ipdc_create_tag(IPDC_TAG_PROTO_VERSION, &si->version, sizeof(si->version));
		if (d[0] == NULL) {
			fprintf(stderr, "could not create data packet (protocol version)\n");
			return -1;
		}

		d[1] = ipdc_create_tag(IPDC_TAG_SYSTEM_ID, si->id, strlen(si->id));
		if (d[1] == NULL) {
			fprintf(stderr, "could not create data packet (system id)\n");
			return -1;
		}

		msg = ipdc_create_pkt(h, d, 2, &pktlen);
		if (msg == NULL) {
			fprintf(stderr, "could not create final packet\n");
			return -1;
		}

		if ((nwrite = ipdc_write_pkt(si, msg, pktlen)) < 0) {
			return -1;
		}

		printf("Sent a ASUP message of %d bytes\n", nwrite);
		free(msg);

		if (ipdc_send_rars(si, 0) < 0) {
			return -1;
		}
	}

	return 0;
}

int msg_handler_MRJ(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;
	struct ipdc_conga_line_t *cl;

	m = (struct ipdc_pkthdr_t *)buf;

	if ((cl = conga_find(si, m->trans_id)) != NULL) {
		conga_delete(cl);
		printf("REJECT! CURRENTLY %d ELEMENTS IN CONGA LINE FOR %s\n", conga_count(si), inet_ntoa(*((struct in_addr *)&si->ip)));
	}

	return 0;
}

int msg_handler_DEFAULT(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;

	m = (struct ipdc_pkthdr_t *)buf;
	printf("Got a %s message of %d bytes in %d byte packet from %s\n", ipdc_msgcode_name(ntohs(m->msg_code)),  ntohs(m->pkt_len), buflen, inet_ntoa(*((struct in_addr *)&si->ip)));

	ipdc_print_all_tags(buf, buflen, IPDC_PKTHDR_T_LEN);

	return 0;
}

int msg_handler_NMS(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;
	struct ipdc_pktdata_t *tag;
	struct mod_stat_t *mod;
	struct ipdc_conga_line_t *cl;
	uint32_t len = IPDC_PKTHDR_T_LEN, n, trans_id;
	uint16_t mod_num = 0;
	uint8_t mod_type = 0;
	uint8_t mod_status = 0;
	uint16_t mod_lines = 0;
	uint16_t i;

	m = (struct ipdc_pkthdr_t *)buf;
	printf("Got a NMS message of %d bytes in %d byte packet\n", ntohs(m->pkt_len), buflen);

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_MODULE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				mod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_MODULE_TYPE:
				memcpy(&n, tag->data, sizeof(n));
				mod_type = ntohs(n) >> 8;
				break;
			case IPDC_TAG_MODULE_STATUS:
				memcpy(&n, tag->data, sizeof(n));
				mod_status = ntohs(n) >> 8;
				break;
			case IPDC_TAG_LINE_STATUS_LEN:
				memcpy(&n, tag->data, sizeof(n));
				mod_lines = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_STATUS_ARRAY:
				break;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x [%s]\n", tag->tag, ipdc_tag_name(tag->tag));
		}

		free(tag);
	}

	if ((cl = conga_find(si, m->trans_id)) != NULL) {
		if (cl->code == IPDC_MSG_RMS) {
			conga_delete(cl);
			printf("CURRENTLY %d ELEMENTS IN CONGA LINE FOR %s\n", conga_count(si), inet_ntoa(*((struct in_addr *)&si->ip)));
		} else {
			fprintf(stderr, "FOUND MATCHING TRANSACTION IN CONGA LINE, BUT INCORRECT MESSAGE!\n");
		}
	}

	if ((mod = mod_find_module(si, mod_num)) == NULL) {
		if (mod_add_module(si, mod_num, mod_type, mod_status, mod_lines) != 0) {
			fprintf(stderr, "could not add module to list\n");
			return -1;
		}

		/* increment # of nms messages received */
		si->nms_rcvd++;
	} else {
		mod->type = mod_type;
		mod->status = mod_status;
		mod->lines = mod_lines;
	}

	if (si->nms_rcvd == si->modules) {
		trans_id = ipdc_inc_trans_id(si);
		printf("\n");

		mod = mod_head;
		while (mod != NULL) {
			for (i = 1; i <= mod->lines; i++) {
				ipdc_send_rls(si, trans_id, mod->module, i);
				trans_id = ipdc_inc_trans_id(si);
			}

			mod = mod->next;
		}

		printf("\n");
	}

	return 0;
}

int msg_handler_NLS(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;
	struct ipdc_pktdata_t *tag;
	struct mod_lines_t *line = NULL;
	struct mod_chans_t *chan = NULL, *dst_chan = NULL;
	struct ipdc_system_info_t *dsi = NULL;
	uint32_t len = IPDC_PKTHDR_T_LEN, n;
	char *lsa = NULL;
	uint16_t mod_num = 0, line_num = 0, line_chans = 0;
	uint8_t line_status = 0;
	uint16_t i;
	struct db_xconnect_t *dxc = NULL;
	struct db_trunkgroup_t *dtc = NULL;
	struct ipdc_conga_line_t *cl;

	m = (struct ipdc_pkthr_t *)buf;
	// printf("Got a NLS message of %d bytes\n", ntohs(m->pkt_len));

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_MODULE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				mod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				line_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_STATUS:
				memcpy(&n, tag->data, sizeof(n));
				line_status = ntohs(n) >> 8;
				break;
			case IPDC_TAG_CHAN_STATUS_LEN:
				memcpy(&n, tag->data, sizeof(n));
				line_chans = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHAN_STATUS_ARRAY:
				lsa = calloc(1, tag->size);
				if (lsa == NULL) {
					perror("calloc");
					return -1;
				}

				memcpy(lsa, tag->data, tag->size);
				break;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x [%s]\n", tag->tag, ipdc_tag_name(tag->tag));
		}

		free(tag);
	}

	if ((cl = conga_find(si, m->trans_id)) != NULL) {
		if (cl->code == IPDC_MSG_RLS) {
			conga_delete(cl);
			printf("CURRENTLY %d ELEMENTS IN CONGA LINE FOR %s\n", conga_count(si), inet_ntoa(*((struct in_addr *)&si->ip)));
		} else {
			fprintf(stderr, "FOUND MATCHING TRANSACTION IN CONGA LINE, BUT INCORRECT MESSAGE!\n");
		}
	}

	if ((line = mod_find_line(si, mod_num, line_num)) == NULL) {
		if (mod_add_line(si, mod_num, line_num, line_status, line_chans) != 0) {
			fprintf(stderr, "could not add line to list\n");
			return -1;
		}
	} else {
		line->status = line_status;
		line->chans = line_chans;
	}

	for (i = 1; i <= line_chans; i++) {
		if ((chan = mod_find_chan(si, mod_num, line_num, i)) == NULL) {
			if ((chan = mod_add_chan(si, mod_num, line_num, i, lsa[i - 1])) == NULL) {
				fprintf(stderr, "could not add chan to list\n");
				return -1;
			}
		} else {
			chan->status = lsa[i - 1];
		}

		/* check trunkgroup db */
		if ((dtc = db_find_tg(si->ip, chan->l->m->module, chan->l->line, chan->chan)) != NULL) {
			if (tg_add(si, chan, dtc->tg) == NULL) {
				fprintf(stderr, "could not add to trunkgroup list\n");
				return -1;
			}
		}

		dxc = NULL;
		dst_chan = NULL;

		/* check crossconnect db */
		if ((dxc = db_find_xc(si->ip, mod_num, line_num, i)) == NULL)
			continue;

		if (dxc->s_ip == si->ip) {
			if ((dsi = sys_find(dxc->d_ip)) == NULL)
				continue;
			dst_chan = mod_find_chan(dsi, dxc->d_module, dxc->d_line, dxc->d_chan);
		} else if (dxc->d_ip == si->ip) {
			if ((dsi = sys_find(dxc->s_ip)) == NULL)
				continue;
			dst_chan = mod_find_chan(dsi, dxc->s_module, dxc->s_line, dxc->s_chan);
		}

		if (dst_chan == NULL)
			continue;

		if (chan->si == dst_chan->si) {
			fprintf(stderr, "no crossconnects on the same chassis!\n");
			continue;
		}

		if (xc_add(chan, dst_chan) == NULL) {
			/* Could not added channel to crossconnect list, perhaps it already exists? */
			continue;
		}

		if (chan->status != IPDC_CHAN_STATUS_CONNECTED && chan->status != IPDC_CHAN_STATUS_NOT_PRESENT) {
			if (ipdc_send_scs(chan, NULL, IPDC_CHAN_STATUS_IDLE, 1) < 0) {
				fprintf(stderr, "could not set source channel to idle\n");
				continue;
			}
		}

		/* check reverse direction for two-way trunk */
		if (xc_find_src(dst_chan) != NULL)
			continue;

		if (xc_add(dst_chan, chan) == NULL) {
			// fprintf(stderr, "could not add two-way to crossconnect list\n");
			continue;
		}

		if (dst_chan->status != IPDC_CHAN_STATUS_CONNECTED && dst_chan->status != IPDC_CHAN_STATUS_NOT_PRESENT) {
			if (ipdc_send_scs(dst_chan, NULL, IPDC_CHAN_STATUS_IDLE, 1) < 0) {
				fprintf(stderr, "could not set destination channel to idle\n");
				continue;
			}
		}
	}

	lsa = NULL;

	return 0;
}

int msg_handler_NCS(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;
	struct ipdc_pktdata_t *tag;
	struct mod_chans_t *chan = NULL;
	uint32_t len = IPDC_PKTHDR_T_LEN, n;
	uint16_t mod_num = 0, line_num = 0, chan_num = 0;
	uint8_t chan_status = 0;
	struct ipdc_conga_line_t *cl;

	m = (struct ipdc_pkthr_t *)buf;
	printf("Got a NCS message of %d bytes\n", ntohs(m->pkt_len));

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_MODULE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				mod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				line_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHANNEL_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				chan_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHAN_STATUS_ARRAY:
				if (tag->size != 1) {
					fprintf(stderr, "ERROR: CHANNEL STATUS ARRAY IS NOT 1 IN NCS\n");
					return -1;
				}

				memcpy(&chan_status, tag->data, tag->size);
				break;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x [%s]\n", tag->tag, ipdc_tag_name(tag->tag));
		}

		free(tag);
	}

	if ((cl = conga_find(si, m->trans_id)) != NULL) {
		if (cl->code == IPDC_MSG_RCS) {
			conga_delete(cl);
			printf("CURRENTLY %d ELEMENTS IN CONGA LINE FOR %s\n", conga_count(si), inet_ntoa(*((struct in_addr *)&si->ip)));
		} else {
			fprintf(stderr, "FOUND MATCHING TRANSACTION IN CONGA LINE, BUT INCORRECT MESSAGE!\n");
		}
	}

	printf("1/%d/%d/%d --> status: %s\n", mod_num, line_num, chan_num, (ipdc_find_chanstatus(chan_status).descr));
	if ((chan = mod_find_chan(si, mod_num, line_num, chan_num)) == NULL) {
		if ((chan = mod_add_chan(si, mod_num, line_num, chan_num, chan_status)) == NULL) {
			fprintf(stderr, "could not add chan to list\n");

			/* Ignore this for now, we probably haven't fully initialized.
			 *   We will find out anyway because we do NMS at the end. */
		}
	} else {
		chan->status = chan_status;
	}

	return 0;
}

int msg_handler_ACSI(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;
	struct ipdc_pktdata_t *tag;
	struct mod_chans_t *chan = NULL;
	uint32_t len = IPDC_PKTHDR_T_LEN, n;
	uint16_t mod_num = 0, line_num = 0, chan_num = 0;
	struct call_ref_t *cr = NULL;
	struct xconnect_t *xc = NULL;
	int  nwrite, retval = 0;
	struct ipdc_conga_line_t *cl;

	m = (struct ipdc_pkthr_t *)buf;
	printf("Got a ACSI message of %d bytes from %s\n", ntohs(m->pkt_len), inet_ntoa(*((struct in_addr *)&si->ip)));

	ipdc_print_all_tags(buf, buflen, IPDC_PKTHDR_T_LEN);

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_MODULE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				mod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				line_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHANNEL_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				chan_num = ntohl(n) >> (tag->size * 8);
				break;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x [%s]\n", tag->tag, ipdc_tag_name(tag->tag));
		}

		free(tag);
	}

	if ((cl = conga_find(si, m->trans_id)) != NULL) {
		if (cl->code == IPDC_MSG_RCSI) {
			conga_delete(cl);
			printf("CURRENTLY %d ELEMENTS IN CONGA LINE FOR %s\n", conga_count(si), inet_ntoa(*((struct in_addr *)&si->ip)));
		} else {
			fprintf(stderr, "FOUND MATCHING TRANSACTION IN CONGA LINE, BUT INCORRECT MESSAGE!\n");
		}
	}

	if ((chan = mod_find_chan(si, mod_num, line_num, chan_num)) == NULL) {
		fprintf(stderr, "ERROR: ACSI RECEIVED FOR NON-EXISTANT CHANNEL!\n");
		retval = -1;
		goto free_all;
	}

	cr = cr_find_callref_by_call(chan);
	if (cr == NULL) {
		/* check if crossconnected */
		xc = xc_find_dst(chan);
		if (xc == NULL) {
			printf("1/%d/%d/%d is not a Q.931 call?\n", chan->l->m->module, chan->l->line, chan->chan);
			retval = -1;
			goto free_all;
		}

		cr = cr_find_callref_by_call(xc->call);
		if (cr == NULL) {
			printf("1/%d/%d/%d is not a crossconnected Q.931 call?\n", mod_num, line_num, chan_num);
			retval = -1;
			goto free_all;
		}
	}

	/* Set new call state, and send CONNECT */
	if ((nwrite = ipdc_q931_send_connect(cr, NULL, 0)) < 0) {
		ipdc_q931_send_release_complete(cr->sig, ipdc_q931_flip_callref(cr->callref), IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
		fprintf(stderr, "could not send CONNECT\n");
		retval = -1;
		goto free_all;
	}

	cr->state = IPDC_Q931_CALL_STATE_U9;
	printf("Sent a TUNL(CONNECT) message of %d bytes to 1/%d/%d/%d\n", nwrite, cr->sig->l->m->module, cr->sig->l->line, cr->sig->chan);

free_all:
	return retval;
}

int msg_handler_CONI(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;
	struct ipdc_pktdata_t *tag;
	struct mod_chans_t *chan = NULL;
	uint32_t len = IPDC_PKTHDR_T_LEN, n;
	uint16_t mod_num = 0, line_num = 0, chan_num = 0, mbid = 0;
	struct call_ref_t *cr = NULL;
	struct xconnect_t *xc = NULL;

	m = (struct ipdc_pkthr_t *)buf;
	printf("Got a CONI message of %d bytes\n", ntohs(m->pkt_len));

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_MODULE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				mod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				line_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHANNEL_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				chan_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_ACCESS_DEV_CALL_ID:
				memcpy(&n, tag->data, sizeof(n));
				mbid = ntohl(n) >> (tag->size * 8);
				break;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x [%s]\n", tag->tag, ipdc_tag_name(tag->tag));
		}

		free(tag);
	}

	if ((chan = mod_find_chan(si, mod_num, line_num, chan_num)) == NULL) {
		fprintf(stderr, "ERROR: CALL CONNECTED ON NON-EXISTANT CHANNEL!\n");
		return -1;
	}

	cr = cr_find_callref_by_call(chan);
	if (cr != NULL) {
		printf("Q.931 call reference: %d, signaling channel: 1/%d/%d/%d\n", cr->callref, cr->sig->l->m->module, cr->sig->l->line, cr->sig->chan);
	} else {
		xc = xc_find_dst(chan);
		if (xc == NULL) {
			ipdc_send_rcr(chan, IPDC_Q931_CAUSE_CODE_CALL_REJECTED & IPDC_Q931_CAUSE_CODE_MASK);
			printf("1/%d/%d/%d is not a crossconnected Q.931 call?\n", mod_num, line_num, chan_num);
			return -1;
		}

		cr = cr_find_callref_by_call(xc->call);
		if (cr == NULL) {
			ipdc_send_rcr(chan, IPDC_Q931_CAUSE_CODE_CALL_REJECTED & IPDC_Q931_CAUSE_CODE_MASK);
			printf("1/%d/%d/%d is not a Q.931 call?\n", mod_num, line_num, chan_num);
			return -1;
		}
	}

	printf("Incoming call connected on 1/%d/%d/%d [MBID %d]\n", mod_num, line_num, chan_num, mbid);
	if (chan->status != IPDC_CHAN_STATUS_CONNECTED) {
		printf("WARNING: channel status is %s\n", (ipdc_find_chanstatus(chan->status).descr));
	}

	/* Set new call state, and notify network of new call state */
	cr->state = IPDC_Q931_CALL_STATE_U9;
	return 0;
}

int msg_handler_RCR(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;
	struct ipdc_pktdata_t *tag;
	struct mod_chans_t *chan = NULL;
	uint32_t len = IPDC_PKTHDR_T_LEN, n, trans_id;
	uint16_t mod_num = 0, line_num = 0, chan_num = 0;
	struct call_ref_t *cr = NULL;
	uint16_t mbid = 0;
	uint8_t	cause = 0;

	m = (struct ipdc_pkthr_t *)buf;
	printf("Got a RCR message of %d bytes\n", ntohs(m->pkt_len));
	ipdc_print_all_tags(buf, buflen, IPDC_PKTHDR_T_LEN);

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_MODULE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				mod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				line_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHANNEL_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				chan_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_ACCESS_DEV_CALL_ID:
				memcpy(&n, tag->data, sizeof(n));
				mbid = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CAUSE_CODE:
				memcpy(&n, tag->data, sizeof(n));
				cause = ntohs(n) >> 8;
				break;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x [%s]\n", tag->tag, ipdc_tag_name(tag->tag));
		}

		free(tag);
	}

	if ((chan = mod_find_chan(si, mod_num, line_num, chan_num)) == NULL) {
		fprintf(stderr, "ERROR: RCR RECEIVED FOR NON-EXISTANT CHANNEL!\n");
		return -1;
	}

	if (mbid > 0) {
		printf("Received disconnect request for modem call on 1/%d/%d/%d, cause code: %d [%s]\n", mod_num, line_num, chan_num, cause, (ipdc_find_q931_cause_code(cause).descr));
	} else {
		printf("Received disconnect request for voice call on 1/%d/%d/%d, cause code: %d [%s]\n", mod_num, line_num, chan_num, cause, (ipdc_find_q931_cause_code(cause).descr));
	}

	if (chan->status == IPDC_CHAN_STATUS_CONNECTED) {
		printf("Disconnecting 1/%d/%d/%d...\n", mod_num, line_num, chan_num);
		if (ipdc_send_scs(chan, NULL, IPDC_CHAN_STATUS_IDLE, 1) < 0) {
			fprintf(stderr, "couldn't set status of channel 1/%d/%d/%d\n", mod_num, line_num, chan_num);
			return -1;
		}

		if ((cr = cr_find_callref_by_call(chan)) != NULL) {
			printf("Active Q.931-associated call!\n");
			ipdc_q931_send_release_complete(cr->sig, cr->callref | IPDC_Q931_CALLREF_TERM, cause & IPDC_Q931_CAUSE_CODE_MASK);
		}

		chan->status = IPDC_CHAN_STATUS_IDLE;
	} else {
		printf("Can't disconnect 1/%d/%d/%d in its current state of %s\n", mod_num, line_num, chan_num, (ipdc_find_chanstatus(chan->status).descr));
	}

	trans_id = m->trans_id & ~(1 << 31);
	if (ipdc_send_acr(si, trans_id, chan->l->m->module, chan->l->line, chan->chan, cause) < 0) {
		fprintf(stderr, "could not send ACR!\n");
		return -1;
	}

	return 0;
}

int msg_handler_ACR(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;
	struct ipdc_pktdata_t *tag;
	struct mod_chans_t *chan = NULL;
	uint32_t len = IPDC_PKTHDR_T_LEN, n;
	uint16_t mod_num = 0, line_num = 0, chan_num = 0;
	uint8_t cause = 0;
	struct call_ref_t *cr = NULL;
	int nwrite, retval = 0;
	struct ipdc_conga_line_t *cl;

	m = (struct ipdc_pkthr_t *)buf;
	printf("Got a ACR message of %d bytes\n", ntohs(m->pkt_len));

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_MODULE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				mod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				line_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHANNEL_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				chan_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CAUSE_CODE:
				memcpy(&n, tag->data, sizeof(n));
				cause = ntohs(n) >> 8;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x [%s]\n", tag->tag, ipdc_tag_name(tag->tag));
		}

		free(tag);
	}

	if ((cl = conga_find(si, m->trans_id)) != NULL) {
		if (cl->code == IPDC_MSG_RCR) {
			conga_delete(cl);
			printf("CURRENTLY %d ELEMENTS IN CONGA LINE FOR %s\n", conga_count(si), inet_ntoa(*((struct in_addr *)&si->ip)));
		} else {
			fprintf(stderr, "FOUND MATCHING TRANSACTION IN CONGA LINE, BUT INCORRECT MESSAGE!\n");
		}
	}

	if ((chan = mod_find_chan(si, mod_num, line_num, chan_num)) == NULL) {
		fprintf(stderr, "ERROR: ACR RECEIVED FOR NON-EXISTANT CHANNEL!\n");
		retval = -1;
		goto free_all;
	}

	if (ipdc_send_scs(chan, NULL, IPDC_CHAN_STATUS_IDLE, 1) < 0) {
		fprintf(stderr, "could not send SCS!\n");
		retval = -1;
		goto free_all;
	}

	printf("channel status changing from %s to idle\n", (ipdc_find_chanstatus(chan->status).descr));

	cr = cr_find_callref_by_call(chan);
	if (cr == NULL) {
		printf("1/%d/%d/%d was not a Q.931 call!\n", mod_num, line_num, chan_num);
	} else {
		/* Set new call state, and send release */
		if ((nwrite = ipdc_q931_send_release_complete(cr->sig, cr->callref | IPDC_Q931_CALLREF_TERM, cause)) < 0) {
			fprintf(stderr, "could not send release complete!\n");
			retval = -1;
			goto free_all;
		}

		cr->state = IPDC_Q931_CALL_STATE_U12;
		printf("Sent a TUNL(RELEASE_COMP) message of %d bytes to 1/%d/%d/%d\n", nwrite, cr->sig->l->m->module, cr->sig->l->line, cr->sig->chan);

		cr_remove_call_ref(cr);
	}

free_all:
	return retval;
}

int msg_handler_RSCS(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;
	struct ipdc_pktdata_t *tag;
	uint32_t len = IPDC_PKTHDR_T_LEN, n, trans_id;
	uint16_t mod_num = 0, line_num = 0, chan_start = 0, chan_end = 0, num_chans = 0;
	uint8_t chan_res = 0;
	char *csa = NULL;
	int i;
	struct mod_chans_t *chan = NULL;
	struct ipdc_conga_line_t *cl;

	m = (struct ipdc_pkthr_t *)buf;

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_MODULE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				mod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				line_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHANGRP_FIRST:
				memcpy(&n, tag->data, sizeof(n));
				chan_start = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHANGRP_LAST:
				memcpy(&n, tag->data, sizeof(n));
				chan_end = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHAN_STATUS_LEN:
				memcpy(&n, tag->data, sizeof(n));
				num_chans = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHAN_STATUS_ARRAY:
				csa = calloc(1, tag->size);
				if (csa == NULL) {
					perror("calloc");
					return -1;
				}
				memcpy(csa, tag->data, tag->size);
				break;
			case IPDC_TAG_SET_CHANSTAT_RESULT:
				memcpy(&n, tag->data, sizeof(n));
				chan_res = ntohs(n) >> 8;
				break;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x [%s]\n", tag->tag, ipdc_tag_name(tag->tag));
		}

		free(tag);
	}

	if ((cl = conga_find(si, m->trans_id)) != NULL) {
		if (cl->code == IPDC_MSG_SCS) {
			conga_delete(cl);
			printf("CURRENTLY %d ELEMENTS IN CONGA LINE FOR %s\n", conga_count(si), inet_ntoa(*((struct in_addr *)&si->ip)));
		} else {
			fprintf(stderr, "FOUND MATCHING TRANSACTION IN CONGA LINE, BUT INCORRECT MESSAGE!\n");
		}
	}

	if (chan_res != 0) {
		ipdc_print_all_tags(buf, buflen - 1, IPDC_PKTHDR_T_LEN);
		printf("WARNING: At least one channel failed SCS...running RCS to get better results.\n");
	}

	trans_id = ipdc_inc_trans_id(si);
	for (i = chan_start; i <= chan_end; i++) {
		if (chan_res != 0) {
			if (ipdc_send_rcs(si, trans_id, mod_num, line_num, i) < 0) {
				fprintf(stderr, "ERROR: could not send SCS for 1/%d/%d/%d\n", mod_num, line_num, i);
				return -1;
			}
			trans_id = ipdc_inc_trans_id(si);
		} else {
			if ((chan = mod_find_chan(si, mod_num, line_num, i)) == NULL) {
				fprintf(stderr, "ERROR: Channel 1/%d/%d/%d does not exist!\n", mod_num, line_num, i);
				return -1;
			} else {
				chan->status = csa[i - chan_start];
			}
		}
	}

	return 0;
}

int msg_handler_TUNL(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	int retval = 0;
	char *data = NULL;
	struct ipdc_q931_t *p;
	struct ipdc_pkthdr_t *m;
	struct ipdc_pktdata_t *tag;
	uint16_t mod_num = 0, line_num = 0, chan_num = 0;
	uint8_t src_port_type = 0;
	uint16_t sig_proto = 0;
	uint32_t len = IPDC_PKTHDR_T_LEN;
	uint32_t n;
	uint32_t pdulen = 0;
	uint8_t ie_id, ie_len;
	struct ipdc_q931_ie_t ie;
	uint32_t cp;
	unsigned char *pdu = NULL;
	struct ie_bundle_t *ieb = NULL, *ieb_head = NULL, *ieb_tail = NULL;
	struct ipdc_q931_elem_t *elem, *chan_elem = NULL, *bcap_elem = NULL, *calling_elem = NULL, *called_elem = NULL, *e[16];
	uint8_t calling_numtype, calling_numplan, called_numtype, called_numplan, ds0_num;
	char *calling_num = NULL, *called_num = NULL;
	struct mod_chans_t *c = NULL, *sigchan = NULL;
	struct call_ref_t *cr = NULL;
	char *r;
	uint16_t callref;
	unsigned char cause[2];
	struct trunkgroup_t *t = NULL, *ot = NULL, *ot2 = NULL;
	struct call_route_t *route = NULL, *route2 = NULL;
	struct xconnect_t *xc = NULL;

	m = (struct ipdc_pkthr_t *)buf;
	printf("Got a TUNL message of %d bytes for %s\n", ntohs(m->pkt_len), inet_ntoa(*((struct in_addr *)&si->ip)));

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_SRC_PORT_TYPE:
				memcpy(&n, tag->data, sizeof(n));
				src_port_type = ntohs(n) >> 8;
				break;
			case IPDC_TAG_SIGNALING_TYPE:
				memcpy(&n, tag->data, sizeof(n));
				sig_proto = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_MODULE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				mod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				line_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHANNEL_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				chan_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_PDU:
				pdulen = tag->size;
				if (pdulen > 0) {
					pdu = calloc(1, pdulen);
					if (pdu == NULL) {
						perror("calloc");
						retval = -1;
						goto free_all;
					}

					memcpy(pdu, tag->data, pdulen);
				}
				break;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x [%s]\n", tag->tag, ipdc_tag_name(tag->tag));
		}
	}

	if (mod_num != 0 && line_num != 0 && chan_num != 0 && sig_proto != 0) {
		printf("Received tunneled signaling data [%s] on channel [1/%d/%d/%d]\n", (ipdc_find_q931_sig_proto(sig_proto).descr), mod_num, line_num, chan_num);
		if (pdu == NULL) {
			printf("...but no data was sent?\n");
			retval = -1;
			goto free_all;
		}

		sigchan = mod_find_chan(si, mod_num, line_num, chan_num);
		if (sigchan == NULL) {
			fprintf(stderr, "...but channel 1/%d/%d/%d doesn't exist with us?\n", mod_num, line_num, chan_num);
			retval = -1;
			goto free_all;
		}

		p = (struct ipdc_pri_t *)pdu;
		printf("protocol discriminator is 0x%02x [%s]\n", p->pd, (ipdc_find_q931_proto(p->pd).descr));
		if ((ipdc_find_q931_proto(p->pd).proto) == IPDC_Q931_PROTO_UNKNOWN) {
			fprintf(stderr, "Unknown protocol!\n");
			retval = -1;
			goto free_all;
		}

		printf("call reference length is %d\n", p->crlen);
		callref = p->callref;
		if ((ntohs(callref) & IPDC_Q931_CALLREF_MASK) == IPDC_Q931_CALLREF_ORIG) {
			printf("call reference direction: call origin to ME\n");
		} else {
			printf("call reference direction: call destination to ME\n");
		}
		printf("call reference is %d/%x\n", callref, callref);

		cr = cr_find_callref_by_ref(callref, sigchan, CALL_REF_SIGNALING_CHAN);
		if (cr == NULL) {
			if (p->msgtype == IPDC_Q931_MSG_RESTART) {
				cr = cr_add_call_ref(sigchan, NULL, IPDC_Q931_GLOBAL_CALLREF, IPDC_Q931_CALL_STATE_R0);
			} else {
				if ((ntohs(callref) & IPDC_Q931_CALLREF_MASK) == IPDC_Q931_CALLREF_ORIG) {
					cr = cr_add_call_ref(sigchan, NULL, p->callref, IPDC_Q931_CALL_STATE_U0);
				} else {
					cr = cr_add_call_ref(sigchan, NULL, p->callref, IPDC_Q931_CALL_STATE_N0);
				}
			}

			if (cr == NULL) {
				ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
				fprintf(stderr, "could not create call reference structure!\n");
				retval = -1;
				goto free_all;
			}
		}

		printf("message type is %s\n", (ipdc_find_q931_msg(p->msgtype).descr));
		printf("total ie data length is %d bytes\n", pdulen - IPDC_Q931_HDR_LEN);

		cp = IPDC_Q931_HDR_LEN;
		while (cp < pdulen) {
			memcpy(&ie_id, pdu + cp, 1);
			ie = ipdc_find_q931_ie(ie_id);
			printf("ie is %02x [%s]\n", ie_id, ie.descr);
			cp++;

			memcpy(&ie_len, pdu + cp, 1);
			printf("ie length is %d bytes\n", ie_len);
			cp++;

			if (ie_len < ie.min_len) {
				printf("WARNING: INFORMATION ELEMENT LENGTH IS LESS THAN Q.931 SPECIFICATION\n");
			} else if (ie_len > ie.max_len) {
				printf("WARNING: INFORMATION ELEMENT LENGTH IS GREATER THAN Q.931 SPECIFICATION\n");
				ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_IE_INVALID);
				cr_remove_call_ref(cr);
				retval = -1;
				goto free_all;
			}

			data = calloc(1, ie_len);
			if (data == NULL) {
				ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
				perror("calloc");
				retval = -1;
				goto free_all;
			}

			memcpy(data, pdu + cp, ie_len);
			printf("  data: %s\n", data);
			free(data);

			elem = ipdc_q931_create_ie(ie_id, pdu + cp, ie_len);
			if (elem == NULL) {
				ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
				perror("calloc");
				retval = -1;
				goto free_all;
			}

			ieb = calloc(1, sizeof(struct ie_bundle_t));
			if (ieb == NULL) {
				ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
				perror("calloc");
				retval = -1;
				goto free_all;
			}
			ieb->elem = elem;

			if (ieb_head == NULL) {
				ieb_head = ieb_tail = ieb;
			} else {
				ieb_tail->next = ieb;
				ieb_tail = ieb_tail->next;
			}

			cp += ie_len;
		}

		switch(p->msgtype) {
			case IPDC_Q931_MSG_RESTART:
				cr->state = IPDC_Q931_CALL_STATE_R1;

				ieb = ieb_head;
				while (ieb != NULL) {
					printf("ie is %s, length is %d bytes\n", (ipdc_find_q931_ie(ieb->elem->ie).descr), ieb->elem->len);
					switch(ieb->elem->ie) {
						case IPDC_Q931_IE_DISPLAY:
							break;
						case IPDC_Q931_IE_CHANNEL_ID:
							/* save ie for later use */
							e[0] = ieb->elem;
							r = ipdc_q931_decode_ie(ieb->elem);
							if (r == NULL) {
								ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_IE_INVALID);
								printf("could not decode ie!\n");
								retval = -1;
								goto free_all;
							}
							memcpy(&ds0_num, r, sizeof(ds0_num));

							c = mod_find_chan(si, mod_num, line_num, ds0_num);
							if (c == NULL) {
								ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_CHAN_INVALID);
								printf("ERROR: could not find channel 1/%d/%d/%d\n", mod_num, line_num, ds0_num);
								return -1;
							}
							break;
						case IPDC_Q931_IE_RESTART_IND:
							e[1] = ieb->elem;
							memcpy(&n, ieb->elem->data, 1);

							printf("  restart indicator: %s\n", ipdc_find_q931_restart_ind(n & IPDC_Q931_RESTART_IND_MASK));
							break;
						default:
							printf("Unexpected IE!\n");
							break;
					}

					ieb = ieb->next;
				}

				printf("received restart request for channel 1/%d/%d/%d (state: %s)\n", mod_num, line_num, ds0_num, (ipdc_find_chanstatus(c->status).descr));
				if (c->status != IPDC_CHAN_STATUS_IDLE) {
					printf("requested channel 1/%d/%d/%d is not idle, state: %s\n", mod_num, line_num, ds0_num, (ipdc_find_chanstatus(c->status).descr));
					printf("setting to idle state...\n");

					if (ipdc_send_scs(c, NULL, IPDC_CHAN_STATUS_IDLE, 1) < 0) {
						ipdc_q931_send_status(sigchan, IPDC_Q931_GLOBAL_CALLREF, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE, cr->state);
						fprintf(stderr, "couldn't set status of channel 1/%d/%d/%d\n", c->l->m->module, c->l->line, c->chan);
						retval = -1;
						goto free_all;
					}
				}

				if (ipdc_send_q931_msg(ipdc_q931_flip_callref(callref), IPDC_Q931_MSG_RESTART_ACK, sigchan, e, 2) < 0) {
					ipdc_q931_send_status(sigchan, IPDC_Q931_GLOBAL_CALLREF, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE, cr->state);
					fprintf(stderr, "couldn't send restart ack for channel 1/%d/%d/%d\n", c->l->m->module, c->l->line, c->chan);
					retval = -1;
					goto free_all;
				}

				break;

			case IPDC_Q931_MSG_SETUP:
				cr->state = IPDC_Q931_CALL_STATE_U6;

				ieb = ieb_head;
				while (ieb != NULL) {
					printf("ie is %s, length is %d bytes\n", (ipdc_find_q931_ie(ieb->elem->ie).descr), ieb->elem->len);
					switch(ieb->elem->ie) {
						case IPDC_Q931_IE_BEARER_CAP:
							/* save ie for later use */
							bcap_elem = ieb->elem;

							// printf("  information transfer capability: %s\n", ipdc_find_q931_info_trans_cap(n & IPDC_Q931_INFO_TRANS_CAP_MASK));
							break;
						case IPDC_Q931_IE_CHANNEL_ID:
							/* save ie for later use */
							chan_elem = ieb->elem;
							r = ipdc_q931_decode_ie(ieb->elem);
							if (r == NULL) {
								ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_IE_INVALID);
								printf("could not decode ie!\n");
								retval = -1;
								goto free_all;
							}
							memcpy(&ds0_num, r, sizeof(ds0_num));

							c = mod_find_chan(si, mod_num, line_num, ds0_num);
							if (c == NULL) {
								/* invalid channel, send back cause */
								ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_CHAN_INVALID);
								printf("ERROR: could not find channel 1/%d/%d/%d\n", mod_num, line_num, ds0_num);

								retval = -1;
								goto free_all;
							}

							if (c->status != IPDC_CHAN_STATUS_IDLE) {
								printf("requested channel 1/%d/%d/%d is not idle, state: %s\n", mod_num, line_num, ds0_num, (ipdc_find_chanstatus(c->status).descr));
								if ((n & IPDC_Q931_PREFEXCL_MASK) == IPDC_Q931_PREFEXCL_EXCLUSIVE) {
									/* exclusive channel in use, send back cause */
									ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_CHAN_UNAVAIL);
									printf("received exclusive channel request, cannot continue.\n");

									retval = -1;
									goto free_all;
								} else {
									c = mod_find_chan_with_status(si, mod_num, line_num, IPDC_CHAN_STATUS_IDLE);
									if (c == NULL) {
										/* out of channels on slot, send back cause */
										ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_NO_CHAN_AVAIL);
										printf("WARNING: no channels available on 1/%d/%d\n", mod_num, line_num);

										retval = -1;
										goto free_all;
									}

									/* set channel id element to be newly selected ds0 */
									chan_elem = ipdc_q931_chg_channel_id(chan_elem, c->chan);
									if (chan_elem == NULL) {
										/* something went awry */
										ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_NO_CHAN_AVAIL);
										printf("ERROR: could not set channel id to 1/%d/%d/%d\n", c->l->m->module, c->l->line, c->chan);

										retval = -1;
										goto free_all;
									}
								}
							}
							c->tid = m->trans_id;

							break;
						case IPDC_Q931_IE_CALLING_PARTY_NUM:
							if (ieb->elem->len > 0) {
								calling_elem = ieb->elem;

								memcpy(&n, ieb->elem->data, sizeof(n));
								calling_numtype = n & 0x70;
								calling_numplan = n & 0x0f;

								calling_num = calloc(1, ieb->elem->len);
								if (calling_num == NULL) {
									ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
									perror("calloc");
									retval = -1;
									goto free_all;
								}

								memcpy(calling_num, ieb->elem->data + 1, ieb->elem->len - 1);
								calling_num[ieb->elem->len] = '\0';
							}
							break;
						case IPDC_Q931_IE_CALLED_PARTY_NUM:
							called_elem = ieb->elem;

							memcpy(&n, ieb->elem->data, sizeof(n));
							called_numtype = n & 0x70;
							called_numplan = n & 0x0f;

							called_num = calloc(1, ieb->elem->len);
							if (called_num == NULL) {
								ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
								perror("calloc");
								retval = -1;
								goto free_all;
							}

							memcpy(called_num, ieb->elem->data + 1, ieb->elem->len - 1);
							called_num[ieb->elem->len] = '\0';
							break;
						default:
							break;
					}
					ieb = ieb->next;
				}

				if (called_num == NULL) {
					ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_IE_MISSING);
					fprintf(stderr, "received call with no called party number\n");
					retval = -1;
					goto free_all;
				}

				t = tg_find(c);
				if (t == NULL) {
					ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_NO_ROUTE_TO_DEST);
					fprintf(stderr, "couldn't find inbound trunk id\n");
					retval = -1;
					goto free_all;
				}

				route = call_route_find(si->ip, called_num);
				if (route == NULL && call_route_find(si->ip, "*") == NULL) {
					printf("no outbound trunk id found!\n");
				} else {
					if (route == NULL) {
						route = call_route_find(si->ip, "*");
					}
					printf("outbound trunk group #%d\n", route->tg);
					ot = tg_find_chan_with_status(si, route->tg, IPDC_CHAN_STATUS_IDLE);
					if (ot == NULL) {
						fprintf(stderr, "no channels available in trunk group #%d!\n", route->tg);
					} else {
						printf("available outbound trunk is 1/%d/%d/%d [trunk group #%d]\n", ot->t->l->m->module, ot->t->l->line, ot->t->chan, ot->tg);
						xc = xc_find_src(ot->t);
						if (xc == NULL) {
							fprintf(stderr, "1/%d/%d/%d [trunk group #%d] is not a crossconnected trunk!\n", ot->t->l->m->module, ot->t->l->line, ot->t->chan, ot->tg);
						} else {
							fprintf(stderr, "1/%d/%d/%d [trunk group #%d] crossconnects to %s:1/%d/%d/%d\n", ot->t->l->m->module, ot->t->l->line, ot->t->chan, ot->tg, inet_ntoa(*((struct in_addr *)&xc->dst->si->ip)), xc->dst->l->m->module, xc->dst->l->line, xc->dst->chan);
							route2 = call_route_find(xc->dst->si->ip, called_num);
							if (route2 == NULL && call_route_find(xc->dst->si->ip, "*") == NULL) {
								ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_NO_ROUTE_TO_DEST);
								fprintf(stderr, "no inbound trunk id found on destination chassis!\n");
								retval = -1;
								goto free_all;
							} else {
								if (route2 == NULL) {
									route2 = call_route_find(xc->dst->si->ip, "*");
								}
								printf("destination inbound trunk group #%d\n", route2->tg);
								if (route2->tg == 999) {
									/* it's our magical tnt modem trunk id! */
								} else {
									ot2 = tg_find_chan_with_status(xc->dst->si, route2->tg, IPDC_CHAN_STATUS_IDLE);
									if (ot2 == NULL) {
										ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_NO_CHAN_AVAIL);
										fprintf(stderr, "no channels available in destination inbound trunk group #%d!\n", route2->tg);
										retval = -1;
										goto free_all;
									} else {
										printf("available inbound trunk is 1/%d/%d/%d [trunk group #%d]\n", ot2->t->l->m->module, ot2->t->l->line, ot2->t->chan, ot2->tg);
									}
								}
							}
						}
					}
				}

				cr->calling = calling_num;
				cr->called = called_num;
				cr->state = IPDC_Q931_CALL_STATE_U6;
				cr->call = c;

				printf("reserved channel 1/%d/%d/%d for %s --> %s, inbound via trunk group #%d\n", c->l->m->module, c->l->line, c->chan, calling_num == NULL ? "<no calling party #>" : calling_num, called_num, t->tg);

				/* Send CALL_PROCEEDING */
				if (ipdc_send_q931_msg(p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_MSG_CALL_PROCEEDING, sigchan, &chan_elem, 1) < 0) {
					ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
					fprintf(stderr, "could send message(CALL_PROCEEDING)\n");
					retval = -1;
					goto free_all;
				}

				/* Set call state */
				cr->state = IPDC_Q931_CALL_STATE_U7;

				if (xc == NULL) {
					if (si->u_ports == 0) {
						fprintf(stderr, "no universal ports available on %s!\n", inet_ntoa(*((struct in_addr *)&si->ip)));
						ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_NO_ROUTE_TO_DEST);
						retval = -1;
						goto free_all;
					}

					if (ipdc_send_rcsi(c, 0x10, calling_num, called_num, 0x01) < 0) {
						ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_CALL_REJECTED);
						fprintf(stderr, "couldn't answer incoming call on channel 1/%d/%d/%d\n", c->l->m->module, c->l->line, c->chan);
						retval = -1;
						goto free_all;
					}
				} else {
					printf("INBOUND: crossconnecting 1/%d/%d/%d to 1/%d/%d/%d\n", c->l->m->module, c->l->line, c->chan, xc->src->l->m->module, xc->src->l->line, xc->src->chan);
					if (ipdc_send_rcst(c, xc->src, 0x00, calling_num, called_num) < 0) {
						ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_CALL_REJECTED);
						fprintf(stderr, "couldn't transfer incoming call on channel 1/%d/%d/%d\n", c->l->m->module, c->l->line, c->chan);
						retval = -1;
						goto free_all;
					}
					xc->call = c;

					if (ot2 != NULL) {
						printf("OUTBOUND: crossconnecting 1/%d/%d/%d to 1/%d/%d/%d\n", xc->dst->l->m->module, xc->dst->l->line, xc->dst->chan, ot2->t->l->m->module, ot2->t->l->line, ot2->t->chan);
						if (ipdc_send_rcst(xc->dst, ot2->t, 0x00, calling_num, called_num) < 0) {
							ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_CALL_REJECTED);
							fprintf(stderr, "couldn't transfer outgoing call on channel 1/%d/%d/%d\n", xc->dst->l->m->module, xc->dst->l->line, xc->dst->chan);
							retval = -1;
							goto free_all;
						}
					} else {
						if (xc->dst->si->u_ports == 0) {
							fprintf(stderr, "no universal ports available on %s!\n", inet_ntoa(*((struct in_addr *)&xc->dst->si->ip)));
							ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_NO_ROUTE_TO_DEST);
							retval = -1;
							goto free_all;
						}

						if (ipdc_send_rcsi(xc->dst, 0x10, calling_num, called_num, 0x01) < 0) {
							ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_CALL_REJECTED);
							fprintf(stderr, "couldn't answer incoming call on channel 1/%d/%d/%d\n", ot->t->l->m->module, ot->t->l->line, ot->t->chan);
							retval = -1;
							goto free_all;
						}
					}
				}

				/* Set call state */
				cr->state = IPDC_Q931_CALL_STATE_U8;

				break;

			case IPDC_Q931_MSG_STATUS_ENQUIRY:
				ieb = ieb_head;
				while (ieb != NULL) {
					printf("ie is %s, length is %d bytes\n", (ipdc_find_q931_ie(ieb->elem->ie).descr), ieb->elem->len);
					switch(ieb->elem->ie) {
						case IPDC_Q931_IE_DISPLAY:
							break;
						default:
							printf("Unexpected IE!\n");
							break;
					}

					ieb = ieb->next;
				}

				cause[0] = 0;
				cause[1] = 0;
				cause[1] = IPDC_Q931_CAUSE_CODE_STATUS_ENQ_RESP & IPDC_Q931_CAUSE_CODE_MASK;

				e[0] = ipdc_q931_create_ie(IPDC_Q931_IE_CAUSE, &cause, sizeof(cause));
				if (e[0] == NULL) {
					ipdc_q931_send_status(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE, cr->state);
					fprintf(stderr, "could not create cause IE\n");
					retval = -1;
					goto free_all;
				}

				e[1] = ipdc_q931_create_ie(IPDC_Q931_IE_CALL_STATE, &cr->state, sizeof(cr->state));
				if (e[1] == NULL) {
					ipdc_q931_send_status(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE, cr->state);
					fprintf(stderr, "could not create call state IE\n");
					retval = -1;
					goto free_all;
				}

				if (ipdc_q931_send_status(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_STATUS_ENQ_RESP, cr->state) < 0) { 
					ipdc_q931_send_status(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE, cr->state);
					perror("calloc");
					retval = -1;
					goto free_all;
				}

				break;

			case IPDC_Q931_MSG_CONNECT_ACK:
				cr->state = IPDC_Q931_CALL_STATE_U10;

				ieb = ieb_head;
				while (ieb != NULL) {
					printf("ie is %s, length is %d bytes\n", (ipdc_find_q931_ie(ieb->elem->ie).descr), ieb->elem->len);
					ieb = ieb->next;
				}

				if (cr->call == NULL) {
					ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_CALL_CLEARED);
					fprintf(stderr, "how can we connect with no call?!\n");
					retval = -1;
					goto free_all;
				}

				c = cr->call;
				c->status = IPDC_CHAN_STATUS_CONNECTED;

				break;

			case IPDC_Q931_MSG_DISCONNECT:
				cr->state = IPDC_Q931_CALL_STATE_U12;

				ieb = ieb_head;
				while (ieb != NULL) {
					printf("ie is %s, length is %d bytes\n", (ipdc_find_q931_ie(ieb->elem->ie).descr), ieb->elem->len);
					switch(ieb->elem->ie) {
						case IPDC_Q931_IE_CAUSE:
							memcpy(&cause, ieb->elem->data, sizeof(cause));

							printf("cause class is %s\n", ipdc_find_q931_cause_class(cause[1] & IPDC_Q931_CAUSE_CLASS_MASK));
							printf("cause code is %d/%s\n", cause[1] & IPDC_Q931_CAUSE_CODE_MASK, (ipdc_find_q931_cause_code(cause[1] & IPDC_Q931_CAUSE_CODE_MASK).descr));

							break;
						default:
							printf("Unexpected IE!\n");
							break;
					}

					ieb = ieb->next;
				}

				if (cr->call != NULL) {
					c = cr->call;

					xc = xc_find_call(c);
					if (xc != NULL) {
						printf("got disconnect for crossconnected call!\n");

						if (ipdc_send_rcr(xc->dst, cause[1] & IPDC_Q931_CAUSE_CODE_MASK) < 0) {
							ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
							fprintf(stderr, "couldn't release channel 1/%d/%d/%d\n", xc->dst->l->m->module, xc->dst->l->line, xc->dst->chan);
							retval = -1;
							goto free_all;
						}

						if (ipdc_send_rcr(xc->src, cause[1] & IPDC_Q931_CAUSE_CODE_MASK) < 0) {
							ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
							fprintf(stderr, "couldn't release channel 1/%d/%d/%d\n", xc->src->l->m->module, xc->src->l->line, xc->src->chan);
							retval = -1;
							goto free_all;
						}

						if (ipdc_send_rcr(xc->call, cause[1] & IPDC_Q931_CAUSE_CODE_MASK) < 0) {
							ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
							fprintf(stderr, "couldn't release channel 1/%d/%d/%d\n", xc->call->l->m->module, xc->call->l->line, xc->call->chan);
							retval = -1;
							goto free_all;
						}
					} else {
						if (ipdc_send_rcr(c, cause[1] & IPDC_Q931_CAUSE_CODE_MASK) < 0) {
							ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
							fprintf(stderr, "couldn't release channel 1/%d/%d/%d\n", c->l->m->module, c->l->line, c->chan);
							retval = -1;
							goto free_all;
						}
					}
				} else {
					printf("Got DISCONNECT with NULL call reference?\n");
				}

				break;

			case IPDC_Q931_MSG_RELEASE:
				cr->state = IPDC_Q931_CALL_STATE_U12;

				ieb = ieb_head;
				while (ieb != NULL) {
					printf("ie is %s, length is %d bytes\n", (ipdc_find_q931_ie(ieb->elem->ie).descr), ieb->elem->len);
					switch(ieb->elem->ie) {
						case IPDC_Q931_IE_CAUSE:
							memcpy(&cause, ieb->elem->data, sizeof(cause));

							printf("cause class is %s\n", ipdc_find_q931_cause_class(cause[1] & IPDC_Q931_CAUSE_CLASS_MASK));
							printf("cause code is %d/%s\n", cause[1] & IPDC_Q931_CAUSE_CODE_MASK, (ipdc_find_q931_cause_code(cause[1] & IPDC_Q931_CAUSE_CODE_MASK).descr));

							break;
						default:
							printf("Unexpected IE!\n");
							break;
					}

					ieb = ieb->next;
				}

				if (cr->call != NULL) {
					c = cr->call;

					/* Set call state */
					cr->state = IPDC_Q931_CALL_STATE_U0;

					xc = xc_find_call(c);
					if (xc != NULL) {
						if (ipdc_send_rcr(xc->call, cause[1] & IPDC_Q931_CAUSE_CODE_MASK) < 0) {
							ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
							fprintf(stderr, "couldn't release channel 1/%d/%d/%d\n", xc->call->l->m->module, xc->call->l->line, xc->call->chan);
							retval = -1;
							goto free_all;
						}
					}

					if (ipdc_send_rcr(c, cause[1] & IPDC_Q931_CAUSE_CODE_MASK) < 0) {
						ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE);
						fprintf(stderr, "couldn't release channel 1/%d/%d/%d\n", c->l->m->module, c->l->line, c->chan);
						retval = -1;
						goto free_all;
					}

					if (ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, cause[1]) < 0) {
						fprintf(stderr, "couldn't gracefully release channel 1/%d/%d/%d\n", c->l->m->module, c->l->line, c->chan);
						retval = -1;
						goto free_all;
					}

					/* Remove call reference */
					cr_remove_call_ref(cr);
				} else {
					printf("RELEASE received when no call associated with callref!\n");
					if (ipdc_q931_send_release_complete(sigchan, p->callref | IPDC_Q931_CALLREF_TERM, cause[1]) < 0) {
						fprintf(stderr, "couldn't gracefully release non-existant callref!\n");
						retval = -1;
						goto free_all;
					}

					cr->state = IPDC_Q931_CALL_STATE_U0;
					cr_remove_call_ref(cr);
				}

				break;

			case IPDC_Q931_MSG_RELEASE_COMP:
				ieb = ieb_head;
				while (ieb != NULL) {
					printf("ie is %s, length is %d bytes\n", (ipdc_find_q931_ie(ieb->elem->ie).descr), ieb->elem->len);
					switch(ieb->elem->ie) {
						case IPDC_Q931_IE_CAUSE:
							memcpy(&cause, ieb->elem->data, sizeof(cause));

							printf("cause class is %s\n", ipdc_find_q931_cause_class(cause[1] & IPDC_Q931_CAUSE_CLASS_MASK));
							printf("cause code is %d/%s\n", cause[1] & IPDC_Q931_CAUSE_CODE_MASK, (ipdc_find_q931_cause_code(cause[1] & IPDC_Q931_CAUSE_CODE_MASK).descr));

							break;
						default:
							printf("Unexpected IE!\n");
							break;
					}

					ieb = ieb->next;
				}

				cr->state = IPDC_Q931_CALL_STATE_U0;
				cr_remove_call_ref(cr);
				break;

			default:
				break;
		}
	}

free_all:
	pdu = NULL;
	data = NULL;
	elem = NULL;
	ieb = ieb_tail = ieb_head = NULL;
	calling_num = called_num = NULL;
	/* h = NULL;
	msg = NULL;
	d[0] = d[1] = d[2] = d[3] = d[4] = d[5] = NULL;
	q = NULL; */

	return retval;
}

int msg_handler_ACST(char *buf, uint32_t buflen, struct ipdc_system_info_t *si)
{
	struct ipdc_pkthdr_t *m;
	struct ipdc_pktdata_t *tag;
	uint32_t n, len = IPDC_PKTHDR_T_LEN;
	uint16_t smod_num = 0, sline_num = 0, schan_num = 0;
	uint16_t dmod_num = 0, dline_num = 0, dchan_num = 0;
	struct mod_chans_t *sc = NULL, *dc = NULL;
	struct xconnect_t *xc = NULL;
	struct ipdc_conga_line_t *cl;

	m = (struct ipdc_pkthdr_t *)buf;
	printf("Got a ACST message of %d bytes in %d byte packet from %s\n", ntohs(m->pkt_len), buflen, inet_ntoa(*((struct in_addr *)&si->ip)));

	while((tag = ipdc_pop_tag(buf, buflen, len)) != NULL) {
		len += tag->size + IPDC_PKTDATA_T_LEN;

		switch(tag->tag) {
			case IPDC_TAG_MODULE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				smod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_LINE_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				sline_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_CHANNEL_NUMBER:
				memcpy(&n, tag->data, sizeof(n));
				schan_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_TDM_DEST_MOD:
				memcpy(&n, tag->data, sizeof(n));
				dmod_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_TDM_DEST_LINE:
				memcpy(&n, tag->data, sizeof(n));
				dline_num = ntohl(n) >> (tag->size * 8);
				break;
			case IPDC_TAG_TDM_DEST_CHAN:
				memcpy(&n, tag->data, sizeof(n));
				dchan_num = ntohl(n) >> (tag->size * 8);
				break;
			default:
				printf("WARNING: UNEXPECTED TAG RECEIVED, TAG ID 0x%02x [%s]\n", tag->tag, ipdc_tag_name(tag->tag));
		}

		free(tag);
	}

	if ((cl = conga_find(si, m->trans_id)) != NULL) {
		if (cl->code == IPDC_MSG_RCST) {
			conga_delete(cl);
			printf("CURRENTLY %d ELEMENTS IN CONGA LINE FOR %s\n", conga_count(si), inet_ntoa(*((struct in_addr *)&si->ip)));
		} else {
			fprintf(stderr, "FOUND MATCHING TRANSACTION IN CONGA LINE, BUT INCORRECT MESSAGE!\n");
		}
	}

	if ((sc = mod_find_chan(si, smod_num, sline_num, schan_num)) == NULL) {
		fprintf(stderr, "ERROR: ACST RECEIVED FOR NON-EXISTANT SOURCE CHANNEL!\n");
		return -1;
	}

	if ((dc = mod_find_chan(si, dmod_num, dline_num, dchan_num)) == NULL) {
		fprintf(stderr, "ERROR: ACST RECEIVED FOR NON-EXISTANT DESTINATION CHANNEL!\n");
		return -1;
	}

	if ((xc = xc_find_src(dc)) == NULL) {
		fprintf(stderr, "ERROR: COULD NOT FIND CROSSCONNECT!\n");
		return -1;
	}

	/* set origin channel */
	// xc->call = sc;

	sc->status = IPDC_CHAN_STATUS_CONNECTED;
	dc->status = IPDC_CHAN_STATUS_CONNECTED;

	return 0;
}

void *gw_loop(void *data)
{
	int s = *(int *)data;
	int p, plen, c, kq, n, osin_len, nread, buflen, datalen;
	struct kevent kevin, kevout;
	struct timespec ts = { 0, 0 };
	char *buf;
	struct ipdc_pkthdr_t *m;
	struct sockaddr_in osin;

	struct ipdc_system_info_t *si = NULL;
	struct call_route_t *r = NULL;
	struct trunkgroup_t *tg = NULL;
	struct xconnect_t *xc = NULL;
	struct db_xconnect_t *dbx = NULL;
	struct db_trunkgroup_t *dbt = NULL;
	struct mod_stat_t *mod = NULL;
	struct mod_lines_t *line = NULL;
	struct mod_chans_t *chan = NULL;

	if ((kq = kqueue()) == -1) {
		perror("kqueue");
		pthread_exit(NULL);
	}

	while(1) {
		bzero(&osin, sizeof(struct sockaddr));
		osin_len = sizeof(struct sockaddr);
		c = accept(s, (struct sockaddr *)&osin, &osin_len);
		if (c == -1) {
			perror("accept");
			pthread_exit(NULL);
		}

		EV_SET(&kevin, c, EVFILT_READ, EV_ADD, 0, 0, 0);
		n = kevent(kq, &kevin, 1, &kevout, 1, &ts);
		if (n < 0) {
			perror("kevent");
			pthread_exit(NULL);
		}

		ts.tv_sec = 0; ts.tv_nsec = 500 * 1000000;
		while(1) {
			n = kevent(kq, &kevin, 1, &kevout, 1, &ts);
			if (n < 0) {
				perror("kevent");
				break;
			} else if (n == 0) {
				continue;
			} else {
				if (pthread_mutex_lock(&mtxnet) != 0) {
					perror("pthread_mutex_lock");
					pthread_exit(NULL);
				}

				buflen = kevout.data;
				if (buflen == 0) {
					printf("[pid %d] end-of-file reached on socket\n", getpid());

					/* Find system info structure */
					si = sys_find_by_thread_id(pthread_self());
					if (si == NULL) {
						printf("Could not find sysinfo struct for our previously connected GW!\n");
					} else {
						/* Remove all call routes */
						r = call_route_head;
						while (r != NULL) {
							if (r->ip == si->ip) {
								if (call_route_delete(r) != 0) {
								}
							}
							r = r->next;
						}
						printf("removed call routes\n");

						/* Remove all crossconnects from offline database */
						dbx = dbx_head;
						while (dbx != NULL) {
							if (dbx->s_ip == si->ip || dbx->d_ip == si->ip) {
								if (db_delete_xc(dbx) != 0) {
								}
							}
							dbx = dbx->next;
						}
						printf("removed offline crossconnects\n");

						/* Remove all trunkgroups from offline database */
						dbt = dbt_head;
						while (dbt != NULL) {
							if (dbt->ip == si->ip) {
								if (db_delete_tg(dbt) != 0) {
								}
							}
							dbt = dbt->next;
						}
						printf("removed offline trunk groups\n");

						/* Remove all crossconnects */
						xc = xc_head;
						while (xc != NULL) {
							if (xc->src->si == si || xc->dst->si == si) {
								if (xc_delete(xc) != 0) {
								}
							}
							xc = xc->next;
						}
						printf("removed crossconnects\n");

						/* Remove all trunkgroups */
						tg = tg_head;
						while (tg != NULL) {
							if (tg->si == si) {
								if (tg_delete(tg) != 0) {
								}
							}
							tg = tg->next;
						}
						printf("removed trunk groups\n");

						/* Disconnect all active crossconnects */

						/* Disconnect all active Q.931 calls */

						/* Reset all channels that aren't idle */

						/* Remove all channel structures */
						chan = mod_chan_head;
						while (chan != NULL) {
							if (chan->si == si) {
								printf("deleting 1/%d/%d/%d\n", chan->l->m->module, chan->l->line, chan->chan);
								if (mod_delete_chan(chan) != 0) {
								}
							}
							chan = chan->next;
						}
						printf("removed all channels\n");

						/* Remove all line structures */
						line = mod_line_head;
						while (line != NULL) {
							if (line->si == si) {
								if (mod_delete_line(line) != 0) {
								}
							}
							line = line->next;
						}
						printf("removed all lines\n");

						/* Remove all module structures */
						mod = mod_head;
						while (mod != NULL) {
							if (mod->si == si) {
								if (mod_delete_module(mod) != 0) {
								}
							}
							mod = mod->next;
						}
						printf("removed all modules\n");

						/* Remove system info structure */
						if (sys_del(si->ip) != 0) {
							printf("Couldn't delete system info structure!\n");
						}
						printf("removed system info structure\n");
					}

					pthread_exit(NULL);
				}

				printf("[pid %d] %d bytes of data ready to be picked up\n", getpid(), buflen);
				if ((buf = malloc(buflen)) == NULL) {
					perror("malloc");
					pthread_exit(NULL);
				}

				nread = read(c, buf, buflen);
				if (nread == -1) {
					perror("read");
					pthread_exit(NULL);
				}
				printf("[pid %d] %d bytes of data read successfully\n", getpid(), nread);

				if (nread <= IPDC_PKTHDR_T_LEN) {
					printf("invalid packet length received\n");
					continue;
				}

				p = 0;
				while (p < nread) {
					m = (struct ipdc_pkthdr_t *)(buf + p);
					plen = ntohs(m->pkt_len);

					/* printf("packet size: %d\n", plen);
					printf("protocol id: 0x%02x\n", m->protocol_id);
					printf("transaction id size: %d\n", m->trans_id_len);

					tid = ntohl(m->trans_id);
					printf("transaction id originator: %d\n", tid >> 31);
					printf("transaction id number: %08x\n", tid);
					printf("message code: %02x --> %s (%s)\n", ntohs(m->msg_code), ipdc_msgcode_name(ntohs(m->msg_code)), ipdc_msgcode_descr(ntohs(m->msg_code))); */

					datalen = (plen + 2) - IPDC_PKTHDR_T_LEN;
					// printf("need to parse %d bytes of data\n", datalen);
					// ipdc_print_all_tags(buf + p, datalen, IPDC_PKTHDR_T_LEN);
					// printf("\n");

					if (ipdc_handle_msg(c, buf + p, plen, osin.sin_addr.s_addr, m->trans_id, pthread_self()) == -127) {
						fprintf(stderr, "could not call general message handler\n");
						pthread_exit(NULL);
					}

					p += plen + 2;
				}

				if (pthread_mutex_unlock(&mtxnet) != 0) {
					perror("pthread_mutex_unlock");
					pthread_exit(NULL);
				}

				free(buf);
			}
		}

		printf("[pid %d] closing connection.\n", getpid());
		close(c);
	}

	pthread_exit(NULL);
}

int ipdc_cmd_help(char *arg);
int ipdc_cmd_show(char *arg);
int ipdc_cmd_exit(char *arg);

struct ipdc_cmd {
	char *name;
	char *alias;
	rl_icpfunc_t *func;
	char *descr;
};

struct ipdc_cmd ipdc_cmds[] = {
	{ "help", "?", ipdc_cmd_help, "Show help" },
	{ "show", NULL, ipdc_cmd_show, "Show information" },
	{ "exit", "quit", ipdc_cmd_exit, "Exit the bootleg softswitch" }
};

int ipdc_cmd_help(char *arg)
{
	int i;

	for (i = 0; i < sizeof(ipdc_cmds) / sizeof(struct ipdc_cmd); i++) {
		printf("%s\t\t%s\n", ipdc_cmds[i].name, ipdc_cmds[i].descr);
	}

	return 0;
}

int ipdc_cmd_show(char *arg)
{
	int i, len = 0, offset = strlen("show");
	uint32_t ip;
	char next[256], sp[16], dp[16];
	struct in_addr sna, dna;
	struct ipdc_system_info_t *si = NULL;
	struct mod_stat_t *m = NULL;
	struct mod_lines_t *l = NULL;
	struct mod_chans_t *c = NULL;
	struct call_ref_t *cr = NULL;
	struct call_route_t *r = NULL;
	struct trunkgroup_t *t = NULL;
	struct xconnect_t *xc = NULL;

	arg += offset;
	while(*arg == ' ' && *arg != '\0') {
		arg++;
	}

	len = strlen(arg);
	if (len == 0) {
		printf("invalid command\n");
		return 1;
	}

	i = 0;
	while (*arg != ' ' && *arg != '\0') {
		next[i] = *arg;
		i++; arg++;
	}
	next[i] = '\0';

	while (*arg == ' ' && *arg != '\0') {
		arg++;
	}

	if (strcasecmp(next, "chassis") == 0) {
		i = 0; bzero(&next, sizeof(next));
		while (*arg != ' ' && *arg != '\0') {
			next[i] = *arg;
			i++; arg++;
		}
		next[i] = '\0';

		while (*arg == ' ' && *arg != '\0') {
			arg++;
		}

		if (strlen(next) == 0) {
			printf("Connected Chassis:\n");
			if (si_head == NULL) {
				printf("  (no chassis currently connected)\n");
			} else {
				si = si_head;
				while(si != NULL) {
					printf("  %s\t%s (%s)\n", inet_ntoa(*((struct in_addr *)&si->ip)), si->bay, si->id);
					si = si->next;
				}
			}
		} else {
			ip = inet_addr(next);
			if (ip == INADDR_NONE) {
				printf("invalid chassis ip address\n");
				return 1;
			}

			si = sys_find(ip);
			if (si == NULL) {
				printf("chassis does not exist\n");
				return 1;
			}

			i = 0; bzero(&next, sizeof(next));
			while (*arg != ' ' && *arg != '\0') {
				next[i] = *arg;
				i++; arg++;
			}
			next[i] = '\0';

			while (*arg == ' ' && *arg != '\0') {
				arg++;
			}

			printf("Chassis @ %s\n", inet_ntoa(*((struct in_addr *)&si->ip)));
			if (strlen(next) == 0) {
				printf("  Type: %s\n", si->type);
				printf("  Bay: %s\n", si->bay);
				printf("  ID: %s\n", si->id);
				printf("  # of Modules: %d\n", si->modules);
				printf("  # of Universal Ports: %d\n", si->u_ports);
				printf("  # of HDLC-only Ports: %d\n", si->h_ports);
				printf("\n");
			} else {
				if (strcasecmp(next, "modules") == 0) {
					printf("  Modules:\n");

					m = mod_head;
					while (m != NULL) {
						if (m->si == si) {
							printf("    #%d (%d lines):\t%s\t%s\n", m->module, m->lines, (ipdc_find_modstatus(m->status).descr), (ipdc_find_modtype(m->type).name));
						}
						m = m->next;
					}
				} else if (strcasecmp(next, "lines") == 0) {
					printf("  Lines:\n");

					l = mod_line_head;
					while (l != NULL) {
						if (l->si == si) {
							printf("    1/%d/%d\t\t%s\t%d channels\n", l->m->module, l->line, (ipdc_find_linestatus(l->status).descr), l->chans);
						}
						l = l->next;
					}
				} else if (strcasecmp(next, "channels") == 0) {
					printf("  Channels:\n");

					c = mod_chan_head;
					while (c != NULL) {
						if (c->si == si) {
							printf("    1/%d/%d/%d\t\t%s\n", c->l->m->module, c->l->line, c->chan, (ipdc_find_chanstatus(c->status).descr));
						}
						c = c->next;
					}
				} else if (strcasecmp(next, "q931") == 0) {
					printf("  Q.931:\n");

					cr = call_ref_head;
					while (cr != NULL) {
						if (cr->sig->si == si) {
							printf("    ref#%04x\t%s --> %s\tstate: %s\tcontrolled by 1/%d/%d/%d\n", cr->callref, cr->calling == NULL ? "<no calling party>" : cr->calling, cr->called == NULL ? "<no called party>" : cr->called, ipdc_find_q931_call_state(cr->state), cr->sig->l->m->module, cr->sig->l->line, cr->sig->chan);
						}
						cr = cr->next;
					}
				} else {
					printf("invalid chassis command\n");
					return 1;
				}
			}
		}
	} else if (strcasecmp(next, "calls") == 0) {
	} else if (strcasecmp(next, "crossconnects") == 0) {
		printf("  Cross Connects:\n");

		xc = xc_head;
		while (xc != NULL) {
			sna.s_addr = xc->src->si->ip;
			if (inet_ntop(AF_INET, (struct sockaddr *)&sna, sp, sizeof(sp)) == NULL) {
				perror("inet_ntop");
				return -1;
			}

			dna.s_addr = xc->dst->si->ip;
			if (inet_ntop(AF_INET, (struct sockaddr *)&dna, dp, sizeof(dp)) == NULL) {
				perror("inet_ntop");
				return -1;
			}

			printf("    %s:1/%d/%d/%d\t%s:1/%d/%d/%d\n", sp, xc->src->l->m->module, xc->src->l->line, xc->src->chan, dp, xc->dst->l->m->module, xc->dst->l->line, xc->dst->chan);
			xc = xc->next;
		}
	} else if (strcasecmp(next, "trunkgroups") == 0) {
		printf("  Trunk Groups:\n");

		t = tg_head;
		while (t != NULL) {
			sna.s_addr = t->t->si->ip;
			if (inet_ntop(AF_INET, (struct sockaddr *)&sna, sp, sizeof(sp)) == NULL) {
				perror("inet_ntop");
				return -1;
			}

			printf("    %s:1/%d/%d/%d\tTG#%d\n", sp, t->t->l->m->module, t->t->l->line, t->t->chan, t->tg);
			t = t->next;
		}
	} else if (strcasecmp(next, "callroutes") == 0) {
		printf("  Call Routes:\n");

		r = call_route_head;
		while (r != NULL) {
			sna.s_addr = r->ip;
			if (inet_ntop(AF_INET, (struct sockaddr *)&sna, sp, sizeof(sp)) == NULL) {
				perror("inet_ntop");
				return -1;
			}

			printf("    %s\t%s\tTG#%d\n", r->cpn, sp, r->tg);
			r = r->next;
		}
	} else if (strcasecmp(next, "help") == 0 || strcasecmp(next, "?") == 0) {
		printf("show commands:\n");
		printf("chassis\t\tChassis information\n");
		printf("calls\t\tConnected/pending calls\n");
		printf("crossconnects\tCross-connected DS0s\n");
		printf("trunkgroups\tDefined trunk groups\n");
		printf("callroutes\tCall routing information\n");
		printf("\n");
	} else {
		printf("invalid command\n");
		return 1;
	}

	return 0;
}

int ipdc_cmd_exit(char *arg)
{
	char c[4];
	
	fprintf(stderr, "Are you SURE you want to exit? (yes/no): ");
	if (fgets(c, sizeof(c), stdin) != NULL) {
		if (strcasecmp(c, "yes") == 0) {
			exit(0);
		}
	}

	return 0;
}

void *console_loop(void)
{
	int i;
	char *line = NULL;

	while ((line = readline("customer@NYCMNYZRXXX> ")) != NULL) {
		if (strlen(line) < 255) {
			if (line && *line) {
				add_history(line);
			}

			for (i = 0; i < sizeof(ipdc_cmds) / sizeof(struct ipdc_cmd); i++) {
				if (strncasecmp(line, ipdc_cmds[i].name, strlen(ipdc_cmds[i].name)) == 0 || (ipdc_cmds[i].alias != NULL && strncasecmp(line, ipdc_cmds[i].alias, strlen(ipdc_cmds[i].alias)) == 0)) {
					ipdc_cmds[i].func(line);
				}
			}
		}
	}

	exit(1);
}

int main(int argc, char **argv)
{
	int i, s, mPort, on = 1, tid[5];
	unsigned int mIP;
	struct sockaddr_in isin;
	pthread_t t;
	FILE *fp;
	char data[1024];
	char s_ip[16]; int s_mod, s_line, s_chan;
	char d_ip[16]; int d_mod, d_line, d_chan;
	uint32_t dip, sip;
	char cpn[24]; int tgid;

	if (argc != 3) {
		fprintf(stderr, "%s <ip address> <port>\n", argv[0]);
		return -1;
	}

	if ((mIP = inet_addr(argv[1])) == INADDR_NONE) {
		fprintf(stderr, "%s: invalid ip address specified\n", argv[0]);
		return -1;
	}

	mPort = atoi(argv[2]);
	if (mPort <= 0 || mPort >= 65535) {
		fprintf(stderr, "%s: invalid port specified\n", argv[0]);
		return -1;
	}

	if ((s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		perror("socket");
		return -1;
	}

	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) == -1) {
		perror("setsockopt");
		return -1;
	}

	bzero(&isin, sizeof(struct sockaddr));
	isin.sin_family = AF_INET;
	isin.sin_addr.s_addr = mIP;
	isin.sin_port = htons(mPort);

	if (bind(s, (struct sockaddr *)&isin, sizeof(struct sockaddr)) == -1) {
		perror("bind");
		return -1;
	}

	if (listen(s, 0) == -1) {
		perror("listen");
		return -1;
	}

	ipdc_register_msg_handler(IPDC_MSG_NSUP, 0, msg_handler_SYSINFO);
	ipdc_register_msg_handler(IPDC_MSG_NSI, 0, msg_handler_SYSINFO);
	ipdc_register_msg_handler(IPDC_MSG_NMS, 0, msg_handler_NMS);
	ipdc_register_msg_handler(IPDC_MSG_NLS, 0, msg_handler_NLS);
	ipdc_register_msg_handler(IPDC_MSG_NCS, 0, msg_handler_NCS);
	ipdc_register_msg_handler(IPDC_MSG_TUNL, 0, msg_handler_TUNL);
	ipdc_register_msg_handler(IPDC_MSG_ACSI, 0, msg_handler_ACSI);
	ipdc_register_msg_handler(IPDC_MSG_CONI, 0, msg_handler_CONI);
	ipdc_register_msg_handler(IPDC_MSG_RCR, 0, msg_handler_RCR);
	ipdc_register_msg_handler(IPDC_MSG_ACR, 0, msg_handler_ACR);
	ipdc_register_msg_handler(IPDC_MSG_RSCS, 0, msg_handler_RSCS);
	ipdc_register_msg_handler(IPDC_MSG_ACST, 0, msg_handler_ACST);
	ipdc_register_msg_handler(IPDC_MSG_MRJ, 0, msg_handler_MRJ);
	ipdc_register_msg_handler(IPDC_MSG_UNKNOWN, 0, msg_handler_DEFAULT);

	if (pthread_rwlock_init(&rwlock, NULL) != 0) {
		perror("pthread_mutex_init");
		return -1;
	}

	if (pthread_mutex_init(&mtxnet, NULL) != 0) {
		perror("pthread_mutex_init");
		return -1;
	}

	/* load trunk groups */
	if ((fp = fopen("trunkgroups", "r")) == NULL) {
		perror("fopen");
		return -1;
	}

	while (fgets(data, sizeof(data), fp) != NULL) {
		if (sscanf(data, "%16[^:]:1/%d/%d/%d:%d\n", s_ip, &s_mod, &s_line, &s_chan, &tgid) == 5) {
			sip = inet_addr(s_ip);
			if (db_add_tg(sip, s_mod, s_line, s_chan, tgid) == NULL) {
				fprintf(stderr, "could not add to pre-trunkgroup list\n");
				return -1;
			}
		}
	}

	fclose(fp);

	/* load cross connects */
	if ((fp = fopen("crossconnects", "r")) == NULL) {
		perror("fopen");
		return -1;
	}

	while (fgets(data, sizeof(data), fp) != NULL) {
		if (sscanf(data, "%16[^:]:1/%d/%d/%d <--> %16[^:]:1/%d/%d/%d\n", s_ip, &s_mod, &s_line, &s_chan, d_ip, &d_mod, &d_line, &d_chan) == 8) {
			dip = inet_addr(d_ip); sip = inet_addr(s_ip);
			if (db_add_xc(sip, s_mod, s_line, s_chan, dip, d_mod, d_line, d_chan) == NULL) {
				fprintf(stderr, "could not add to pre-crossconnect list\n");
				return -1;
			}
		}
	}

	fclose(fp);

	/* load call routes */
	if ((fp = fopen("callroutes", "r")) == NULL) {
		perror("fopen");
		return -1;
	}

	while (fgets(data, sizeof(data), fp) != NULL) {
		if (sscanf(data, "%24[^:]:%16[^:]:%d\n", cpn, s_ip, &tgid) == 3) {
			sip = inet_addr(s_ip);
			if (call_route_add(sip, cpn, tgid) == NULL) {
				fprintf(stderr, "could not add to call route list\n");
				return -1;
			}
		}
	}

	fclose(fp);
	
	for (i = 0; i < sizeof(tid); i++) {
		tid[i] = pthread_create(&t, NULL, gw_loop, (void *)&s);
	}

	console_loop();

	close(s);
	return 0;
}
