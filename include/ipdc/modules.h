#ifndef IPDC_MODULES_H
#define IPDC_MODULES_H

enum {
	IPDC_MODULE_UNKNOWN = 0x01,
	IPDC_MODULE_SHELF_CONTROLLER = 0x02,
	IPDC_MODULE_ROUTER = 0x03,
	IPDC_MODULE_8PT_CHAN_T1 = 0x04,
	IPDC_MODULE_8PT_CHAN_E1 = 0x05,
	IPDC_MODULE_48PT_MODEM = 0x06,
	IPDC_MODULE_192PT_HDLC = 0x07,
	IPDC_MODULE_4PT_ETH = 0x08,
	IPDC_MODULE_SERIAL_WAN = 0x09,
	IPDC_MODULE_HSSI = 0x0A,
	IPDC_MODULE_10PT_UNCHAN_T1 = 0x0B,
	IPDC_MODULE_36PT_MODEM = 0x0C,
	IPDC_MODULE_T3 = 0x0D,
	IPDC_MODULE_48PT_MODEM_56K = 0x0E,
	IPDC_MODULE_FORWARD = 0x0F,
	IPDC_MODULE_SDSL = 0x10,
	IPDC_MODULE_CAP_ADSL = 0x11,
	IPDC_MODULE_DMT_ADSL = 0x12,
	IPDC_MODULE_SMC = 0x13,
	IPDC_MODULE_32PT_IDSL = 0x14,
	IPDC_MODULE_10PT_UNCHAN_E1 = 0x15,
	IPDC_MODULE_36PT_MODEM_ANALOG2 = 0x16,
	IPDC_MODULE_CSMX_MODEM = 0x17,
	IPDC_MODULE_UDS3 = 0x18,
	IPDC_MODULE_DS3_ATM = 0x19,
	IPDC_MODULE_4PT_ETH2 = 0x1A,
	IPDC_MODULE_192PT_HDLC2 = 0x1B,
	IPDC_MODULE_SDSL_70_DATA = 0x1C,
	IPDC_MODULE_MADD = 0x1D,
	IPDC_MODULE_SDSL_70_VOICE = 0x1E,
	IPDC_MODULE_OC3_DAUGHTER = 0x1F,
	IPDC_MODULE_OC3_ATM = 0x20,
	IPDC_MODULE_4PT_ETH3 = 0x21,
	IPDC_MODULE_SRS_ETH = 0x22,
	IPDC_MODULE_SDSL_ATM = 0x23,
	IPDC_MODULE_AL_DADSL_ATM = 0x24,
	IPDC_MODULE_CSM3V_MODEM = 0x25,
	IPDC_MODULE_HDLC2EC = 0x26,
	IPDC_MODULE_DS3_DAUGHTER = 0x27,
	IPDC_MODULE_2PT_ETH = 0x28,
	IPDC_MODULE_STM0 = 0x2A,
	IPDC_MODULE_SDSL_RIPPER = 0x2B,
	IPDC_MODULE_STINGER_TERM = 0x2F,
	IPDC_MODULE_GS_DADSL = 0x30,
	IPDC_MODULE_PCTFIT = 0x31,
	IPDC_MODULE_PCTFIE = 0x32,
	IPDC_MODULE_CT_DADSL_GLITE = 0x33,
	IPDC_MODULE_DS3_ATM2 = 0x34,
	IPDC_MODULE_E3_ATM = 0x35,
	IPDC_MODULE_24PT_STINGER_IMA_T1 = 0x36,
	IPDC_MODULE_MADD2 = 0x37,
	IPDC_MODULE_GS_HDSL_2 = 0x38,
	IPDC_MODULE_32PT_STINGER_IDSL = 0x39,
	IPDC_MODULE_ANNEXB_DADSL_ATM = 0x3A,
	IPDC_MODULE_24PT_STINGER_IMA_E1 = 0x3B,
	IPDC_MODULE_40C_CT_DADSL_ATM = 0x3C,
	IPDC_MODULE_4PT_ETH3_PLUS = 0x3D,
	IPDC_MODULE_CLPMT = 0x3E,
	IPDC_MODULE_CLPME = 0x3F,
	IPDC_MODULE_E3_DAUGHTER = 0x40,
	IPDC_MODULE_8PT_STINGER_IMA_T1 = 0x41,
	IPDC_MODULE_8PT_STINGER_IMA_E1 = 0x42,
	IPDC_MODULE_48A_GS_DADSL_ATM = 0x43,
	IPDC_MODULE_48B_GS_DADSL_ATM = 0x44,
	IPDC_MODULE_48C_GS_DADSL_ATM = 0x45,
	IPDC_MODULE_40A_CT_DADSL_ATM = 0x46,
	IPDC_MODULE_OC3_ATM2 = 0x47,
	IPDC_MODULE_4PT_SERIAL_WAN2 = 0x48,
	IPDC_MODULE_CDS3_LIM = 0x49,
	IPDC_MODULE_R7000 = 0x4A,
	IPDC_MODULE_VPN = 0x4B,
	IPDC_MODULE_HSE = 0x4C,
	IPDC_MODULE_MADD3 = 0x4D,
	IPDC_MODULE_STINGER_CM_V2 = 0x4E,
	IPDC_MODULE_COC3_LIM = 0x4F,
	IPDC_MODULE_STINGER_SHDSL = 0x50,
	IPDC_MODULE_2PT_OC3_4PT_DS3_DAUGHTER = 0x51,
	IPDC_MODULE_72PT_DADSL_ATM = 0x52,
	IPDC_MODULE_36PT_DMT_MRT = 0x53,
	IPDC_MODULE_24PT_T1 = 0x54,
	IPDC_MODULE_24PT_E1 = 0x55,
	IPDC_MODULE_STINGER_MRT_CM = 0x56,
	IPDC_MODULE_CSTM1_LIM = 0x57
};

struct ipdc_mod_types_t {
	uint8_t type;
	char *name;
};

struct ipdc_mod_types_t ipdc_mod_types[] = {
	{ IPDC_MODULE_UNKNOWN, "Unknown" },
	{ IPDC_MODULE_SHELF_CONTROLLER, "Shelf controller" },
	{ IPDC_MODULE_ROUTER, "Router" },
	{ IPDC_MODULE_8PT_CHAN_T1, "8-port channelized T1" },
	{ IPDC_MODULE_8PT_CHAN_E1, "8-port channelized E1" },
	{ IPDC_MODULE_48PT_MODEM, "48-port modem" },
	{ IPDC_MODULE_192PT_HDLC, "192-port HDLC" },
	{ IPDC_MODULE_4PT_ETH, "4-port Ethernet" },
	{ IPDC_MODULE_SERIAL_WAN, "Serial WAN" },
	{ IPDC_MODULE_HSSI, "HSSI" },
	{ IPDC_MODULE_10PT_UNCHAN_T1, "10-port unchannelized T1" },
	{ IPDC_MODULE_36PT_MODEM, "36-modem (analog)" },
	{ IPDC_MODULE_T3, "T3" },
	{ IPDC_MODULE_48PT_MODEM_56K, "48-modem (56k)" },
	{ IPDC_MODULE_FORWARD, "Forward" },
	{ IPDC_MODULE_SDSL, "SDSL" },
	{ IPDC_MODULE_CAP_ADSL, "ADSL CAP" },
	{ IPDC_MODULE_DMT_ADSL, "ADSL DMT" },
	{ IPDC_MODULE_SMC, "Standalone modem controller" },
	{ IPDC_MODULE_32PT_IDSL, "32-port IDSL" },
	{ IPDC_MODULE_10PT_UNCHAN_E1, "10-port unchannelized E1" },
	{ IPDC_MODULE_36PT_MODEM_ANALOG2, "36-modem (analog, v2)" },
	{ IPDC_MODULE_CSMX_MODEM, "CSMX modem" },
	{ IPDC_MODULE_UDS3, "Unchannelized DS3" },
	{ IPDC_MODULE_DS3_ATM, "DS3 ATM" },
	{ IPDC_MODULE_4PT_ETH2, "4-port Ethernet (v2)" },
	{ IPDC_MODULE_192PT_HDLC2, "192-port HDLC (v2)" },
	{ IPDC_MODULE_SDSL_70_DATA, "SDSL 70 (data)" },
	{ IPDC_MODULE_MADD, "MADD" },
	{ IPDC_MODULE_SDSL_70_VOICE, "SDSL 70 (voice)" },
	{ IPDC_MODULE_OC3_DAUGHTER, "OC3 daughter" },
	{ IPDC_MODULE_OC3_ATM, "OC3 ATM" },
	{ IPDC_MODULE_4PT_ETH3, "4-port Ethernet (v3)" },
	{ IPDC_MODULE_SRS_ETH, "SRS Ethernet" },
	{ IPDC_MODULE_SDSL_ATM, "SDSL ATM" },
	{ IPDC_MODULE_AL_DADSL_ATM, "AL DADSL ATM" },
	{ IPDC_MODULE_CSM3V_MODEM, "CSM3V modem" },
	{ IPDC_MODULE_HDLC2EC, "HDLC EC (v2)" },
	{ IPDC_MODULE_DS3_DAUGHTER, "DS3 daughter" },
	{ IPDC_MODULE_2PT_ETH, "2-port Ethernet" },
	{ IPDC_MODULE_STM0, "STM0" },
	{ IPDC_MODULE_SDSL_RIPPER, "SDSL Ripper" },
	{ IPDC_MODULE_STINGER_TERM, "Stinger Terminator" },
	{ IPDC_MODULE_GS_DADSL, "GS DADSL" },
	{ IPDC_MODULE_PCTFIT, "PCTFIT" },
	{ IPDC_MODULE_PCTFIE, "PCTFIE" },
	{ IPDC_MODULE_CT_DADSL_GLITE, "CT DADSL G.lite" },
	{ IPDC_MODULE_DS3_ATM2, "DS3 ATM (v2)" },
	{ IPDC_MODULE_E3_ATM, "E3 ATM" },
	{ IPDC_MODULE_24PT_STINGER_IMA_T1, "24-port Stinger IMA T1" },
	{ IPDC_MODULE_MADD2, "MADD (v2)" },
	{ IPDC_MODULE_GS_HDSL_2, "GS HDSL (v2)" },
	{ IPDC_MODULE_32PT_STINGER_IDSL, "32-port Stinger IDSL" },
	{ IPDC_MODULE_ANNEXB_DADSL_ATM, "ANNEXB DADSL ATM" },
	{ IPDC_MODULE_24PT_STINGER_IMA_E1, "24-port Stinger IMA E1" },
	{ IPDC_MODULE_40C_CT_DADSL_ATM, "40C CT DADSL ATM" },
	{ IPDC_MODULE_4PT_ETH3_PLUS, "4-port Ethernet (v3+)" },
	{ IPDC_MODULE_CLPMT, "CLPMT" },
	{ IPDC_MODULE_CLPME, "CLPME" },
	{ IPDC_MODULE_E3_DAUGHTER, "E3 Daughter" },
	{ IPDC_MODULE_8PT_STINGER_IMA_T1, "8-port Stinger IMA T1" },
	{ IPDC_MODULE_8PT_STINGER_IMA_E1, "8-port Stinger IMA E1" },
	{ IPDC_MODULE_48A_GS_DADSL_ATM, "48A GS DADSL" },
	{ IPDC_MODULE_48B_GS_DADSL_ATM, "48B GS DADSL" },
	{ IPDC_MODULE_48C_GS_DADSL_ATM, "48C GS DADSL" },
	{ IPDC_MODULE_40A_CT_DADSL_ATM, "40A CT DADSL" },
	{ IPDC_MODULE_OC3_ATM2, "OC3 ATM (v2)" },
	{ IPDC_MODULE_4PT_SERIAL_WAN2, "4-port Serial WAN (v2)" },
	{ IPDC_MODULE_CDS3_LIM, "Channelized DS3 LIM" },
	{ IPDC_MODULE_R7000, "R7000" },
	{ IPDC_MODULE_VPN, "VPN" },
	{ IPDC_MODULE_HSE, "HSE" },
	{ IPDC_MODULE_MADD3, "MADD (v3)" },
	{ IPDC_MODULE_STINGER_CM_V2, "Stinger CM (v2)" },
	{ IPDC_MODULE_COC3_LIM, "Channelized OC3 LIM" },
	{ IPDC_MODULE_STINGER_SHDSL, "Stinger SHDSL" },
	{ IPDC_MODULE_2PT_OC3_4PT_DS3_DAUGHTER, "2-port OC3, 4-port DS3 daughter" },
	{ IPDC_MODULE_72PT_DADSL_ATM, "72-port DADSL ATM" },
	{ IPDC_MODULE_36PT_DMT_MRT, "36-port DMT MRT" },
	{ IPDC_MODULE_24PT_T1, "24-port T1" },
	{ IPDC_MODULE_24PT_E1, "24-port E1" },
	{ IPDC_MODULE_STINGER_MRT_CM, "Stinger MRT CM" },
	{ IPDC_MODULE_CSTM1_LIM, "Channelized STM1 LIM" }
};	

#define IPDC_MOD_TYPE_LEN	sizeof(ipdc_mod_types) / sizeof(struct ipdc_mod_types_t)

enum {
	IPDC_MOD_STATUS_EMPTY = 0x00,
	IPDC_MOD_STATUS_DOWN = 0x01,
	IPDC_MOD_STATUS_UP = 0x02,
	IPDC_MOD_STATUS_ERROR = 0x03,
	IPDC_MOD_STATUS_NONEXISTENT = 0x04,
	IPDC_MOD_STATUS_UNKNOWN
};

struct ipdc_mod_status_t {
	uint8_t type;
	char *descr;
};

struct ipdc_mod_status_t ipdc_mod_status[] = {
	{ IPDC_MOD_STATUS_EMPTY, "Empty" },
	{ IPDC_MOD_STATUS_DOWN, "Out-of-service" },
	{ IPDC_MOD_STATUS_UP, "In-service" },
	{ IPDC_MOD_STATUS_ERROR, "Error" },
	{ IPDC_MOD_STATUS_NONEXISTENT, "Does not exist" },
	{ IPDC_MOD_STATUS_UNKNOWN, "Unknown" }
};
#define IPDC_MOD_STATUS_LEN	sizeof(ipdc_mod_status) / sizeof(struct ipdc_mod_status_t)

enum {
	IPDC_LINE_STATUS_NOT_PRESENT = 0x00,
	IPDC_LINE_STATUS_DISABLED = 0x01,
	IPDC_LINE_STATUS_RED_ALARM = 0x02,
	IPDC_LINE_STATUS_YELLOW_ALARM = 0x03,
	IPDC_LINE_STATUS_OTHER_ALARM = 0x04,
	IPDC_LINE_STATUS_UP = 0x05,
	IPDC_LINE_STATUS_LOOPBACK = 0x06,
	IPDC_LINE_STATUS_UNKNOWN
};

struct ipdc_line_status_t {
	uint8_t type;
	char *descr;
};

struct ipdc_line_status_t ipdc_line_status[] = {
	{ IPDC_LINE_STATUS_NOT_PRESENT, "Not present" },
	{ IPDC_LINE_STATUS_DISABLED, "Disabled" },
	{ IPDC_LINE_STATUS_RED_ALARM, "Red alarm" },
	{ IPDC_LINE_STATUS_YELLOW_ALARM, "Yellow alarm" },
	{ IPDC_LINE_STATUS_OTHER_ALARM, "Other alarm" },
	{ IPDC_LINE_STATUS_UP, "Up" },
	{ IPDC_LINE_STATUS_LOOPBACK, "Loopback" },
	{ IPDC_LINE_STATUS_UNKNOWN, "Unknown" }
};
#define IPDC_LINE_STATUS_LEN	sizeof(ipdc_line_status) / sizeof(struct ipdc_line_status_t)

enum {
	IPDC_CHAN_STATUS_NOT_PRESENT = 0x00,
	IPDC_CHAN_STATUS_OOS = 0x01,
	IPDC_CHAN_STATUS_MAINT = 0x03,
	IPDC_CHAN_STATUS_BLOCKED = 0x04,
	IPDC_CHAN_STATUS_IDLE = 0x06,
	IPDC_CHAN_STATUS_IN_USE = 0x07,
	IPDC_CHAN_STATUS_CONNECTED = 0x08,
	IPDC_CHAN_STATUS_LOOPBACK = 0x05,
	IPDC_CHAN_STATUS_ON_HOOK = 0x50,
	IPDC_CHAN_STATUS_OFF_HOOK = 0x51,
	IPDC_CHAN_STATUS_UNKNOWN
};

struct ipdc_chan_status_t {
	uint8_t type;
	char *descr;
};

struct ipdc_chan_status_t ipdc_chan_status[] = {
	{ IPDC_CHAN_STATUS_NOT_PRESENT, "Not present" },
	{ IPDC_CHAN_STATUS_OOS, "Out-of-service" },
	{ IPDC_CHAN_STATUS_MAINT, "Maintenance" },
	{ IPDC_CHAN_STATUS_BLOCKED, "Blocked" },
	{ IPDC_CHAN_STATUS_IDLE, "Idle" },
	{ IPDC_CHAN_STATUS_IN_USE, "In use" },
	{ IPDC_CHAN_STATUS_CONNECTED, "Connected" },
	{ IPDC_CHAN_STATUS_LOOPBACK, "Loopback" },
	{ IPDC_CHAN_STATUS_ON_HOOK, "On hook" },
	{ IPDC_CHAN_STATUS_OFF_HOOK, "Off hook" },
	{ IPDC_CHAN_STATUS_UNKNOWN, "Unknown" }
};
#define IPDC_CHAN_STATUS_LEN	sizeof(ipdc_chan_status) / sizeof(struct ipdc_chan_status_t)

struct mod_stat_t {
	struct ipdc_system_info_t *si;
	uint16_t module;
	uint8_t type;
	uint8_t status;
	uint16_t lines;
	struct mod_stat_t *next;
};

struct mod_lines_t {
	struct ipdc_system_info_t *si;
	struct mod_stat_t *m;
	uint16_t line;
	uint8_t status;
	uint16_t chans;
	struct mod_lines_t *next;
};

struct mod_chans_t {
	struct ipdc_system_info_t *si;
	struct mod_lines_t *l;
	uint16_t chan;
	uint8_t status;
	uint32_t tid;
	struct mod_chans_t *next;
};

struct xconnect_t {
	struct mod_chans_t *src;
	struct mod_chans_t *dst;
	struct mod_chans_t *call;
	struct xconnect_t *next;
};

struct db_xconnect_t {
	uint32_t s_ip;
	uint16_t s_module;
	uint16_t s_line;
	uint16_t s_chan;
	uint32_t d_ip;
	uint16_t d_module;
	uint16_t d_line;
	uint16_t d_chan;
	struct db_xconnect_t *next;
};

struct trunkgroup_t {
	struct ipdc_system_info_t *si;
	uint16_t tg;
	struct mod_chans_t *t;
	struct trunkgroup_t *next;
};

struct db_trunkgroup_t {
	uint32_t ip;
	uint16_t module;
	uint16_t line;
	uint16_t chan;
	uint16_t tg;
	struct db_trunkgroup_t *next;
};

struct call_route_t {
	uint32_t ip;
	char cpn[24];		/* Q.931 specification calls for maximum of 24 digits */
	uint8_t cpn_len;
	uint16_t tg;
	struct call_route_t *next;
};

#endif /* IPDC_MODULES_H */
