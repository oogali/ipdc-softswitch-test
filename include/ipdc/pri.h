#ifndef IPDC_Q931_H

struct ipdc_q931_t {
	uint8_t pd;
	uint8_t crlen:4;
	uint8_t n0:4;
	uint16_t callref;
	uint8_t msgtype;
	unsigned char ie[];
};

#define IPDC_Q931_HDR_LEN	5

enum {
	IPDC_Q931_MSG_ALERTING = 0x01,
	IPDC_Q931_MSG_CALL_PROCEEDING = 0x02,
	IPDC_Q931_MSG_CONNECT = 0x07,
	IPDC_Q931_MSG_CONNECT_ACK = 0x0F,
	IPDC_Q931_MSG_PROGRESS = 0x03,
	IPDC_Q931_MSG_SETUP = 0x05,
	IPDC_Q931_MSG_SETUP_ACK = 0x0D,
	IPDC_Q931_MSG_RESUME = 0x26,
	IPDC_Q931_MSG_RESUME_ACK = 0x2E,
	IPDC_Q931_MSG_RESUME_REJ = 0x22,
	IPDC_Q931_MSG_SUSPEND = 0x25,
	IPDC_Q931_MSG_SUSPEND_ACK = 0x2D,
	IPDC_Q931_MSG_SUSPEND_REJ = 0x21,
	IPDC_Q931_MSG_USER_INFO = 0x20,
	IPDC_Q931_MSG_DISCONNECT = 0x45,
	IPDC_Q931_MSG_RELEASE = 0x4D,
	IPDC_Q931_MSG_RELEASE_COMP = 0x5A,
	IPDC_Q931_MSG_RESTART = 0x46,
	IPDC_Q931_MSG_RESTART_ACK = 0x4E,
	IPDC_Q931_MSG_SEGMENT = 0x60,
	IPDC_Q931_MSG_CONGESTION_CTRL = 0x79,
	IPDC_Q931_MSG_INFORMATION = 0x7B,
	IPDC_Q931_MSG_NOTIFY = 0x6E,
	IPDC_Q931_MSG_STATUS = 0x7D,
	IPDC_Q931_MSG_STATUS_ENQUIRY = 0x75,
	IPDC_Q931_MSG_UNKNOWN
};

struct ipdc_q931_msg_t {
	uint8_t msg;
	char *descr;
};

struct ipdc_q931_msg_t ipdc_q931_msgs[] = {
	{ IPDC_Q931_MSG_ALERTING, "Alerting" },
	{ IPDC_Q931_MSG_CALL_PROCEEDING, "Call proceeding" },
	{ IPDC_Q931_MSG_CONNECT, "Connect" },
	{ IPDC_Q931_MSG_CONNECT_ACK, "Connect acknowledgment" },
	{ IPDC_Q931_MSG_PROGRESS, "Progress" },
	{ IPDC_Q931_MSG_SETUP, "Setup" },
	{ IPDC_Q931_MSG_SETUP_ACK, "Setup acknowledgment" },
	{ IPDC_Q931_MSG_RESUME, "Resume" },
	{ IPDC_Q931_MSG_RESUME_ACK, "Resume acknowledgment" },
	{ IPDC_Q931_MSG_RESUME_REJ, "Resume reject" },
	{ IPDC_Q931_MSG_SUSPEND, "Suspend" },
	{ IPDC_Q931_MSG_SUSPEND_ACK, "Suspend acknowledgment" },
	{ IPDC_Q931_MSG_SUSPEND_REJ, "Suspend reject" },
	{ IPDC_Q931_MSG_USER_INFO, "User information" },
	{ IPDC_Q931_MSG_DISCONNECT, "Disconnect" },
	{ IPDC_Q931_MSG_RELEASE, "Release" },
	{ IPDC_Q931_MSG_RELEASE_COMP, "Release complete" },
	{ IPDC_Q931_MSG_RESTART, "Restart" },
	{ IPDC_Q931_MSG_RESTART_ACK, "Restart acknowledgment" },
	{ IPDC_Q931_MSG_SEGMENT, "Segment" },
	{ IPDC_Q931_MSG_CONGESTION_CTRL, "Congestion control" },
	{ IPDC_Q931_MSG_INFORMATION, "Information" },
	{ IPDC_Q931_MSG_NOTIFY, "Notify" },
	{ IPDC_Q931_MSG_STATUS, "Status" },
	{ IPDC_Q931_MSG_STATUS_ENQUIRY, "Status enquiry" },
	{ IPDC_Q931_MSG_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_MSG_LEN	sizeof(ipdc_q931_msgs) / sizeof(struct ipdc_q931_msg_t)

enum {
	IPDC_Q931_PROTO_Q931 = 0x08,
	IPDC_Q931_PROTO_Q293 = 0x09,
	IPDC_Q931_PROTO_UNKNOWN
};

struct ipdc_q931_proto_t {
	uint8_t proto;
	char *descr;
};

struct ipdc_q931_proto_t ipdc_q931_protos[] = {
	{ IPDC_Q931_PROTO_Q931, "Q.931" },
	{ IPDC_Q931_PROTO_Q293, "Q.293" },
	{ IPDC_Q931_PROTO_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_PROTO_LEN	sizeof(ipdc_q931_protos) / sizeof(struct ipdc_q931_proto_t)

typedef enum {
	IPDC_Q931_IE_SINGLE_OCTET,
	IPDC_Q931_IE_VAR_OCTET
} ipdc_ie_type_t;

enum {
	IPDC_Q931_IE_RESERVED = 0x80,
	IPDC_Q931_IE_SHIFT = 0x90,
	IPDC_Q931_IE_MORE_DATA = 0xA0,
	IPDC_Q931_IE_SENDING_COMP = 0xA1,
	IPDC_Q931_IE_CONGESTION_LEVEL = 0xB0,
	IPDC_Q931_IE_REPEAT_IND = 0xD0,
	IPDC_Q931_IE_SEGMENTED_MSG = 0x00,
	IPDC_Q931_IE_BEARER_CAP = 0x04,
	IPDC_Q931_IE_CAUSE = 0x08,
	IPDC_Q931_IE_CALL_IDENTITY = 0x10,
	IPDC_Q931_IE_CALL_STATE = 0x14,
	IPDC_Q931_IE_CHANNEL_ID = 0x18,
	IPDC_Q931_IE_PROGRESS_IND = 0x1E,
	IPDC_Q931_IE_NSF = 0x20,
	IPDC_Q931_IE_NOTIFY_IND = 0x27,
	IPDC_Q931_IE_DISPLAY = 0x28,
	IPDC_Q931_IE_DATETIME = 0x29,
	IPDC_Q931_IE_KEYPAD_FAC = 0x2C,
	IPDC_Q931_IE_SIGNAL = 0x34,
	IPDC_Q931_IE_INFO_RATE = 0x40,
	IPDC_Q931_IE_END_TO_END_DELAY = 0x42,
	IPDC_Q931_IE_TRANSIT_DELAY_SEL = 0x43,
	IPDC_Q931_IE_PKT_LAYER_PARAMS = 0x44,
	IPDC_Q931_IE_PKT_LAYER_WNDSIZE = 0x45,
	IPDC_Q931_IE_PKT_SIZE = 0x46,
	IPDC_Q931_IE_CUG = 0x47,
	IPDC_Q931_IE_REV_CHARGING_IND = 0x4A,
	IPDC_Q931_IE_CALLING_PARTY_NUM = 0x6C,
	IPDC_Q931_IE_CALLING_PARTY_SUBADDR = 0x6D,
	IPDC_Q931_IE_CALLED_PARTY_NUM = 0x70,
	IPDC_Q931_IE_CALLED_PARTY_SUBADDR = 0x71,
	IPDC_Q931_IE_REDIRECT_NUM = 0x74,
	IPDC_Q931_IE_TRANSIT_NTWK_SEL = 0x78,
	IPDC_Q931_IE_RESTART_IND = 0x79,
	IPDC_Q931_IE_LOW_LAYER_COMPAT = 0x7C,
	IPDC_Q931_IE_HIGH_LAYER_COMPAT = 0x7D,
	IPDC_Q931_IE_USER_USER = 0x7E,
	IPDC_Q931_IE_ESCAPE_FOR_EXT = 0x7F,
	IPDC_Q931_IE_UNKNOWN
};

struct ipdc_q931_ie_t {
	uint8_t ie;
	unsigned short min_len;
	unsigned short max_len;
	char *descr;
	ipdc_ie_type_t type;
};

struct ipdc_q931_ie_t ipdc_q931_ies[] = {
	{ IPDC_Q931_IE_RESERVED, 1, 1, "Reserved", IPDC_Q931_IE_SINGLE_OCTET },
	{ IPDC_Q931_IE_SHIFT, 1, 1, "Shift", IPDC_Q931_IE_SINGLE_OCTET },
	{ IPDC_Q931_IE_MORE_DATA, 1, 1, "More data", IPDC_Q931_IE_SINGLE_OCTET },
	{ IPDC_Q931_IE_SENDING_COMP, 1, 1, "Sending complete", IPDC_Q931_IE_SINGLE_OCTET },
	{ IPDC_Q931_IE_CONGESTION_LEVEL, 1, 1, "Congestion level", IPDC_Q931_IE_SINGLE_OCTET },
	{ IPDC_Q931_IE_REPEAT_IND, 1, 1, "Repeat indicator", IPDC_Q931_IE_SINGLE_OCTET },
	{ IPDC_Q931_IE_SEGMENTED_MSG, 0, 4, "Segmented message", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_BEARER_CAP, 0, 12, "Bearer capability", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_CAUSE, 0, 32, "Cause", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_CALL_IDENTITY, 0, 10, "Call identity", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_CALL_STATE, 0, 3, "Call state", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_CHANNEL_ID, 0, 256, "Channel identification", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_PROGRESS_IND, 4, 4, "Progress indicator", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_NSF, 0, 256, "Network-specific facilities", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_NOTIFY_IND, 3, 3, "Notification indicator", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_DISPLAY, 0, 82, "Display", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_DATETIME, 8, 8, "Date/time", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_KEYPAD_FAC, 0, 34, "Keypad facility", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_SIGNAL, 3, 3, "Signal", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_INFO_RATE, 6, 6, "Information rate", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_END_TO_END_DELAY, 11, 11, "End-to-end transit delay", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_TRANSIT_DELAY_SEL, 5, 5, "Transit delay selection and indication", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_PKT_LAYER_PARAMS, 3, 3, "Packet layer binary parameters", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_PKT_LAYER_WNDSIZE, 4, 4, "Packet layer window size", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_PKT_SIZE, 4, 4, "Packet size", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_CUG, 7, 7, "Closed user group", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_REV_CHARGING_IND, 3, 3, "Reverse charging indicator", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_CALLING_PARTY_NUM, 0, 256, "Calling party number", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_CALLING_PARTY_SUBADDR, 0, 23, "Calling party subaddress", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_CALLED_PARTY_NUM, 0, 256, "Called party number", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_CALLED_PARTY_SUBADDR, 0, 23, "Called party subaddress", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_REDIRECT_NUM, 0, 256, "Redirecting number", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_TRANSIT_NTWK_SEL, 0, 256, "Transit network selection", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_RESTART_IND, 3, 3, "Restart indicator", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_LOW_LAYER_COMPAT, 18, 18, "Low layer compatibility", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_HIGH_LAYER_COMPAT, 5, 5, "High layer compatibility", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_USER_USER, 0, 131, "User-user", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_ESCAPE_FOR_EXT, 0, 256, "Escape for extension", IPDC_Q931_IE_VAR_OCTET },
	{ IPDC_Q931_IE_UNKNOWN, 0, 0, "Unknown", IPDC_Q931_IE_SINGLE_OCTET }
};
#define IPDC_Q931_IE_LEN	sizeof(ipdc_q931_ies) / sizeof(struct ipdc_q931_ie_t)

struct ipdc_q931_elem_t {
	uint8_t ie;
	uint8_t len;
	unsigned char data[];
};
#define IPDC_Q931_ELEM_T_LEN	2

enum {
	IPDC_Q931_SIG_PROTO_Q931 = 0x01,
	IPDC_Q931_SIG_PROTO_DMS = 0x02,
	IPDC_Q931_SIG_PROTO_5ESS = 0x03,
	IPDC_Q931_SIG_PROTO_EURO = 0x04,
	IPDC_Q931_SIG_PROTO_UNKNOWN
};

struct ipdc_q931_sig_proto_t {
	uint16_t sig_proto;
	char *descr;
};

struct ipdc_q931_sig_proto_t ipdc_q931_sig_protos[] = {
	{ IPDC_Q931_SIG_PROTO_Q931, "Q.931" },
	{ IPDC_Q931_SIG_PROTO_DMS, "ISDN (Nortel DMS)" },
	{ IPDC_Q931_SIG_PROTO_5ESS, "ISDN (AT&T/Lucent 5ESS)" },
	{ IPDC_Q931_SIG_PROTO_EURO, "ISDN (European)" },
	{ IPDC_Q931_SIG_PROTO_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_SIG_PROTO_LEN	sizeof(ipdc_q931_sig_protos) / sizeof(struct ipdc_q931_sig_proto_t)

struct ie_bundle_t {
	struct ipdc_q931_elem_t *elem;
	struct ie_bundle_t *next;
};

enum {
	IPDC_Q931_NUMBER_TYPE_UNKNOWN = 0x00,
	IPDC_Q931_NUMBER_TYPE_INTL = 0x10,
	IPDC_Q931_NUMBER_TYPE_NATL = 0x20,
	IPDC_Q931_NUMBER_TYPE_NSF = 0x21,
	IPDC_Q931_NUMBER_TYPE_SUBSCR = 0x40,
	IPDC_Q931_NUMBER_TYPE_ABBREV = 0x60,
	IPDC_Q931_NUMBER_TYPE_EXTEN = 0x70,
	IPDC_Q931_NUMBER_TYPE_RESERVED
};

struct ipdc_q931_num_type_t {
	uint8_t type;
	char *descr;
};

struct ipdc_q931_num_type_t ipdc_q931_num_types[] = {
	{ IPDC_Q931_NUMBER_TYPE_UNKNOWN, "Unknown" },
	{ IPDC_Q931_NUMBER_TYPE_INTL, "International" },
	{ IPDC_Q931_NUMBER_TYPE_NATL, "National" },
	{ IPDC_Q931_NUMBER_TYPE_NSF, "Network-specific facilities" },
	{ IPDC_Q931_NUMBER_TYPE_SUBSCR, "Subscriber" },
	{ IPDC_Q931_NUMBER_TYPE_ABBREV, "Abbreviation" },
	{ IPDC_Q931_NUMBER_TYPE_EXTEN, "Extension" },
	{ IPDC_Q931_NUMBER_TYPE_RESERVED, "Reserved" }
};
#define IPDC_Q931_NUM_TYPES_LEN	sizeof(ipdc_q931_num_types) / sizeof(struct ipdc_q931_num_type_t)

enum {
	IPDC_Q931_NUMBER_PLAN_UNKNOWN = 0x00,
	IPDC_Q931_NUMBER_PLAN_ISDN = 0x01,
	IPDC_Q931_NUMBER_PLAN_DATA = 0x03,
	IPDC_Q931_NUMBER_PLAN_TELEX = 0x04,
	IPDC_Q931_NUMBER_PLAN_NATIONAL = 0x08,
	IPDC_Q931_NUMBER_PLAN_PRIVATE = 0x09,
	IPDC_Q931_NUMBER_PLAN_EXTEN = 0x0F,
	IPDC_Q931_NUMBER_PLAN_RESERVED
};

struct ipdc_q931_num_plan_t {
	uint8_t plan;
	char *descr;
};

struct ipdc_q931_num_plan_t ipdc_q931_num_plans[] = {
	{ IPDC_Q931_NUMBER_PLAN_UNKNOWN, "Unknown" },
	{ IPDC_Q931_NUMBER_PLAN_ISDN, "ISDN" },
	{ IPDC_Q931_NUMBER_PLAN_DATA, "Data" },
	{ IPDC_Q931_NUMBER_PLAN_TELEX, "Telex" },
	{ IPDC_Q931_NUMBER_PLAN_NATIONAL, "National" },
	{ IPDC_Q931_NUMBER_PLAN_PRIVATE, "Private" },
	{ IPDC_Q931_NUMBER_PLAN_EXTEN, "Extension" },
	{ IPDC_Q931_NUMBER_PLAN_RESERVED, "Reserved" }
};
#define IPDC_Q931_NUM_PLANS_LEN	sizeof(ipdc_q931_num_plans) / sizeof(struct ipdc_q931_num_plan_t)

#define IPDC_Q931_INFO_CHAN_MASK	0x03
enum {
	IPDC_Q931_INFO_CHAN_NO_CHANNEL = 0x00,
	IPDC_Q931_INFO_CHAN_B1_CHANNEL = 0x01,
	IPDC_Q931_INFO_CHAN_B2_CHANNEL = 0x02,
	IPDC_Q931_INFO_CHAN_ANY_CHANNEL = 0x03,
	IPDC_Q931_INFO_CHAN_UNKNOWN
};

struct ipdc_q931_info_channel_t {
	uint8_t sel;
	char *descr;
};

struct ipdc_q931_info_channel_t ipdc_q931_info_channels[] = {
	{ IPDC_Q931_INFO_CHAN_NO_CHANNEL, "No channel" },
	{ IPDC_Q931_INFO_CHAN_B1_CHANNEL, "B1 channel" },
	{ IPDC_Q931_INFO_CHAN_B2_CHANNEL, "B2 channel [reserved]" },
	{ IPDC_Q931_INFO_CHAN_ANY_CHANNEL, "Any channel" },
	{ IPDC_Q931_INFO_CHAN_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_INFO_CHANNELS_LEN	sizeof(ipdc_q931_info_channels) / sizeof(struct ipdc_q931_info_channel_t)

#define IPDC_Q931_DCHAN_MASK	0x04
enum {
	IPDC_Q931_DCHAN_FALSE = 0x00,
	IPDC_Q931_DCHAN_TRUE = 0x04,
	IPDC_Q931_DCHAN_UNKNOWN
};

struct ipdc_q931_dchan_t {
	uint8_t id;
	char *descr;
};

struct ipdc_q931_dchan_t ipdc_q931_dchan[] = {
	{ IPDC_Q931_DCHAN_FALSE, "Is not d-channel" },
	{ IPDC_Q931_DCHAN_TRUE, "Is d-channel" },
	{ IPDC_Q931_DCHAN_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_DCHAN_LEN	sizeof(ipdc_q931_dchan) / sizeof(struct ipdc_q931_dchan_t)

#define IPDC_Q931_PREFEXCL_MASK	0x08
enum {
	IPDC_Q931_PREFEXCL_PREFERRED = 0x00,
	IPDC_Q931_PREFEXCL_EXCLUSIVE = 0x08,
	IPDC_Q931_PREFEXCL_UNKNOWN
};

struct ipdc_q931_prefexcl_t {
	uint8_t val;
	char *descr;
};

struct ipdc_q931_prefexcl_t ipdc_q931_prefexcl[] = {
	{ IPDC_Q931_PREFEXCL_PREFERRED, "Preferred" },
	{ IPDC_Q931_PREFEXCL_EXCLUSIVE, "Exclusive" },
	{ IPDC_Q931_PREFEXCL_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_PREFEXCL_LEN	sizeof(ipdc_q931_prefexcl) / sizeof(struct ipdc_q931_prefexcl_t)

#define IPDC_Q931_INT_TYPE_MASK	0x20
enum {
	IPDC_Q931_INT_TYPE_BASIC = 0x00,
	IPDC_Q931_INT_TYPE_OTHER = 0x20,
	IPDC_Q931_INT_TYPE_UNKNOWN
};

struct ipdc_q931_int_type_t {
	uint8_t type;
	char *descr;
};

struct ipdc_q931_int_type_t ipdc_q931_int_types[] = {
	{ IPDC_Q931_INT_TYPE_BASIC, "Basic" },
	{ IPDC_Q931_INT_TYPE_OTHER, "Other" },
	{ IPDC_Q931_INT_TYPE_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_INT_TYPES_LEN	sizeof(ipdc_q931_int_types) / sizeof(struct ipdc_q931_int_type_t)

#define IPDC_Q931_INT_ID_PRESENT_MASK	0x40
enum {
	IPDC_Q931_INT_ID_PRESENT_IMPLICIT = 0x00,
	IPDC_Q931_INT_ID_PRESENT_EXPLICIT = 0x40,
	IPDC_Q931_INT_ID_PRESENT_UNKNOWN
};

struct ipdc_q931_int_id_present_t {
	uint8_t type;
	char *descr;
};

struct ipdc_q931_int_id_present_t ipdc_q931_int_id_present[] = {
	{ IPDC_Q931_INT_ID_PRESENT_IMPLICIT, "Implicit" },
	{ IPDC_Q931_INT_ID_PRESENT_EXPLICIT, "Explicit" },
	{ IPDC_Q931_INT_ID_PRESENT_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_INT_ID_PRESENT_LEN	sizeof(ipdc_q931_int_id_present) / sizeof(struct ipdc_q931_int_id_present_t)

#define IPDC_Q931_CHANMAP_TYPE_MASK	0x0F
enum {
	IPDC_Q931_CHANMAP_TYPE_B = 0x03,
	IPDC_Q931_CHANMAP_TYPE_H0 = 0x06,
	IPDC_Q931_CHANMAP_TYPE_H11 = 0x08,
	IPDC_Q931_CHANMAP_TYPE_H12 = 0x09,
	IPDC_Q931_CHANMAP_TYPE_UNKNOWN
};

struct ipdc_q931_chanmap_type_t {
	uint8_t type;
	char *descr;
};

struct ipdc_q931_chanmap_type_t ipdc_q931_chanmap_types[] = {
	{ IPDC_Q931_CHANMAP_TYPE_B, "B-channel units" },
	{ IPDC_Q931_CHANMAP_TYPE_H0, "H0-channel units" },
	{ IPDC_Q931_CHANMAP_TYPE_H11, "H11-channel units" },
	{ IPDC_Q931_CHANMAP_TYPE_H12, "H12-channel units" },
	{ IPDC_Q931_CHANMAP_TYPE_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_CHANMAP_TYPES_LEN	sizeof(ipdc_q931_chanmap_types) / sizeof(struct ipdc_q931_chanmap_type_t)

#define IPDC_Q931_CHANMAP_NUM_MASK	0x10
enum {
	IPDC_Q931_CHANMAP_NUM_CHANNEL = 0x00,
	IPDC_Q931_CHANMAP_NUM_SLOTMAP = 0x10,
	IPDC_Q931_CHANMAP_NUM_UNKNOWN
};

struct ipdc_q931_chanmap_num_t {
	uint8_t type;
	char *descr;
};

struct ipdc_q931_chanmap_num_t ipdc_q931_chanmap_num[] = {
	{ IPDC_Q931_CHANMAP_NUM_CHANNEL, "Channel" },
	{ IPDC_Q931_CHANMAP_NUM_SLOTMAP, "Slot map" },
	{ IPDC_Q931_CHANMAP_NUM_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_CHANMAP_NUM_LEN	sizeof(ipdc_q931_chanmap_num) / sizeof(struct ipdc_q931_chanmap_num_t)

#define IPDC_Q931_CODING_STD_MASK	0x60
enum {
	IPDC_Q931_CODING_STD_ITU_T = 0x00,
	IPDC_Q931_CODING_STD_ISO = 0x20,
	IPDC_Q931_CODING_STD_NATL = 0x40,
	IPDC_Q931_CODING_STD_STD = 0x60,
	IPDC_Q931_CODING_STD_UNKNOWN
};

struct ipdc_q931_coding_std_t {
	uint8_t type;
	char *descr;
};

struct ipdc_q931_coding_std_t ipdc_q931_coding_std[] = {
	{ IPDC_Q931_CODING_STD_ITU_T, "ITU-T standardized coding" },
	{ IPDC_Q931_CODING_STD_ISO, "ISO/IEC standard" },
	{ IPDC_Q931_CODING_STD_NATL, "National standard" },
	{ IPDC_Q931_CODING_STD_STD, "Network-defined standard" },
	{ IPDC_Q931_CODING_STD_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_CODING_STD_LEN	sizeof(ipdc_q931_coding_std) / sizeof(struct ipdc_q931_coding_std_t)

#define IPDC_Q931_RESTART_IND_MASK	0x07
enum {
	IPDC_Q931_RESTART_IND_INDICATED = 0x00,
	IPDC_Q931_RESTART_IND_SINGLE = 0x06,
	IPDC_Q931_RESTART_IND_ALL = 0x07,
	IPDC_Q931_RESTART_IND_UNKNOWN
};

struct ipdc_q931_restart_ind_t {
	uint8_t ind;
	char *descr;
};

struct ipdc_q931_restart_ind_t ipdc_q931_restart_ind[] = {
	{ IPDC_Q931_RESTART_IND_INDICATED, "Indicated channels" },
	{ IPDC_Q931_RESTART_IND_SINGLE, "Single interface" },
	{ IPDC_Q931_RESTART_IND_ALL, "All interfaces" },
	{ IPDC_Q931_RESTART_IND_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_RESTART_IND_LEN	sizeof(ipdc_q931_restart_ind) / sizeof(struct ipdc_q931_restart_ind_t)

#define IPDC_Q931_CALLREF_MASK	0x80
enum {
	IPDC_Q931_CALLREF_ORIG = 0x00,
	IPDC_Q931_CALLREF_TERM = 0x80,
	IPDC_Q931_CALLREF_UNKNOWN
};

#define IPDC_Q931_CALL_STATE_MASK	0x20
enum {
	IPDC_Q931_CALL_STATE_U0 = 0x00,
	IPDC_Q931_CALL_STATE_U1 = 0x01,
	IPDC_Q931_CALL_STATE_U2 = 0x02,
	IPDC_Q931_CALL_STATE_U3 = 0x03,
	IPDC_Q931_CALL_STATE_U4 = 0x04,
	IPDC_Q931_CALL_STATE_U6 = 0x06,
	IPDC_Q931_CALL_STATE_U7 = 0x07,
	IPDC_Q931_CALL_STATE_U8 = 0x08,
	IPDC_Q931_CALL_STATE_U9 = 0x09,
	IPDC_Q931_CALL_STATE_U10 = 0x0A,
	IPDC_Q931_CALL_STATE_U11 = 0x0B,
	IPDC_Q931_CALL_STATE_U12 = 0x0C,
	IPDC_Q931_CALL_STATE_U15 = 0x0F,
	IPDC_Q931_CALL_STATE_U17 = 0x11,
	IPDC_Q931_CALL_STATE_U19 = 0x13,
	IPDC_Q931_CALL_STATE_U25 = 0x19,
	IPDC_Q931_CALL_STATE_N0 = 0x00,
	IPDC_Q931_CALL_STATE_N1 = 0x01,
	IPDC_Q931_CALL_STATE_N2 = 0x02,
	IPDC_Q931_CALL_STATE_N3 = 0x03,
	IPDC_Q931_CALL_STATE_N4 = 0x04,
	IPDC_Q931_CALL_STATE_N6 = 0x06,
	IPDC_Q931_CALL_STATE_N7 = 0x07,
	IPDC_Q931_CALL_STATE_N8 = 0x08,
	IPDC_Q931_CALL_STATE_N9 = 0x09,
	IPDC_Q931_CALL_STATE_N10 = 0x0A,
	IPDC_Q931_CALL_STATE_N11 = 0x0B,
	IPDC_Q931_CALL_STATE_N12 = 0x0C,
	IPDC_Q931_CALL_STATE_N15 = 0x0F,
	IPDC_Q931_CALL_STATE_N17 = 0x11,
	IPDC_Q931_CALL_STATE_N19 = 0x13,
	IPDC_Q931_CALL_STATE_N22 = 0x16,
	IPDC_Q931_CALL_STATE_N25 = 0x19,
	IPDC_Q931_CALL_STATE_R0 = 0x00,
	IPDC_Q931_CALL_STATE_R1 = 0x3D,
	IPDC_Q931_CALL_STATE_R2 = 0x3E,
	IPDC_Q931_CALL_STATE_UNKNOWN
};

struct ipdc_q931_call_state_t {
	uint8_t state;
	char *descr;
};

struct ipdc_q931_call_state_t ipdc_q931_call_state[] = {
	{ IPDC_Q931_CALL_STATE_U0, "null state (user)" },
	{ IPDC_Q931_CALL_STATE_U1, "call initiated (user)" },
	{ IPDC_Q931_CALL_STATE_U2, "overlap sending (user)" },
	{ IPDC_Q931_CALL_STATE_U3, "outgoing call proceeding (user)" },
	{ IPDC_Q931_CALL_STATE_U4, "call delivered (user)" },
	{ IPDC_Q931_CALL_STATE_U6, "call present (user)" },
	{ IPDC_Q931_CALL_STATE_U7, "call received (user)" },
	{ IPDC_Q931_CALL_STATE_U8, "connect request (user)" },
	{ IPDC_Q931_CALL_STATE_U9, "incoming call proceeeding (user)" },
	{ IPDC_Q931_CALL_STATE_U10, "active (user)" },
	{ IPDC_Q931_CALL_STATE_U11, "disconnect request (user)" },
	{ IPDC_Q931_CALL_STATE_U12, "disconnect indication (user)" },
	{ IPDC_Q931_CALL_STATE_U15, "suspend request (user)" },
	{ IPDC_Q931_CALL_STATE_U17, "resume request (user)" },
	{ IPDC_Q931_CALL_STATE_U19, "release request (user)" },
	{ IPDC_Q931_CALL_STATE_U25, "overlap receiving (user)" },
	{ IPDC_Q931_CALL_STATE_N0, "null state (network)" },
	{ IPDC_Q931_CALL_STATE_N1, "call initiated (network)" },
	{ IPDC_Q931_CALL_STATE_N2, "overlap sending (network)" },
	{ IPDC_Q931_CALL_STATE_N3, "outgoing call proceeding (network)" },
	{ IPDC_Q931_CALL_STATE_N4, "call delivered (network)" },
	{ IPDC_Q931_CALL_STATE_N6, "call present (network)" },
	{ IPDC_Q931_CALL_STATE_N7, "call received (network)" },
	{ IPDC_Q931_CALL_STATE_N8, "connect request (network)" },
	{ IPDC_Q931_CALL_STATE_N9, "incoming call proceeding (network)" },
	{ IPDC_Q931_CALL_STATE_N10, "active (network)" },
	{ IPDC_Q931_CALL_STATE_N11, "disconnect request (network)" },
	{ IPDC_Q931_CALL_STATE_N12, "disconnect indication (network)" },
	{ IPDC_Q931_CALL_STATE_N15, "suspend request (network)" },
	{ IPDC_Q931_CALL_STATE_N17, "resume request (network)" },
	{ IPDC_Q931_CALL_STATE_N19, "release request (network)" },
	{ IPDC_Q931_CALL_STATE_N22, "call abort (network)" },
	{ IPDC_Q931_CALL_STATE_N25, "overlap receiving (network)" },
	{ IPDC_Q931_CALL_STATE_R0, "null state (restart)" },
	{ IPDC_Q931_CALL_STATE_R1, "restart request" },
	{ IPDC_Q931_CALL_STATE_R2, "restart" },
	{ IPDC_Q931_CALL_STATE_UNKNOWN, "unknown" }
};
#define IPDC_Q931_CALL_STATE_LEN	sizeof(ipdc_q931_call_state) / sizeof(struct ipdc_q931_call_state_t)

#define IPDC_Q931_INFO_TRANS_CAP_MASK	0x1F
enum {
	IPDC_Q931_INFO_TRANS_CAP_SPEECH = 0x00,
	IPDC_Q931_INFO_TRANS_CAP_UNRESTRICTED = 0x08,
	IPDC_Q931_INFO_TRANS_CAP_RESTRICTED = 0x09,
	IPDC_Q931_INFO_TRANS_CAP_31KHZ_AUDIO = 0x10,
	IPDC_Q931_INFO_TRANS_CAP_UNRESTRICTED_TONES = 0x11,
	IPDC_Q931_INFO_TRANS_CAP_VIDEO = 0x18,
	IPDC_Q931_INFO_TRANS_CAP_UNKNOWN
};

struct ipdc_q931_info_trans_cap_t {
	uint8_t type;
	char *descr;
};

struct ipdc_q931_info_trans_cap_t ipdc_q931_info_trans_cap[] = {
	{ IPDC_Q931_INFO_TRANS_CAP_SPEECH, "Speech" },
	{ IPDC_Q931_INFO_TRANS_CAP_UNRESTRICTED, "Unrestricted digital information" },
	{ IPDC_Q931_INFO_TRANS_CAP_RESTRICTED, "Restricted digital information" },
	{ IPDC_Q931_INFO_TRANS_CAP_31KHZ_AUDIO, "3.1kHz audio" },
	{ IPDC_Q931_INFO_TRANS_CAP_UNRESTRICTED_TONES, "Unrestricted digital information with tones/announcements" },
	{ IPDC_Q931_INFO_TRANS_CAP_VIDEO, "Video" },
	{ IPDC_Q931_INFO_TRANS_CAP_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_INFO_TRANS_CAP_LEN	sizeof(ipdc_q931_info_trans_cap) / sizeof(struct ipdc_q931_info_trans_cap_t)

#define IPDC_Q931_INFO_TRANS_RATE_MASK 0x1F
enum {
	IPDC_Q931_INFO_TRANS_RATE_PACKET = 0x00,
	IPDC_Q931_INFO_TRANS_RATE_64K = 0x10,
	IPDC_Q931_INFO_TRANS_RATE_2X64K = 0x11,
	IPDC_Q931_INFO_TRANS_RATE_384K = 0x13,
	IPDC_Q931_INFO_TRANS_RATE_1536K = 0x15,
	IPDC_Q931_INFO_TRANS_RATE_1920K = 0x17,
	IPDC_Q931_INFO_TRANS_RATE_MULTIRATE = 0x18,
	IPDC_Q931_INFO_TRANS_RATE_UNKNOWN
};

struct ipdc_q931_info_trans_rate_t {
	uint8_t type;
	char *descr;
};

struct ipdc_q931_info_trans_rate_t ipdc_q931_info_trans_rate[] = {
	{ IPDC_Q931_INFO_TRANS_RATE_PACKET, "Packet mode" },
	{ IPDC_Q931_INFO_TRANS_RATE_64K, "64 kbit/s" },
	{ IPDC_Q931_INFO_TRANS_RATE_2X64K, "2x64 kbit/s" },
	{ IPDC_Q931_INFO_TRANS_RATE_384K, "384 kbit/s" },
	{ IPDC_Q931_INFO_TRANS_RATE_1536K, "1536 kbit/s" },
	{ IPDC_Q931_INFO_TRANS_RATE_1920K, "1920 kbit/s" },
	{ IPDC_Q931_INFO_TRANS_RATE_MULTIRATE, "Multirate (64 kbit/s base rate)" },
	{ IPDC_Q931_INFO_TRANS_RATE_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_INFO_TRANS_RATE_LEN	sizeof(ipdc_q931_info_trans_rate) / sizeof(struct ipdc_q931_info_trans_rate_t)

#define IPDC_Q931_TRANS_MODE_MASK	0x60
enum {
	IPDC_Q931_TRANS_MODE_CIRCUIT = 0x00,
	IPDC_Q931_TRANS_MODE_PACKET = 0x40,
	IPDC_Q931_TRANS_MODE_UNKNOWN
};

struct ipdc_q931_trans_mode_t {
	uint8_t type;
	char *descr;
};

struct ipdc_q931_trans_mode_t ipdc_q931_trans_mode[] = {
	{ IPDC_Q931_TRANS_MODE_CIRCUIT, "Circuit" },
	{ IPDC_Q931_TRANS_MODE_PACKET, "Packet" },
	{ IPDC_Q931_TRANS_MODE_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_TRANS_MODE_LEN	sizeof(ipdc_q931_trans_mode) / sizeof(struct ipdc_q931_trans_mode_t)

#define IPDC_Q931_RATE_MULTIPLIER_MASK	0x7F

#define IPDC_Q931_USERINFO_L1P_MASK	0x1F
enum {
	IPDC_Q931_USERINFO_L1P_V110 = 0x01,
	IPDC_Q931_USERINFO_L1P_G711_ULAW = 0x02,
	IPDC_Q931_USERINFO_L1P_G711_ALAW = 0x03,
	IPDC_Q931_USERINFO_L1P_G721 = 0x04,
	IPDC_Q931_USERINFO_L1P_H221 = 0x05,
	IPDC_Q931_USERINFO_L1P_H223 = 0x06,
	IPDC_Q931_USERINFO_L1P_NONSTANDARD = 0x07,
	IPDC_Q931_USERINFO_L1P_V120 = 0x08,
	IPDC_Q931_USERINFO_L1P_X31 = 0x09,
	IPDC_Q931_USERINFO_L1P_UNKNOWN
};

struct ipdc_q931_userinfo_l1p_t {
	uint8_t type;
	char *descr;
};

struct ipdc_q931_userinfo_l1p_t ipdc_q931_userinfo_l1p[] = {
	{ IPDC_Q931_USERINFO_L1P_V110, "V.110/I.460/X.30" },
	{ IPDC_Q931_USERINFO_L1P_G711_ULAW, "G.711 u-law" },
	{ IPDC_Q931_USERINFO_L1P_G711_ALAW, "G.711 a-law" },
	{ IPDC_Q931_USERINFO_L1P_G721, "G.721 ADPCM/I.460" },
	{ IPDC_Q931_USERINFO_L1P_H221, "H.221/H.242" },
	{ IPDC_Q931_USERINFO_L1P_H223, "H.223/H.245" },
	{ IPDC_Q931_USERINFO_L1P_NONSTANDARD, "Non-standard" },
	{ IPDC_Q931_USERINFO_L1P_V120, "V.120" },
	{ IPDC_Q931_USERINFO_L1P_X31, "X.31" },
	{ IPDC_Q931_USERINFO_L1P_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_USERINFO_L1P_LEN	sizeof(ipdc_q931_userinfo_l1p) / sizeof(struct ipdc_q931_userinfo_l1p_t)

#define IPDC_Q931_NEGOTIATION_MASK	0x20
enum {
	IPDC_Q931_NEGOTIATION_INBAND_DENIED = 0x00,
	IPDC_Q931_NEGOTIATION_INBAND_ALLOWED = 0x20,
	IPDC_Q931_NEGOTIATION_UNKNOWN
};

struct ipdc_q931_negotiation_t {
	uint8_t val;
	char *descr;
};

struct ipdc_q931_negotiation_t ipdc_q931_negotiation[] = {
	{ IPDC_Q931_NEGOTIATION_INBAND_DENIED, "In-band negotiation not possible" },
	{ IPDC_Q931_NEGOTIATION_INBAND_ALLOWED, "In-band negotiation possible" },
	{ IPDC_Q931_NEGOTIATION_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_NEGOTIATION_LEN	sizeof(ipdc_q931_negotiation) / sizeof(struct ipdc_q931_negotiation_t)

#define IPDC_Q931_SYNC_ASYNC_MASK	0x40
enum {
	IPDC_Q931_SYNC_ASYNC_SYNC = 0x00,
	IPDC_Q931_SYNC_ASYNC_ASYNC = 0x40,
	IPDC_Q931_SYNC_ASYNC_UNKNOWN
};

struct ipdc_q931_sync_async_t {
	uint8_t val;
	char *descr;
};

struct ipdc_q931_sync_async_t ipdc_q931_sync_async[] = {
	{ IPDC_Q931_SYNC_ASYNC_SYNC, "Synchronous" },
	{ IPDC_Q931_SYNC_ASYNC_ASYNC, "Asynchronous" },
	{ IPDC_Q931_SYNC_ASYNC_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_SYNC_ASYNC_LEN	sizeof(ipdc_q931_sync_async) / sizeof(struct ipdc_q931_sync_async_t)

struct call_ref_t {
	struct ipdc_system_info_t *si;
	struct mod_chans_t *sig;
	struct mod_chans_t *call;
	uint16_t callref;
	uint8_t state;
	char *called;
	char *calling;
	struct call_ref_t *next;
};

enum {
	CALL_REF_SIGNALING_CHAN,
	CALL_REF_CALL_CHAN,
	CALL_REF_UNKNOWN_CHAN
};

#define IPDC_Q931_GLOBAL_CALLREF		0x0000

#define IPDC_Q931_CAUSE_CLASS_MASK	0x70
enum {
	IPDC_Q931_CAUSE_CLASS_NORMAL = 0x00,
	IPDC_Q931_CAUSE_CLASS_NORMAL2 = 0x10,
	IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL = 0x20,
	IPDC_Q931_CAUSE_CLASS_OPT_UNAVAIL = 0x30,
	IPDC_Q931_CAUSE_CLASS_OPT_UNIMPL = 0x40,
	IPDC_Q931_CAUSE_CLASS_INVALID_MSG = 0x50,
	IPDC_Q931_CAUSE_CLASS_PROTO_ERR = 0x60,
	IPDC_Q931_CAUSE_CLASS_INTERWORKING = 0x70,
	IPDC_Q931_CAUSE_CLASS_UNKNOWN
};

struct ipdc_q931_cause_class_t {
	uint8_t class;
	char *descr;
};

struct ipdc_q931_cause_class_t ipdc_q931_cause_class[] = {
	{ IPDC_Q931_CAUSE_CLASS_NORMAL, "normal event" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, "normal event" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, "resource unavailable" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNAVAIL, "service/option not available" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNIMPL, "service/option not implemented" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, "invalid message" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, "protocol error" },
	{ IPDC_Q931_CAUSE_CLASS_INTERWORKING, "interworking" },
	{ IPDC_Q931_CAUSE_CLASS_UNKNOWN, "unknown" }
};
#define IPDC_Q931_CAUSE_CLASS_LEN	sizeof(ipdc_q931_cause_class) / sizeof(struct ipdc_q931_cause_class_t)

#define IPDC_Q931_CAUSE_CODE_MASK	0x7f
enum {
	IPDC_Q931_CAUSE_CODE_UNALLOC_NUM = 0x01,
	IPDC_Q931_CAUSE_CODE_NO_ROUTE_TO_TRANSIT = 0x02,
	IPDC_Q931_CAUSE_CODE_NO_ROUTE_TO_DEST = 0x03,
	IPDC_Q931_CAUSE_CODE_SEND_SIT = 0x04,
	IPDC_Q931_CAUSE_CODE_INVALID_TRUNK_PFX = 0x05,
	IPDC_Q931_CAUSE_CODE_CHAN_INVALID = 0x06,
	IPDC_Q931_CAUSE_CODE_CALL_AWARDED = 0x07,
	IPDC_Q931_CAUSE_CODE_PREEMPTION = 0x08,
	IPDC_Q931_CAUSE_CODE_PREEMPTION_RESERVED = 0x09,
	IPDC_Q931_CAUSE_CODE_NORMAL_CLEARING = 0x10,
	IPDC_Q931_CAUSE_CODE_USER_BUSY = 0x11,
	IPDC_Q931_CAUSE_CODE_NOT_RESPONDING = 0x12,
	IPDC_Q931_CAUSE_CODE_NO_ANSWER = 0x13,
	IPDC_Q931_CAUSE_CODE_SUBSCRIBER_ABSENT = 0x14,
	IPDC_Q931_CAUSE_CODE_CALL_REJECTED = 0x15,
	IPDC_Q931_CAUSE_CODE_NUM_CHANGED = 0x16,
	IPDC_Q931_CAUSE_CODE_REDIRECTION = 0x17,
	IPDC_Q931_CAUSE_CODE_EXCH_ROUTING_ERR = 0x19,
	IPDC_Q931_CAUSE_CODE_NONSELECTED_CLEARING = 0x1A,
	IPDC_Q931_CAUSE_CODE_DEST_OUT_OF_ORDER = 0x1B,
	IPDC_Q931_CAUSE_CODE_INVALID_NUM_FMT = 0x1C,
	IPDC_Q931_CAUSE_CODE_FAC_REJECTED = 0x1D,
	IPDC_Q931_CAUSE_CODE_STATUS_ENQ_RESP = 0x1E,
	IPDC_Q931_CAUSE_CODE_NORMAL_UNSPECIFIED = 0x1F,
	IPDC_Q931_CAUSE_CODE_NO_CHAN_AVAIL = 0x22,
	IPDC_Q931_CAUSE_CODE_NET_OUT_OF_ORDER = 0x26,
	IPDC_Q931_CAUSE_CODE_CONN_OOS = 0x27,
	IPDC_Q931_CAUSE_CODE_CONN_OPER = 0x28,
	IPDC_Q931_CAUSE_CODE_TEMP_FAILURE = 0x29,
	IPDC_Q931_CAUSE_CODE_CONGESTION = 0x2A,
	IPDC_Q931_CAUSE_CODE_ACCESS_INFO_DISC = 0x2B,
	IPDC_Q931_CAUSE_CODE_CHAN_UNAVAIL = 0x2C,
	IPDC_Q931_CAUSE_CODE_PREC_CALL_BLOCKED = 0x2E,
	IPDC_Q931_CAUSE_CODE_RES_UNAVAIL = 0x2F,
	IPDC_Q931_CAUSE_CODE_QOS_UNAVAIL = 0x31,
	IPDC_Q931_CAUSE_CODE_FAC_UNAVAIL = 0x32,
	IPDC_Q931_CAUSE_CODE_OUTGOING_CUG_BARRED = 0x35,
	IPDC_Q931_CAUSE_CODE_INCOMING_CUG_BARRED = 0x37,
	IPDC_Q931_CAUSE_CODE_CAP_UNAUTH = 0x39,
	IPDC_Q931_CAUSE_CODE_CAP_UNAVAIL = 0x3A,
	IPDC_Q931_CAUSE_CODE_INCONSISTENCY = 0x3E,
	IPDC_Q931_CAUSE_CODE_OPT_UNAVAIL = 0x3F,
	IPDC_Q931_CAUSE_CODE_CAP_UNIMPL = 0x41,
	IPDC_Q931_CAUSE_CODE_CHANTYPE_UNIMPL = 0x42,
	IPDC_Q931_CAUSE_CODE_FAC_UNIMPL = 0x45,
	IPDC_Q931_CAUSE_CODE_RESTRICTED_DIGITAL = 0x46,
	IPDC_Q931_CAUSE_CODE_OPT_UNIMPL = 0x4F,
	IPDC_Q931_CAUSE_CODE_REF_INVALID = 0x51,
	IPDC_Q931_CAUSE_CODE_CHAN_NOTEXIST = 0x52,
	IPDC_Q931_CAUSE_CODE_CALL_SUSPENDED = 0x53,
	IPDC_Q931_CAUSE_CODE_ID_INUSE = 0x54,
	IPDC_Q931_CAUSE_CODE_NO_CALL_SUSPENDED = 0x55,
	IPDC_Q931_CAUSE_CODE_CALL_CLEARED = 0x56,
	IPDC_Q931_CAUSE_CODE_NOT_CUG_MEMBER = 0x57,
	IPDC_Q931_CAUSE_CODE_DEST_INCOMPAT = 0x58,
	IPDC_Q931_CAUSE_CODE_CUG_NOTEXIST = 0x5A,
	IPDC_Q931_CAUSE_CODE_TRANSIT_INVALID = 0x5B,
	IPDC_Q931_CAUSE_CODE_MSG_INVALID = 0x5F,
	IPDC_Q931_CAUSE_CODE_IE_MISSING = 0x60,
	IPDC_Q931_CAUSE_CODE_MSGTYPE_UNIMPL = 0x61,
	IPDC_Q931_CAUSE_CODE_MSG_INCOMPAT = 0x62,
	IPDC_Q931_CAUSE_CODE_IE_UNIMPL = 0x63,
	IPDC_Q931_CAUSE_CODE_IE_INVALID = 0x64,
	IPDC_Q931_CAUSE_CODE_MSG_UNIMPL = 0x65,
	IPDC_Q931_CAUSE_CODE_TIMER_EXPIRY = 0x66,
	IPDC_Q931_CAUSE_CODE_PARAM_UNRECOG_PASSED = 0x66,
	IPDC_Q931_CAUSE_CODE_PARAM_UNRECOG_DROPPED = 0x6E,
	IPDC_Q931_CAUSE_CODE_PROTO_ERROR = 0x6F,
	IPDC_Q931_CAUSE_CODE_INTERWORKING = 0x7F,
	IPDC_Q931_CAUSE_CODE_UNKNOWN
};

struct ipdc_q931_cause_code_t {
	uint8_t class;
	uint8_t code;
	char *descr;
};

struct ipdc_q931_cause_code_t ipdc_q931_cause_code[] = {
	{ IPDC_Q931_CAUSE_CLASS_NORMAL, IPDC_Q931_CAUSE_CODE_UNALLOC_NUM , "Unallocated number" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL, IPDC_Q931_CAUSE_CODE_NO_ROUTE_TO_TRANSIT , "No route to specified transit network" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL, IPDC_Q931_CAUSE_CODE_NO_ROUTE_TO_DEST , "No route to destination" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL, IPDC_Q931_CAUSE_CODE_SEND_SIT , "Send special information tone" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL, IPDC_Q931_CAUSE_CODE_INVALID_TRUNK_PFX , "Misdialed trunk prefix" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL, IPDC_Q931_CAUSE_CODE_CHAN_INVALID , "Channel unacceptable" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL, IPDC_Q931_CAUSE_CODE_CALL_AWARDED , "Call awarded and being delivered in an established channel" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL, IPDC_Q931_CAUSE_CODE_PREEMPTION , "Preemption" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL, IPDC_Q931_CAUSE_CODE_PREEMPTION_RESERVED , "Preemption - circuit reserved for reuse" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_NORMAL_CLEARING , "Normal call clearing" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_USER_BUSY , "User busy" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_NOT_RESPONDING , "No user responding" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_NO_ANSWER , "No answer from user (user alerted)" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_SUBSCRIBER_ABSENT , "Subscriber absent" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_CALL_REJECTED , "Call rejected" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_NUM_CHANGED , "Number changed" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_REDIRECTION , "Redirection to new destination" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_EXCH_ROUTING_ERR , "Exchange routing error" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_NONSELECTED_CLEARING , "Non-selected user clearing" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_DEST_OUT_OF_ORDER , "Destination out of order" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_INVALID_NUM_FMT , "Address incomplete" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_FAC_REJECTED , "Facility rejected" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_STATUS_ENQ_RESP , "Response to STATUS ENQUIRY" },
	{ IPDC_Q931_CAUSE_CLASS_NORMAL2, IPDC_Q931_CAUSE_CODE_NORMAL_UNSPECIFIED , "Normal, unspecified" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, IPDC_Q931_CAUSE_CODE_NO_CHAN_AVAIL , "No circuit/channel available" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, IPDC_Q931_CAUSE_CODE_NET_OUT_OF_ORDER , "Network out of order" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, IPDC_Q931_CAUSE_CODE_CONN_OOS , "Permanent frame mode connection out of service" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, IPDC_Q931_CAUSE_CODE_CONN_OPER , "Permanent frame mode connection operational" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, IPDC_Q931_CAUSE_CODE_TEMP_FAILURE , "Temporary failure" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, IPDC_Q931_CAUSE_CODE_CONGESTION , "Switching equipment congestion" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, IPDC_Q931_CAUSE_CODE_ACCESS_INFO_DISC , "Access information discarded" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, IPDC_Q931_CAUSE_CODE_CHAN_UNAVAIL , "Requested circuit/channel unavailable" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, IPDC_Q931_CAUSE_CODE_PREC_CALL_BLOCKED , "Precedence call blocked" },
	{ IPDC_Q931_CAUSE_CLASS_RES_UNAVAIL, IPDC_Q931_CAUSE_CODE_RES_UNAVAIL , "Resource unavailable, unspecified" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNAVAIL, IPDC_Q931_CAUSE_CODE_QOS_UNAVAIL , "Quality of service not available" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNAVAIL, IPDC_Q931_CAUSE_CODE_FAC_UNAVAIL , "Requested facility not subsccribed" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNAVAIL, IPDC_Q931_CAUSE_CODE_OUTGOING_CUG_BARRED , "Outgoing calls barred within CUG" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNAVAIL, IPDC_Q931_CAUSE_CODE_INCOMING_CUG_BARRED , "Incoming calls barred within CUG" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNAVAIL, IPDC_Q931_CAUSE_CODE_CAP_UNAUTH , "Bearer capability not authorized" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNAVAIL, IPDC_Q931_CAUSE_CODE_CAP_UNAVAIL , "Bearer capability not presently available" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNAVAIL, IPDC_Q931_CAUSE_CODE_INCONSISTENCY , "Inconsistency in designated outgoing access information and subscriber calls" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNAVAIL, IPDC_Q931_CAUSE_CODE_OPT_UNAVAIL , "Service or option unavailable, unspecified" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNIMPL, IPDC_Q931_CAUSE_CODE_CAP_UNIMPL , "Bearer capability not implemented" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNIMPL, IPDC_Q931_CAUSE_CODE_CHANTYPE_UNIMPL , "Channel type not implemented" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNIMPL, IPDC_Q931_CAUSE_CODE_FAC_UNIMPL , "Requested facility not implemented" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNIMPL, IPDC_Q931_CAUSE_CODE_RESTRICTED_DIGITAL , "Only restricted digital information bearer capability is available" },
	{ IPDC_Q931_CAUSE_CLASS_OPT_UNIMPL, IPDC_Q931_CAUSE_CODE_OPT_UNIMPL , "Service or option not implemented, unspecified" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_REF_INVALID , "Invalid call reference value" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_CHAN_NOTEXIST , "Identified channel does not exist" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_CALL_SUSPENDED , "A suspended call exists, but this call identity does not" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_ID_INUSE , "Call identity in use" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_NO_CALL_SUSPENDED , "No call suspended" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_CALL_CLEARED , "Call having the requested call identity has been cleared" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_NOT_CUG_MEMBER , "User not member of CUG" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_DEST_INCOMPAT , "Incompatible destination" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_CUG_NOTEXIST , "Non-existent CUG" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_TRANSIT_INVALID , "Invalid transit network selection" },
	{ IPDC_Q931_CAUSE_CLASS_INVALID_MSG, IPDC_Q931_CAUSE_CODE_MSG_INVALID , "Invalid message, unspecified" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, IPDC_Q931_CAUSE_CODE_IE_MISSING , "Mandatory information element is missing" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, IPDC_Q931_CAUSE_CODE_MSGTYPE_UNIMPL , "Message type non-existent or not implemented" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, IPDC_Q931_CAUSE_CODE_MSG_INCOMPAT , "Message not compatible with call state or message type non-existent or not implemented" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, IPDC_Q931_CAUSE_CODE_IE_UNIMPL , "Information element/parameter non-existent or not implemented" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, IPDC_Q931_CAUSE_CODE_IE_INVALID , "Invalid information element contents" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, IPDC_Q931_CAUSE_CODE_MSG_UNIMPL , "Message not compatible with call state" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, IPDC_Q931_CAUSE_CODE_TIMER_EXPIRY , "Recovery on timer expiry" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, IPDC_Q931_CAUSE_CODE_PARAM_UNRECOG_PASSED , "Paramater non-existent or not implemented, passed on" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, IPDC_Q931_CAUSE_CODE_PARAM_UNRECOG_DROPPED , "Message with unrecognized parameter, discarded" },
	{ IPDC_Q931_CAUSE_CLASS_PROTO_ERR, IPDC_Q931_CAUSE_CODE_PROTO_ERROR , "Protocol error, unspecified" },
	{ IPDC_Q931_CAUSE_CLASS_INTERWORKING, IPDC_Q931_CAUSE_CODE_INTERWORKING , "Interworking, unspecified" },
	{ IPDC_Q931_CAUSE_CLASS_UNKNOWN, IPDC_Q931_CAUSE_CODE_UNKNOWN, "Unknown" }
};
#define IPDC_Q931_CAUSE_CODE_LEN	sizeof(ipdc_q931_cause_code) / sizeof(struct ipdc_q931_cause_code_t)

#endif /* IPDC_Q931_H */
