#ifndef IPDC_MESSAGES_H
#define IPDC_MESSAGES_H

enum {
	IPDC_CONVERT_MSGCODE_TO_NAME,
	IPDC_CONVERT_MSGCODE_TO_DESCR
};

enum ipdc_codes {
	IPDC_MSG_UNKNOWN = 0x0000,

	/* Gateway setup/teardown */
	IPDC_MSG_NSUP = 0x0081,
	IPDC_MSG_ASUP = 0x0082,
	IPDC_MSG_NSDN = 0x0083,
	IPDC_MSG_LNK = 0x0084,
	IPDC_MSG_ALNK = 0x0085,
	IPDC_MSG_SLNK = 0x0086,
	IPDC_MSG_RCGST = 0x0087,
	IPDC_MSG_CGST = 0x0088,
	IPDC_MSG_RSI = 0x0091,
	IPDC_MSG_NSI = 0x0092,
	IPDC_MSG_MRJ = 0x00FF,

	/* Physical media status */
	IPDC_MSG_RMS = 0x0041,
	IPDC_MSG_NMS = 0x0042,
	IPDC_MSG_RLS = 0x0043,
	IPDC_MSG_NLS = 0x0044,
	IPDC_MSG_RCS = 0x0045,
	IPDC_MSG_NCS = 0x0046,
	IPDC_MSG_SMS = 0x0051,
	IPDC_MSG_SLS = 0x0053,
	IPDC_MSG_SCS = 0x0055,
	IPDC_MSG_RSCS = 0x0056,

	/* RTP port status */
	IPDC_MSG_RRS = 0x0047,
	IPDC_MSG_RARS = 0x0048,
	IPDC_MSG_NRS = 0x0049,
	IPDC_MSG_NARS = 0x004A,

	/* Continuity Testing */
	IPDC_MSG_PCT = 0x0061,
	IPDC_MSG_APCT = 0x0062,
	IPDC_MSG_SCT = 0x0063,
	IPDC_MSG_ASCT = 0x0064,
	IPDC_MSG_LTN = 0x0071,
	IPDC_MSG_ALTN = 0x0072,
	IPDC_MSG_STN = 0x0073,
	IPDC_MSG_ASTN = 0x0074,
	IPDC_MSG_RTE = 0x007D,
	IPDC_MSG_ARTE = 0x007E,
	IPDC_MSG_NTN = 0x00F0,

	/* Tunneling */
	IPDC_MSG_NATV = 0x0079,
	IPDC_MSG_TUNL = 0x007A,

	/* Call Control */
	IPDC_MSG_RCSI = 0x0001,
	IPDC_MSG_ACSI = 0x0002,
	IPDC_MSG_CONI = 0x0003,
	IPDC_MSG_RCSO = 0x0005,
	IPDC_MSG_ACSO = 0x0006,
	IPDC_MSG_CONO = 0x0007,
	IPDC_MSG_RCST = 0x0009,
	IPDC_MSG_ACST = 0x000A,
	IPDC_MSG_RCR = 0x0011,
	IPDC_MSG_ACR = 0x0012,
	IPDC_MSG_RCCP = 0x0013,
	IPDC_MSG_ACCP = 0x0014,
	IPDC_MSG_RMCP = 0x0015,
	IPDC_MSG_AMCP = 0x0016
};

struct ipdc_msgs_t {
	uint16_t code;
	char *name;
	char *descr;
	uint8_t ack_needed;
};

static struct ipdc_msgs_t ipdc_msgs[] = {
	{ IPDC_MSG_NSUP, "NSUP", "Notify the softswitch that the access server is coming up", 1 },
	{ IPDC_MSG_ASUP, "ASUP", "Acknowledgment to NSUP", 0 },
	{ IPDC_MSG_NSDN, "NSDN", "Indication that the access server has been asked to shut down", 0 },
	{ IPDC_MSG_LNK, "LNK", "Link active", 1 },
	{ IPDC_MSG_ALNK, "ALNK", "Acknowledgment to link active", 0 },
	{ IPDC_MSG_SLNK, "SLNK", "Link status", 0 },
	{ IPDC_MSG_RCGST, "RCGST", "Request congestion status", 1 },
	{ IPDC_MSG_CGST, "CGST", "Congestion status", 0 },
	{ IPDC_MSG_RSI, "RSI", "Request system information", 1 },
	{ IPDC_MSG_NSI, "NSI", "Notify system information", 0 },
	{ IPDC_MSG_MRJ, "MRJ", "Message reject", 0 },
	{ IPDC_MSG_RMS, "RMS", "Request module status", 1 },
	{ IPDC_MSG_NMS, "NMS", "Notify module status", 0 },
	{ IPDC_MSG_RLS, "RLS", "Request line status", 1 },
	{ IPDC_MSG_NLS, "NLS", "Notify line status", 0 },
	{ IPDC_MSG_RCS, "RCS", "Request channel status", 1 },
	{ IPDC_MSG_NCS, "NCS", "Notify channel status", 0 },
	{ IPDC_MSG_SMS, "SMS", "Set a module to a given state", 1 },
	{ IPDC_MSG_SLS, "SLS", "Set a line to given state", 1 },
	{ IPDC_MSG_SCS, "SCS", "Set a channel to a given state", 1 },
	{ IPDC_MSG_RSCS, "RSCS", "Response to set channel state", 0 },
	{ IPDC_MSG_RRS, "RRS", "Request RTP port status", 1 },
	{ IPDC_MSG_RARS, "RARS", "Request all RTP port status", 1 },
	{ IPDC_MSG_NRS, "NRS", "Notify RTP port status", 0 },
	{ IPDC_MSG_NARS, "NARS", "Notify all RTP port status", 0 },
	{ IPDC_MSG_PCT, "PCT", "Prepare channel for continuity test", 1 },
	{ IPDC_MSG_APCT, "APCT", "Response to prepare channel for continuity test", 0 },
	{ IPDC_MSG_SCT, "SCT", "Start continuity test procedure with far-end loopback", 1 },
	{ IPDC_MSG_ASCT, "ASCT", "Continuity test result", 0 },
	{ IPDC_MSG_LTN, "LTN", "Listen for tones", 1 },
	{ IPDC_MSG_ALTN, "ALTN", "Response to listen for tones", 0 },
	{ IPDC_MSG_STN, "STN", "Send tones", 1 },
	{ IPDC_MSG_ASTN, "ASTN", "Response to send tones", 0 },
	{ IPDC_MSG_RTE, "RTE", "Request test echo", 1 },
	{ IPDC_MSG_ARTE, "ARTE", "Acknowledgment of test echo", 0 },
	{ IPDC_MSG_NATV, "NATV", "Native mode Q.931 signaling transport", 0 },
	{ IPDC_MSG_TUNL, "TUNL", "Tunneled transport of signaling PDUs", 0 },
	{ IPDC_MSG_RCSI, "RCSI", "Request inbound call setup", 1 },
	{ IPDC_MSG_ACSI, "ACSI", "Accept inbound call setup", 0 },
	{ IPDC_MSG_CONI, "CONI", "Inbound call connected", 0 },
	{ IPDC_MSG_RCSO, "RCSO", "Request outbound call setup", 1 },
	{ IPDC_MSG_ACSO, "ACSO", "Accept outbound call setup", 0 },
	{ IPDC_MSG_CONO, "CONO", "Outbound call connected", 0 },
	{ IPDC_MSG_RCST, "RCST", "Request pass-through call setup", 1 },
	{ IPDC_MSG_ACST, "ACST", "Accept pass-through call setup", 0 },
	{ IPDC_MSG_RCCP, "RCCP", "Request packet call setup", 1 },
	{ IPDC_MSG_ACCP, "ACCP", "Accept packet call setup", 0 },
	{ IPDC_MSG_RMCP, "RMCP", "Modify/query packet call request", 1 },
	{ IPDC_MSG_AMCP, "AMCP", "Accept packet call request modification", 0 },
	{ IPDC_MSG_RCR, "RCR", "Release channel request", 1 },
	{ IPDC_MSG_ACR, "ACR", "Release channel complete", 0 },
	{ IPDC_MSG_UNKNOWN, "UNKN", "Unknown message code", 0 }
};
#define IPDC_MSGS_LEN		sizeof(ipdc_msgs) / sizeof(struct ipdc_msgs_t)

struct ipdc_system_info_t {
	pthread_t thread_id;
	uint32_t ip;
	int sock;
	uint8_t version;
	char id[25];
	char type[10];
	uint16_t modules;
	char bay[9];
	uint8_t country;
	uint32_t u_ports;
	uint32_t h_ports;
	uint16_t nms_rcvd;
	uint32_t last_tid_rcvd;
	uint32_t last_tid_sent;
	struct ipdc_system_info_t *next;
};

struct ipdc_msg_handlers_t {
	uint16_t code;
	uint8_t stack_pos;
	int (*func)(char *, uint32_t, struct ipdc_system_info_t *);
	struct ipdc_msg_handlers_t *next;
};

struct ipdc_conga_line_t {
	struct ipdc_system_info_t *si;
	uint16_t code;
	uint32_t trans_id;
	struct ipdc_conga_line_t *next;
};

#endif /* IPDC_MESSAGES_H */
