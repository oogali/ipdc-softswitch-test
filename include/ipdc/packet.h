#ifndef IPDC_PACKET_H
#define IPDC_PACKET_H

struct ipdc_pktdata_t {
	uint8_t tag;
	uint8_t size;
	unsigned char data[];
};
#define IPDC_PKTDATA_T_LEN			2

struct ipdc_pkthdr_t {
	uint16_t pkt_len;
	uint8_t protocol_id;
	uint8_t trans_id_len;
	uint32_t trans_id;
	uint16_t msg_code;
};
#define IPDC_PKTHDR_T_LEN				10

#define IPDC_MAX_PACKET_SIZE		0xFFFF
#define IPDC_TRANS_ID_SIZE			0x04
#define IPDC_PROTOCOL_ID				0x4B

#endif /* IPDC_PACKET_H */
